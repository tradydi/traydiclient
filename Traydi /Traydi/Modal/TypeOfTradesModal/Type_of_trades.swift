
import Foundation
struct Type_of_trades : Codable {
    var id : String = ""
    var type_of_trade : String = ""
    var status : Bool = false
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case type_of_trade = "type_of_trade"
        case status  = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)!
        type_of_trade = try values.decodeIfPresent(String.self, forKey: .type_of_trade)!
        status = ((try values.decodeIfPresent(Bool.self, forKey: .status)) != nil)
	}

}
