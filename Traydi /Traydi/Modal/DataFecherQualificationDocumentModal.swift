//
//  DataFecherQualificationDocumentModal.swift
//  Traydi
//
//  Created by mac on 01/05/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFecherQualificationDocumentModal: NSObject {
    var code:Bool?
    var message:String?
    var list:[ListModal]?
    
    init(parameters:[String:Any]) {
        
        let strCode = parameters["code"] as? String
            if strCode == "1" {
                code = true
            } else {
                code = false
            }
        message = parameters["message"] as? String
        let lists = parameters["list"] as? [[String:Any]]
        var listmodal = [ListModal]()
        
        for item in lists! {
            let listM = ListModal(parameters: item)
            listmodal.append(listM)
        }
        list = listmodal
    }
}

class ListModal: NSObject {
    var id:String?
    var user_id:String?
    var document_type:String?
    var document:String?
    var created_at:String?
    var  document_name : String?
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        document_type = parameters["document_type"] as? String
        document = parameters["document"] as? String
        created_at = parameters["created_at"] as? String
        document_name = parameters["document_name"] as? String
    }
}
