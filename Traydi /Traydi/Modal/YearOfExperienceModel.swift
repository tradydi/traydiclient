//
//  WorkTypeModel.swift
//  Traydi
//
//  Created by mac on 04/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class YearOfExperienceModel: Codable {
    let code : String?
    let message : String?
    let years : [YearsModel]?
    
    enum CodingKeys: String, CodingKey {
        
        case code = "code"
        case message = "message"
        case years = "years"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        years = try values.decodeIfPresent([YearsModel].self, forKey: .years)
    }

}

class YearsModel: Codable {
    
    let id : String?
    let years: String?
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case years = "years"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        years = try values.decodeIfPresent(String.self, forKey: .years)
    }

}
