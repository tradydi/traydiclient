//
//  LeadProjectModel.swift
//  Traydi
//
//  Created by mac on 23/09/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class LeadProjectMainModel: NSObject {
    var code:String?
    var message:String?
    var projectlist: [LeadProjectModel]?
    
    init(parameters:[String:Any]) {
        code = parameters["code"] as? String
        message = parameters["message"] as? String
        let  bookingList = parameters["projectlist"] as! [[String:Any]]
        var book =  [LeadProjectModel]()
        for item in bookingList {
            let books = LeadProjectModel(parameters: item)
            book.append(books)
        }
        projectlist = book
    }
}




class LeadProjectModel: NSObject {
    
     var created_at:String?
    var customer_email:String?
    var customer_name:String?
    var customer_phone_no:String?
    var id:String?
    var lastest_updates:String?
    var project_end_time:String?
    var project_name:String?
    var project_start_time:String?
    var project_type:String?
    var type:String?
    var user_id:String?
    var user_type:String?
    

    init(parameters: [String:Any]) {
        created_at = parameters["created_at"] as? String
        customer_email = parameters["customer_email"] as? String
        customer_name = parameters["customer_name"] as? String
        customer_phone_no = parameters["customer_phone_no"] as? String
        id = parameters["id"] as? String
        lastest_updates = parameters["lastest_updates"] as? String
        project_end_time = parameters["project_end_time"] as? String
        project_name = parameters["project_name"] as? String
        project_start_time = parameters["project_start_time"] as? String
        project_type = parameters["project_type"] as? String
        type = parameters["type"] as? String
        user_id = parameters["user_id"] as? String
        user_type = parameters["user_type"] as? String
    }
}
