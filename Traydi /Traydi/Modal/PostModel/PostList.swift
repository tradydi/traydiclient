

import Foundation

class Postlist : NSObject {
	var code : String?
    var message : String?
    var postinfo : [Postinfo]?

    init(parameters:[String:Any]) {
         code = parameters["code"] as? String
         message = parameters["message"]  as? String
        let arrPostinfo = parameters ["postinfo"] as? [[String:Any]]
        var arrPostInfoMain = [Postinfo]()
        if arrPostinfo?.count ?? 0 > 0{
            for item in arrPostinfo! {
                      let post = Postinfo(parameters: item)
                      arrPostInfoMain.append(post)
                  }
                  postinfo = arrPostInfoMain
        }
      
    }

}
