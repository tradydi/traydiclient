//
//  DataFetcherRequestModal.swift
//  Traydi
//
//  Created by mac on 14/05/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherRequestModal: NSObject {
    
    var code:String?
    var message:String?
    var requestList: [RequestModal]?
    
    init(parameters:[String:Any]) {
        code = parameters["code"] as? String
        message = parameters["message"] as? String
        let  requestLists = parameters["request_list"] as! [[String:Any]]
        var requests =  [RequestModal]()
        for item in requestLists {
            let request = RequestModal(item)
            requests.append(request)
        }
        requestList = requests
    }
}


class RequestModal: NSObject {
        var id:String?
        var full_name:String?
        var image:String?
       
       init(_ parameters:[String:Any]) {
           id = parameters["id"] as? String
           full_name = parameters["full_name"] as? String
           image = parameters["image"] as? String
       }
}
