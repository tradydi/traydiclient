//
//  DataFetcherStatusModal.swift
//  Traydi
//
//  Created by mac on 29/04/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherStatusModal: NSObject {
    var code:String?
    var message:String?
    var story: [UserDetails]?
    
    init(parameters:[String:Any]) {
        code = parameters["code"] as? String
        message = parameters["message"] as? String
        let  storyList = parameters["story"] as! [[String:Any]]
        var storys =  [UserDetails]()
        for item in storyList {
            let stor = UserDetails(parameters: item)
            storys.append(stor)
        }
        story = storys
    }
}


class UserDetails: NSObject {
    var id:String?
    var full_name:String?
    var image:String?
    var story:[Content]?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        full_name = parameters["full_name"] as? String
        image = parameters["image"] as? String
        let  storyList = parameters["story"] as! [[String:Any]]
        var storys =  [Content]()
        for item in storyList {
            let stor = Content(parameters: item)
            storys.append(stor)
        }
        story = storys
    }
}

class Content: NSObject {
    var created_at:String?
    var id:String?
    var image :String?
    var type :String?
    var user_id :String?
    var user_type :String?
    
    init(parameters:[String:Any]) {
        created_at = parameters["created_at"] as? String
        id = parameters["id"] as? String
        image = parameters["image"] as? String
        type = parameters["type"] as? String
        user_id = parameters["user_id"] as? String
        user_type = parameters["user_type"] as? String
    }
    
}
