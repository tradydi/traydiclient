
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let notificationListingModel = try? newJSONDecoder().decode(NotificationListingModel.self, from: jsonData)

import Foundation

// MARK: - NotificationListingModel
struct NotificationListingModel: Codable {
    let code, message: String?
    let notificationList: [NotificationList]?

    enum CodingKeys: String, CodingKey {
        case code, message
        case notificationList = "notification_list"
    }
}

// MARK: - NotificationList
struct NotificationList: Codable {
    let notificationID, message, projectID, resourceID: String?
    let senderID, receiverID, createdTime, type: String?

    enum CodingKeys: String, CodingKey {
        case notificationID = "notification_id"
        case message
        case projectID = "project_id"
        case resourceID = "resource_id"
        case senderID = "sender_id"
        case receiverID = "receiver_id"
        case createdTime = "created_time"
        case type
    }
}
