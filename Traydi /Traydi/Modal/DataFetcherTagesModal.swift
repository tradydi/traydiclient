//
//  DataFetcherTagesModal.swift
//  Traydi
//
//  Created by mac on 02/03/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherTagesModal: Codable {
    let code : String?
    let message : String?
    let tagsList : [TagsdModal]?
    
    enum CodingKeys: String, CodingKey {
        
        case code = "code"
        case message = "message"
        case tagsList = "tags"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        tagsList = try values.decodeIfPresent([TagsdModal].self, forKey: .tagsList)
    }

}

class TagsdModal: Codable {
    
    let id : String?
    let tags: String?
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case tags = "tags"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        tags = try values.decodeIfPresent(String.self, forKey: .tags)
    }

}
