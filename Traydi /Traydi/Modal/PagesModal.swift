//
//  PagesModel.swift
//  Traydi
//
//  Created by mac on 23/03/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class PagesModel: NSObject {
    var id:String?
    var user_id:String?
    var page_tittle:String?
    var image:String?
    var created_at:String?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        user_id = parameters["user_id"]  as? String
        page_tittle = parameters["page_tittle"] as? String
        image = parameters["image"] as? String
        created_at = parameters["created_at"] as? String
    }
}

