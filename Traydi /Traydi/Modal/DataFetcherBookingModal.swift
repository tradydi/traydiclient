//
//  DataFetcherBookingModal.swift
//  Traydi
//
//  Created by mac on 11/05/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherBookingModal: NSObject {
    var code:String?
    var message:String?
    var bookings: [BookingModal]?
    
    init(parameters:[String:Any]) {
        code = parameters["code"] as? String
        message = parameters["message"] as? String
        let  bookingList = parameters["booking"] as! [[String:Any]]
        var book =  [BookingModal]()
        for item in bookingList {
            let books = BookingModal(parameters: item)
            book.append(books)
        }
        bookings = book
    }
}


class BookingModal: NSObject {
    var id:String?
    var user_id:String?
    var project_name:String?
    var project_type:String?
    var project_start_time:String?
    var project_end_time:String?
    var job_end_time:String?
    var job_start_time:String?
    var required_resources:String?
    var created_at:String?
    var user_type:String?
    var job_type:String?
    var job_post:String?
    var working_skills:String?
    var job_description:String?
    var postcode:String?
    var address:String?
    var latitude:String?
    var longitude:String?
    var distance:String?
    var step1:String?
    var step2:String?
    var step3:String?
    var step4:String?
    var progress_status:String?
    var project_status:String?
    var project_resources: [JoblistResources]?
    var applied:String?
    
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        project_name = parameters["project_name"] as? String
        project_type = parameters["project_type"] as? String
        project_start_time = parameters["project_start_time"] as? String
        project_end_time = parameters["project_end_time"] as? String
        job_start_time = parameters["job_start_time"] as? String
        job_end_time = parameters["job_end_time"] as? String
        required_resources = parameters["required_resources"] as? String
        created_at = parameters["created_at"] as? String
        user_type = parameters["user_type"] as? String
        job_type = parameters["job_type"] as? String
        job_post = parameters["job_post"] as? String
        working_skills = parameters["working_skills"] as? String
        job_description = parameters["job_description"] as?  String
        postcode = parameters["postcode"] as? String
        address = parameters["address"] as? String
        latitude = parameters["latitude"] as? String
        longitude = parameters["longitude"] as? String
        distance = parameters["distance"] as? String
        step1 = parameters["step1"] as? String
        step2 = parameters["step2"] as? String
        step3 = parameters["step3"] as? String
        step4 = parameters["step4"] as? String
        progress_status = parameters["progress_status"] as? String
        project_status = parameters["project_status"] as? String
        applied = parameters["applied"] as? String
        let arrCommen = parameters["project_resources"] as? [[String:Any]]
               var arrcomentmain = [JoblistResources]()
               if arrCommen != nil {
                   if  arrCommen?.count ?? 0 > 0{
                       for item in arrCommen!{
                           let comment = JoblistResources(parameters: item)
                           arrcomentmain.append(comment)
                       }
                   }
               }
               project_resources = arrcomentmain
    }
}



class JoblistResources : NSObject {
    var address : String?
    var applied : Int?
    var credential : String?
    var define_rate : String?
    var endorse_skill : String?
    var id : String?
    var job_description : String?
    var job_post : String?
    var job_type : String?
    var latitude : String?
    var longitude : String?
    var payment : String?
    var project_id : String?
    var rating : String?
    var resource : String?
    var skills : String?
    var type : String?
    var user_id : String?
    var view : Int?
    var working_skills : String?
     
    
    init(parameters:[String:Any]){
         address = parameters["address"] as? String
         applied = parameters["applied"] as? Int
         credential = parameters["credential"] as? String
         define_rate = parameters["define_rate"] as? String
         endorse_skill = parameters["endorse_skill"] as? String
         id = parameters["id"] as? String
         job_description = parameters["job_description"] as? String
         job_post = parameters["job_post"] as? String
         job_type = parameters["job_type"] as? String
         latitude = parameters["latitude"] as? String
         longitude = parameters["longitude"] as? String
         payment = parameters["payment"] as? String
         project_id = parameters["project_id"] as? String
         rating = parameters["rating"] as? String
         resource = parameters["resource"] as? String
         skills = parameters["skills"] as? String
         type = parameters["type"] as? String
         user_id = parameters["user_id"] as? String
         view = parameters["view"] as? Int
         working_skills = parameters["working_skills"] as? String
        
    }
}
