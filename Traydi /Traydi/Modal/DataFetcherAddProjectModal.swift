//
//  DataFetcherAddProjectModal.swift
//  Traydi
//
//  Created by mac on 18/05/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherAddProjectModal: NSObject {
    var address:String?
    var created_at:String?
    var id:String?
    var job_time:String?
    var latitude:String?
    var longitude:String?
    var project_name:String?
    var project_status:String?
    var project_time:String?
    var project_type:String?
    var resource_required:String?
    var step1:String?
    var step2:String?
    var step3:String?
    var step4:String?
    var user_id:String?
    var user_type:String?

    init(parameters: [String:Any]) {
        address = parameters["address"] as? String
        created_at = parameters["created_at"] as? String
        id = parameters["id"] as? String
        job_time = parameters["job_time"] as? String
        latitude = parameters["latitude"] as? String
        longitude = parameters["longitude"] as? String
        project_name = parameters["project_name"] as? String
        project_status = parameters["project_status"] as? String
        project_time = parameters["project_time"] as? String
        project_type = parameters["project_type"] as? String
        resource_required = parameters["resource_required"] as? String
        step1 = parameters["step1"] as? String
        step2 = parameters["step2"] as? String
        step3 = parameters["step3"] as? String
        step4 = parameters["step4"] as? String
        user_id = parameters["user_id"] as? String
        user_type = parameters["user_type"] as? String
    }
}
