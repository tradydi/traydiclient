//
//  DataFetcherSideMenuPages.swift
//  Traydi
//
//  Created by mac on 30/03/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherSideMenuPages: NSObject {
        
    var sideMenuTitle:String?
    var pages:[DataFetcherPagesModal]?
    init(sideMenuTitle:String, pages:[DataFetcherPagesModal]) {
        self.sideMenuTitle = sideMenuTitle
        self.pages = pages
    }
    
}

class DataFetcherPagesModal: NSObject {
    
    var id:String?
    var user_id:String?
    var page_tittle:String?
    var image:String?
    var created_at:String?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        page_tittle = parameters["page_tittle"] as? String
        image = parameters["image"] as? String
        created_at = parameters["created_id"] as? String
    }
}
