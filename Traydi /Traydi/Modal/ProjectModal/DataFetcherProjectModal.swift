//
//  DataFetcherProjectModal.swift
//  Traydi
//
//  Created by mac on 23/04/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherProjectModal: NSObject {
    var id:String?
    var user_id:String?
    var project_name:String?
    var project_type:String?
    var project_start_time:String?
    var project_end_time:String?
    var job_start_time:String?
    var required_resources:String?
    var created_at:String?
    var user_type:String?
    var job_type:String?
    var job_post:String?
    var working_skills:String?
    var job_description:String?
    var postcode:String?
    var address:String?
    var latitude:String?
    var longitude:String?
    var distance:String?
    var step1:String?
    var step2:String?
    var step3:String?
    var step4:String?
   
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        user_id = parameters["user_id"] as? String
        project_name = parameters["project_name"] as? String
        project_type = parameters["project_type"] as? String
        project_start_time = parameters["project_start_time"] as? String
        project_end_time = parameters["project_end_time"] as? String
        job_start_time = parameters["job_start_time"] as? String
        required_resources = parameters["required_resources"] as? String
        created_at = parameters["created_at"] as? String
        user_type = parameters["user_type"] as? String
        job_type = parameters["job_type"] as? String
        job_post = parameters["job_post"] as? String
        working_skills = parameters["working_skills"] as? String
        job_description = parameters["job_description"] as? String
        postcode = parameters["postcode"] as? String
        address = parameters["address"] as? String
        latitude = parameters["latitude"] as? String
        longitude = parameters["longitude"] as? String
        distance = parameters["distance"] as? String
        step1 = parameters["step1"] as? String
        step2 = parameters["step2"] as? String
        step3 = parameters["step3"] as? String
        step4 = parameters["step4"] as? String
    }
    
}

