//
//  DataFetcherLeadsModal.swift
//  Traydi
//
//  Created by mac on 23/04/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation

class DataFetcherLeadsModal: NSObject {
    var id:String?
    var project_name:String?
    var project_type:String?
    var project_start_time:String?
    var project_end_time:String?
    var customer_name:String?
    var customer_phone_no:String?
    var customer_email:String?
    var lastest_updates:String?
    var created_at:String?
    var user_id:String?
    var user_type:String?
    
    init(parameters:[String:Any]) {
        id = parameters["id"] as? String
        project_name = parameters["project_name"] as? String
        project_type = parameters["project_type"] as? String
        project_start_time = parameters["project_start_time"] as? String
        project_end_time = parameters["project_end_time"] as? String
        customer_name = parameters["customer_name"] as? String
        customer_phone_no = parameters["customer_phone_no"] as? String
        customer_email = parameters["customer_email"] as? String
        lastest_updates = parameters["lastest_updates"] as? String
        created_at = parameters["created_at"] as? String
        user_id = parameters["user_id"] as? String
        user_type = parameters["user_type"] as? String
    }
}
