//
//  TDBaseViewController.swift
//  Traydi
//
//  Created by mac on 02/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .selected)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    
    func rightandLeftBottmCornerRadius(view:UIView) {
        view.clipsToBounds = true
        view.layer.cornerRadius = 30
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func setupSideMenuAnimation(isAdd:Bool, toView:UIView) {
           if isAdd {
               toView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
               toView.frame.origin.y =  view.frame.origin.y
               toView.frame.origin.x = -self.view.frame.width
               toView.alpha = 0
                self.navigationController?.view.addSubview(toView)
            UIView.animate(withDuration: 0.5) {
                   toView.frame.origin.x = 0
                   toView.alpha = 1
               }
           } else {
            UIView.animate(withDuration: 0.5, animations: {
                   toView.frame.origin.x = -self.view.frame.width
                   toView.alpha = 0
               }, completion: { (status) in
                   toView.removeFromSuperview()
               })
           }
       }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

}

