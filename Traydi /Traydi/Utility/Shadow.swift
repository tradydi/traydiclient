//
//  Shadow.swift
//  Radds
//
//  Created by mac on 17/08/18.
//  Copyright © 2018 CTIMac. All rights reserved.
//

import UIKit

class Shadow: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        //self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {        super.init(coder: aDecoder)
        //self.setup()
    }
    override func layoutSubviews() {
        self.setup()
    }
    
    func setup() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 20
        //self.layer.cornerRadius = 15.0
        //self.layer.masksToBounds = false
      
    }
}


class RoundCorner10: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.setup()
    }
    override func layoutSubviews() {
        self.setup()
    }
    
    func setup() {
        
        self.layer.cornerRadius = 10.0
        
    }
}

class CustomTextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
