//
//  Loader.swift
//  DemoWebervice
//
//  Created by MacMini on 03/01/18.
//  Copyright © 2018 Codiant. All rights reserved.
//

import UIKit

class Loader: UIView {

    static var loaderView : Loader?
    
    @IBOutlet weak  var imgView : UIImageView?
    @IBOutlet weak var viewBg : UIView?
    @IBOutlet weak var label: UILabel?

    
    override func awakeFromNib() {
       // imgView?.image = imgView?.image!.withRenderingMode(.alwaysTemplate)
       // imgView?.tintColor = UIColor.blue
    }
    
    class  func showLoader(_ title: String? = nil) {
        DotLoader.shared.showLoader()
      /*  if  loaderView != nil {
            hideLoader()
        }
        loaderView = UINib(nibName: "Loader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? Loader
        loaderView?.tag = 2000
        UIApplication.shared.keyWindow?.addSubview(loaderView!)
        loaderView!.frame = UIScreen.main.bounds
        loaderView?.label?.text = title

        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            loaderView?.viewBg!.alpha = 0.5
        }) { (finished) -> Void in
        }
        let rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation.fromValue = 0
        rotation.toValue = 2 * Double.pi
        rotation.duration = 1.1
        rotation.repeatCount = Float.infinity
        loaderView?.imgView!.layer.add(rotation, forKey: "Spin")*/
    }
    
    class func hideLoader() {
        DotLoader.shared.stopLoader()
       /* UIView.animate(withDuration: 0.0, animations: {
            loaderView?.viewBg!.alpha=0.1
        }) { (completion) in
            if  loaderView != nil {
                for subview : UIView in ( UIApplication.shared.keyWindow?.subviews)! {
                    if subview.tag == 2000 {
                        subview.removeFromSuperview()
                        loaderView = nil
                    }
                }
            }
        }*/
    }
}
