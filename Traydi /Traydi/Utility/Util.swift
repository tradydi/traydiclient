//
//  Util.swift
//  Recipe
//
//  Created by MacMini on 19/01/18.
//  Copyright © 2018 Codiant. All rights reserved.
//

import UIKit

class Util: NSObject {
    
    
    //MARK: open url
    class func openUrl(str: String) {
        if let url = URL(string: str) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { (completed) in
                        
                    })
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    class func makeACall(phoneNumber: String, completionHandler completion: ((Bool) -> Swift.Void)? = nil) {
        var strUrl = phoneNumber
        if phoneNumber.range(of: "tel://") == nil {
            strUrl = "tel://" + phoneNumber
        }
        if let url = URL(string: strUrl) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: completion)
                } else {
                    UIApplication.shared.openURL(url)
                    if completion != nil {
                        completion!(true)
                    }
                }
            }
        }
    }
    
    
    // MARK: - Alert Methods
    class func showNetWorkAlert()
    {
        showAlertWithCallback("No Network Connection", message: "Please check your connection and try again.", isWithCancel: false)
        // Loader.hideLoader()
    }
    
    //MARK:- alert with handler
    class func showAlertWithCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: (() -> Void)? = nil) {
        if UIApplication.topViewController() != nil {
            if (UIApplication.topViewController()!.isKind(of: UIAlertController.self)) {
                return
            }
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if isWithCancel {
            alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        }
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            handler?()
        }))
        
       // alertController.view.tintColor = UIColor.red
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlertWithCancelCallback(_ title: String?, message: String?, isWithCancel: Bool, handler: ((String) -> Void)? = nil) {
        if UIApplication.topViewController() != nil {
            if (UIApplication.topViewController()!.isKind(of: UIAlertController.self)) {
                return
            }
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            handler?("Ok")
        }))
        
        if isWithCancel {
            alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
                handler?("Cancel")
            }))
        }
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - *******Validations Methods*******
    class func isValidEmail(_ emailStr: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailStr)
    }
    
    //MARK: - ********Image Picker********
    class func openImagePicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = UIApplication.currentViewController() as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = true
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            Util.openCamera(imagePicker)
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            Util.openGallary(imagePicker)
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        UIApplication.currentViewController()?.present(alert, animated: true, completion: nil)
    }
    
    class func openCamera(_ picker: UIImagePickerController){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            UIApplication.currentViewController()?.present(picker, animated: true, completion: nil)
            
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            UIApplication.currentViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    class func openGallary(_ picker: UIImagePickerController){
        
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        UIApplication.currentViewController()?.present(picker, animated: true, completion: nil)
    }
    
    class func getValidString(_ string: String?) -> (String) {
        if string == nil || string == "nil" || string!.isKind(of: NSNull.self) || string == "null" || string == "<null>" || string == "(null)" {
            
            return ""
        }
        return string!.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    class func encodeURLLink(_ string: String) -> (String){
        return string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    }
    
    
   class func viewDotedBorder(_ viewDashed: UIView) {
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.red.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = viewDashed.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: viewDashed.bounds).cgPath
        viewDashed.layer.addSublayer(yourViewBorder)
        
    }
}

//Return top or visible view controller
extension UIApplication {
    
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension Float {
    func round(nearest: Float) -> Float {
        let n = 1 / nearest
        let numberToRound = self * n
        return numberToRound.rounded() / n
        
    }
}

extension Int {
    func round(nearest: Int) -> Int {
        let n = 1 / nearest
        let numberToRound = self * n
        return numberToRound.round(nearest: n)
        
    }
}

struct ScreenSize {
    
    static let width = UIScreen.main.bounds.width
    static let height = UIScreen.main.bounds.height
    
    static func inOrientation() -> CGSize {
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft, .landscapeRight:
            return CGSize(width: max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), height: min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height))
        default:
            return CGSize(width: min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height), height: max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height))
        }
    }
}



public extension UINavigationController {
    func popNumberOfViewControllers(_ nb: Int) {
        let viewControllers: [UIViewController] = self.viewControllers
        guard viewControllers.count < nb else {
            self.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
            return
        }
    }
}

extension Optional where Wrapped == String {
    
    var isBlank: Bool {
        return self.validate.count == 0
    }
    
    var validate: String {
        return self ?? ""
    }
    
}

extension Optional where Wrapped == Int {
    var validate: Int {
        return self ?? 0
    }
}


class ShadowView: UIView
{
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func layoutSubviews() {
        self.setup()
    }
    
    func setup() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 2.0
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = false
    }
    
    
}
