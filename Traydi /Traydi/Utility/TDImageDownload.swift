//
//  TDImageDownload.swift
//  Traydi
//
//  Created by mac on 01/05/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//


import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()


extension UIViewController {
    
  func getImageFromCache(urlString: String)-> UIImage?{
    var image : UIImage?
    
    if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
         image = imageFromCache
        return image
    }
    image = nil
    return image

  }
    
    func cacheImage(urlString: String, img : UIImage) {
     //   let url = URL(string: urlString)
         var image : UIImage?
        image = img
        imageCache.setObject(img, forKey: urlString as AnyObject)
        
       /* if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        else {
            self.image = img
            imageCache.setObject(img, forKey: urlString as AnyObject)
        }*/
        
        
        /* URLSession.shared.dataTask(with: url!) {
         data, response, error in
         if let response = data {
         DispatchQueue.main.async {
         let imageToCache = UIImage(data: response)
         if imageToCache == nil {
         
         } else {
         self.image = imageToCache
         imageCache.setObject(imageToCache!, forKey: urlString as AnyObject)
         }
         }
         }
         }.resume()*/
    }
}
