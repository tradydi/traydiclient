//
//  APIManager.swift
//  SwipeK Teacher
//
//  Created by mac on 10/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Alamofire

protocol ApiManagerDelegate:NSObject {
    func successResponse(response:Any, check:String)
    func errorResponse(error:Any, check:String)
}

class APIManager: NSObject {
    static let share = APIManager()
    weak var delegate:ApiManagerDelegate?
    //MARK:- Multipart Api
    func performMaltipartPost(params:[String:Any], apiAction:String) {
        Loader.showLoader("Loading....")
        if !ReachabilityTraydi.isConnectedToNetwork() {
            Loader.hideLoader()
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
            DispatchQueue.main.async(execute: {
                self.delegate?.errorResponse(error: error , check: apiAction)
            })
            return
        }
       var postUrl:String = ""
        if UserDefaults.standard.bool(forKey: "isSeeker") {
          postUrl = kAPI_BaseURL  + kTradees  + apiAction
        } else {
         postUrl = kAPI_BaseURL + kTraders + apiAction
        }
         print("postUrl--\(postUrl)")
         print("params-=-\(params)")
        
         let urlString = postUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, usingThreshold: UInt64.init(), to: urlString ?? "", method: .post, headers:nil /*headers*/) { (result) in
           
            switch result {
            case .success(let upload, _, _):
               upload.responseJSON { response in
                Loader.hideLoader()
                
                print(response.result.value as Any)
               // let dict  = response.result.value as? [String:Any]
              
               // print("Response--\(dict!)")
                self.delegate?.successResponse(response: response.data as Any, check: apiAction)
               }
            case .failure(let error):
                 Loader.hideLoader()
                print("Error in upload: \(error.localizedDescription)")
                self.delegate?.errorResponse(error: error, check: apiAction)
                //completionHandler(false, nil, error.localizedDescription, error)
            }
        }
    }
    
    func performActionGetApi(params:[String:Any],  apiAction:String) {
        Loader.showLoader("Loading....")
        if !ReachabilityTraydi.isConnectedToNetwork() {
            Loader.hideLoader()
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
            DispatchQueue.main.async(execute: {
                self.delegate?.errorResponse(error: error , check: apiAction)
            })
            return
        }
        var postUrl:String = ""
        if UserDefaults.standard.bool(forKey: "isSeeker") {
            postUrl = kAPI_BaseURL  + kTradees  + apiAction
        } else {
            postUrl = kAPI_BaseURL  + kTraders + apiAction
        }
        print("postUrl--\(postUrl)")
        print("params-=-\(params)")
        let urlString = postUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
               
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, usingThreshold: UInt64.init(), to: urlString ?? "" , method: .get, headers:nil /*headers*/) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    Loader.hideLoader()
                    print("response-=-\(response.result.value as Any)")
                   // print(response.result.value as Any)
                    self.delegate?.successResponse(response: response.data as Any, check: apiAction)
                }
            case .failure(let error):
                Loader.hideLoader()
                print("Error in upload: \(error.localizedDescription)")
                self.delegate?.errorResponse(error: error, check: apiAction)
                //completionHandler(false, nil, error.localizedDescription, error)
            }
        }
    }
    
    func performDocumentsUpload(params:[String:Any], apiAction:String, documentsParams:[[String:Any]], documentType:String ,successCompletionHandler: @escaping ((DataResponse<Any>)-> Void) , errorCompletionHandler: @escaping ((Error) -> Void)) {
        Loader.showLoader("Loading....")
        if !ReachabilityTraydi.isConnectedToNetwork() {
            Loader.hideLoader()
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
          DispatchQueue.main.async(execute: {
                errorCompletionHandler(error)
            })
            return
        }
        var postUrl:String = ""
        if UserDefaults.standard.bool(forKey: "isSeeker") {
            postUrl = kAPI_BaseURL + kTradees  + apiAction
        } else {
            postUrl = kAPI_BaseURL  + kTraders + apiAction
        }
        print(postUrl)
       // print(documentsParams)
        print(params)
        let urlString = postUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for document in documentsParams {
                for (key, value) in document {
                    let data = value as? Data
                    let imageName = UUID().uuidString + ".\(documentType)"
                    //print(key)
                    // print(value)
                    // print(imageName)
                    print(documentType)
                    multipartFormData.append(data!, withName: key ,fileName: imageName , mimeType: "image/jpg") //"image/jpg"  "application/\(documentType)"
                }
            }
            /*image/png
            text/plain
            image/gif
            text/plain
            application/vnd.ms-powerpoint
            application/pdf*/
          
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, usingThreshold: UInt64.init(), to: urlString ?? "", method: .post, headers:nil /*headers*/) { (result) in
           
            switch result {
            case .success(let upload, _, _):
               upload.responseJSON { response in
                 Loader.hideLoader()
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                
                })
                    print(response.result.value as Any)
                    successCompletionHandler(response)
               }
                
            case .failure(let error):
                 Loader.hideLoader()
                print("Error in upload: \(error.localizedDescription)")
                 DispatchQueue.main.async(execute: {
                    errorCompletionHandler(error)
                 })
                //completionHandler(false, nil, error.localizedDescription, error)
            }
        }
    }
    
     //MARK:- Image uploads
    func performImageUpload(params:[String:Any], apiAction:String, imageParams:[[String:Any]], isVideoUpload : Bool? = false) {
        Loader.showLoader("Loading....")
        if !ReachabilityTraydi.isConnectedToNetwork() {
            Loader.hideLoader()
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
            DispatchQueue.main.async(execute: {
                self.delegate?.errorResponse(error: error , check: apiAction)
            })
            return
        }
        var postUrl:String = ""
        if UserDefaults.standard.bool(forKey: "isSeeker") {
            postUrl = kAPI_BaseURL  + kTradees  + apiAction
        } else {
            postUrl = kAPI_BaseURL  + kTraders + apiAction
        }
        print("postUrl-=-\(postUrl)")
          print("params-=-\(params)")
        print("imageParams=\(imageParams)=")
        let urlString = postUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for images in imageParams {
                if isVideoUpload! {
                    print("Upload video-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
                     for (key, value) in images {
                         let videoUrl = value as! URL
                        do {
                            let data = try Data(contentsOf: videoUrl, options: .mappedIfSafe)
                            print(data)
                            print("File size before compression: \(Double(data.count / 1048576)) mb")
                            //  here you can see data bytes of selected video, this data object is upload to server by multipartFormData upload
                            let imageName = UUID().uuidString + ".mp4"
                            multipartFormData.append(data, withName: key ,fileName: imageName , mimeType: "video/mp4")
                        } catch  {
                            return
                        }
                    }
                }else{
                      print("Upload photo-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
                    for (key, value) in images {
                        let image = value as? UIImage
                        let thumb1 = image?.resized(withPercentage: 0.5)
                        let thumb2 = thumb1?.resized(toWidth: 50.0)
                        guard let data = thumb1?.pngData() else {
                            return
                        }
                        let imageName = UUID().uuidString + ".png"
                        multipartFormData.append(data, withName: key ,fileName: imageName , mimeType: "image/jpg")
                    }
                }
            }
          
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, usingThreshold: UInt64.init(), to: urlString ?? "", method: .post, headers:nil /*headers*/) { (result) in
           
            switch result {
            case .success(let upload, _, _):
               upload.responseJSON { response in
                 Loader.hideLoader()
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                
                })
                print(response.result.value as Any)
                self.delegate?.successResponse(response: response.data as Any, check: apiAction)
               }
              
            case .failure(let error):
                 Loader.hideLoader()
                print("Error in upload: \(error.localizedDescription)")
                self.delegate?.errorResponse(error: error, check: apiAction)
                //completionHandler(false, nil, error.localizedDescription, error)
            }
        }
        
    }
    // completionHandler
    func performPostApiHandler(params:[String:Any], apiAction:String , successCompletionHandler: @escaping ((DataResponse<Any>)-> Void) , errorCompletionHandler: @escaping ((Error) -> Void)) {
         Loader.showLoader("Loading....")
        if !ReachabilityTraydi.isConnectedToNetwork() {
           Loader.hideLoader()
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Please check your internet connection"])
            DispatchQueue.main.async(execute: {
                errorCompletionHandler(error)
            })
            return
        }
        var postUrl:String = ""
        if UserDefaults.standard.bool(forKey: "isSeeker") {
            postUrl = kAPI_BaseURL + kTradees  + apiAction
        } else {
            postUrl = kAPI_BaseURL  + kTraders + apiAction
        }
        let urlString = postUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        }, usingThreshold: UInt64.init(), to: urlString ?? "", method: .post, headers:nil /*headers*/) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                  Loader.hideLoader()
                    print(response.result.value as Any)
                    successCompletionHandler(response)
                }
                
            case .failure(let error):
                //Loader.hideLoader()
                print("Error in upload: \(error.localizedDescription)")
                 errorCompletionHandler(error)
            }
        }
        
    }
    
    //Mark:- Upload video
    
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    
    /*
     let image = UIImage(data: try! Data(contentsOf: URL(string:"http://i.stack.imgur.com/Xs4RX.jpg")!))!

     let thumb1 = image.resized(withPercentage: 0.1)
     let thumb2 = image.resized(toWidth: 72.0)*/
}


