//
//  AppDelegate.swift
//  Traydi
//
//  Created by mac on 09/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import SocketIO
import CoreLocation
import GoogleMaps
//import GooglePlaces
import CoreLocation
import IQKeyboardManagerSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,CLLocationManagerDelegate  {

    var window: UIWindow?
    var deviceToken: String = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      //  let backImage = UIImage(named: "backarrowwhite")!.withRenderingMode(.alwaysOriginal)
     //   UINavigationBar.appearance().backIndicatorImage = backImage
       // UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        //UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -80.0), for: .default)
        DotLoader.shared.animateDotLoader = true
        DotLoader.shared.dotColor = UIColor.orange
        DotLoader.shared.loaderContainerColor = UIColor.clear
        
        GMSServices.provideAPIKey(googleAPIKey)
      // SocketIOManager.sharedInstance.connectSocket()
      //  SocketIOManager.sharedInstance.establishConnection()
        if UserDefaults.standard.isLoggedIn(){
            window = UIWindow.init(frame: UIScreen.main.bounds)
            let vc = CustomTabBarController.instance(.tabBarViewController) as! CustomTabBarController
            let data = UserDefaults.standard.getUserData()
            guard let loginModal = try? JSONDecoder().decode(LoginModal.self, from:data) else { return true }
            AppSignleHelper.shard.login = loginModal
            vc.tabBar.isHidden = true
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
            
        }
        IQKeyboardManager.shared.enable = true;
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    initLocationManager()
       
        return true
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        self.deviceToken = deviceTokenString
        //print(deviceTokenString)
    }

    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        Thread.sleep(forTimeInterval: 0.5)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
       // SocketIOManager.sharedInstance.establishConnection()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //hp
    var locManager: CLLocationManager? = CLLocationManager()
    
//    func curLocation() -> CLLocation? {
//
//        locManager?.startUpdatingLocation()
//
//        let userLocation = locManager?.location
//
//        if userLocation != nil {
//            //print("location is \(userLocation!)")
//        } else {
//            //print("location is nil")
//        }
//
//        return userLocation
//    }
    func initLocationManager() {
        
        locManager?.delegate = self
        locManager?.desiredAccuracy = kCLLocationAccuracyBest
        
        locManager?.requestAlwaysAuthorization()
        locManager?.requestWhenInUseAuthorization()
        
        locManager?.distanceFilter = 10
        locManager?.startUpdatingLocation()
        locManager?.startUpdatingHeading()
        
        // Check authorizationStatus
        let authorizationStatus = CLLocationManager.authorizationStatus()
        // List out all responses
        
        switch authorizationStatus {
        case .authorizedAlways:
            print("authorized")
        case .authorizedWhenInUse:
            print("authorized when in use")
        case .denied:
            print("denied")
        case .notDetermined:
            print("not determined")
        case .restricted:
            print("restricted")
        }
        
        // Get the location
        locManager?.startUpdatingLocation()
        
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            //   Extract the location from CLLocationManager
            let userLocation = locManager?.location
            
            // Check to see if it is nil
            if userLocation != nil {
                print("location is \(userLocation!)")
            } else {
                print("location is nil")
            }
        }else {
            if (CLLocationManager.authorizationStatus() == .denied) { // CLLocationManager.locationServicesEnabled() ||
                locManager?.requestWhenInUseAuthorization()
                //                    curLocation()
            }
            ////print("not authorized")
        }}


}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}


@IBDesignable extension UINavigationController {
    @IBInspectable var barTintColor: UIColor? {
        set {
            navigationBar.barTintColor = newValue
        }
        get {
            guard  let color = navigationBar.barTintColor else { return nil }
            return color
        }
    }

    @IBInspectable var tintColor: UIColor? {
        set {
            navigationBar.tintColor = newValue
        }
        get {
            guard  let color = navigationBar.tintColor else { return nil }
            return color
        }
    }

    @IBInspectable var titleColor: UIColor? {
        set {
            guard let color = newValue else { return }
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
        }
        get {
            return navigationBar.titleTextAttributes?[NSAttributedString.Key(rawValue: "NSForegroundColorAttributeName")] as? UIColor
        }
    }
}
