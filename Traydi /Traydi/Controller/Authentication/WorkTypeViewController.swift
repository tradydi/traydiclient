//
//  WorkTypeViewController.swift
//  Traydi
//
//  Created by mac on 13/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class WorkTypeViewController: UIViewController ,TagListViewDelegate{
     @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblWorkType: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnSelectYourWork: UIButton!
    @IBOutlet weak var imgViewWorkType: UIImageView!
    @IBOutlet weak var btnWT1: UIButton!
    @IBOutlet weak var btnWT2: UIButton!
    @IBOutlet weak var btnWT3: UIButton!
    @IBOutlet weak var scrollViewBg: UIScrollView!
    @IBOutlet weak var constraintHeightImageType: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightBack: NSLayoutConstraint!
    @IBOutlet weak var tblViewWork: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
    var controllerType:Bool = false
    
    var arrWorkType = [Type_of_trades]()
    var arrSearch = [Type_of_trades]()
    var arrSearchFinal = [Type_of_trades]()
    var strSearchText = ""
   // var selectedRow:Bool = false
    var selectButton:UIButton?
   // var selectedIndexPath = IndexPath()
    var strSkill = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
        tblViewHeightConstraint.constant = 210
        registerNotifications()
        self.setupToHideKeyboardOnTapOnView()
        self.tblViewWork.delegate = self
        self.tblViewWork.dataSource = self
        self.tblViewWork.allowsMultipleSelection = true
        self.txtSearch.delegate = self
        self.btnWT1.setTitle("Commercial", for: .normal)
               self.btnWT2.setTitle("Residential", for: .normal)
               self.btnWT3.setTitle("Both", for: .normal)
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
            self.lblWorkType.text = "What job do you work on?"
        }else{
            //Traders
            self.lblTitle.text = "Traders"
            self.lblWorkType.text = "Work Type"
        }
        if controllerType {
            lblTitle.text = "Edit"
            constraintHeightBack.constant = 0
            constraintHeightImageType.constant = 0
            btnContinue.setTitle("Save", for: .normal)
            let tabBarController = self.tabBarController as! CustomTabBarController
                tabBarController.isHiddenTabBar(hidden: true)
          //  tabBarController.lblNotificationCount.text = "1"
          let workType =   AppSignleHelper.shard.login.userinfo?.work_type
            if workType == "Commercial"{
               self.btnWorktype1Action(btnWT1)
            }else if workType == "Residential"{
                self.btnWorktype2Action(btnWT2)
            }else if workType == "Both"{
                self.btnWorktype3Action(btnWT3)
            }
        }else{
            self.btnWorktype1Action(btnWT1)
        }
        
        self.btnContinue.layer.cornerRadius = 25.0
        self.imgViewWorkType.layer.cornerRadius = 25.0
       // self.tblViewHeightConstraint.constant = 0
        self.btnWorktype1Action(btnWT1)
         tagListView.delegate = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.workTypeApiCall()
    }
    
    func workTypeApiCall() {
        APIManager.share.delegate = self
        let apiAction = ApiAction.TradersAndTradees.ktype_of_trade
        APIManager.share.performMaltipartPost(params: [:], apiAction: apiAction)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          self.view.endEditing(true)
      }
    
    @IBAction func selectYourWorkBtnAction(_ sender: Any) {
      //  self.pickerViewWork.isHidden  = !self.pickerViewWork.isHidden
    }
    
    @IBAction func btnWorktype1Action(_ sender: UIButton) {
        self.btnWT1.layer.cornerRadius = 25.0
        self.btnWT2.layer.cornerRadius = 25.0
        self.btnWT3.layer.cornerRadius = 25.0
        
        self.btnWT1.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        self.btnWT2.backgroundColor = UIColor.clear
        self.btnWT3.backgroundColor = UIColor.clear
        
        self.btnWT1.setTitleColor(UIColor.white, for: .normal)
        self.btnWT2.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT3.setTitleColor(UIColor.gray, for: .normal)
        
        self.btnWT1.setTitle("Commercial", for: .normal)
        self.btnWT2.setTitle("Residential", for: .normal)
        self.btnWT3.setTitle("Both", for: .normal)
        selectButton = sender
        
    }
    @IBAction func btnWorktype2Action(_ sender: UIButton) {
        
        self.btnWT1.backgroundColor = UIColor.clear
        self.btnWT2.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        self.btnWT3.backgroundColor = UIColor.clear
        
        self.btnWT1.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT2.setTitleColor(UIColor.white, for: .normal)
        self.btnWT3.setTitleColor(UIColor.gray, for: .normal)
        selectButton = sender
    }
    
    @IBAction func btnWorktype3Action(_ sender: UIButton) {
        
        self.btnWT1.backgroundColor = UIColor.clear
        self.btnWT2.backgroundColor = UIColor.clear
        self.btnWT3.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        
        self.btnWT1.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT2.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT3.setTitleColor(UIColor.white, for: .normal)
        selectButton = sender
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func actionContinue(_ sender: Any) {
        if selectButton?.titleLabel?.text == ""{
             Util.showAlertWithCallback(AppName, message: UseCaseMessage.validation.empty.kWorkTypes, isWithCancel: false)
            return
        }
            if arrSearch.count > 0  {
                let data = AppSignleHelper.shard.login
                var params = [String:Any]()
                params[ApiAction.TradersAndTradees.kwork_type] = selectButton?.titleLabel?.text
                let selectWorkType = arrSearch.filter{$0.status == true }
                let keys = selectWorkType.map { $0.type_of_trade }
                 var arrslots = [[String:Any]]()
                for dicts in selectWorkType {
                    var dict = [String:Any]()
                    dict["type_of_trade"] = dicts.type_of_trade
                    dict["id"] = dicts.id
                    dict["selectable"] = String(dicts.status)
                    arrslots.append(dict)
                }
               let str =  json(from: arrslots)
                let formattedArray = (keys.map{String($0)}).joined(separator: ",")
                params[ApiAction.TradersAndTradees.ktype_of_trade] = str //arrJson
                params[ApiAction.TradersAndTradees.ktrade_needed] = formattedArray
                
                 if AppSharedData.shared.isSeeker == true{
                    params[ApiConstants.key.ktradee_id] =  data?.userinfo?.id
                 }else{
                    params[ApiConstants.key.ktrader_id] =  data?.userinfo?.id
                }
                print(params)
               let apiAction = ApiAction.TradersAndTradees.kwork_type
                APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
            } else {
                Util.showAlertWithCallback(AppName, message: UseCaseMessage.validation.empty.kWorkTypeOfTtrade, isWithCancel: false)
            }
       // }
    }
    func stringArrayToData(stringArray: [String]) -> Data? {
      return try? JSONSerialization.data(withJSONObject: stringArray, options: [])
    }
    
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
      //  print("Tag Remove pressed: \(title), \(sender)")
        print(title)
       //   arrSearch = arrSearch.filter{ $0.type_of_trade != title}
        arrSearchFinal = arrSearchFinal.filter{$0.type_of_trade != title}
        for i in 0..<arrSearch.count {
            let dict = arrSearch[i]
            if dict.type_of_trade == title {
                arrSearch[i].status = false
                self.tblViewWork.reloadData()
            }
            
        }
        tblViewWork.reloadData()
      
        sender.removeTagView(tagView)
    }

}


extension WorkTypeViewController: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorktypeViewCell", for: indexPath) as! WorktypeViewCell
        let obj = arrSearch[indexPath.row]
        cell.lblTypeName.text = obj.type_of_trade
        if obj.status{
            cell.lblTypeName.textColor = appColor
         //   cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            cell.imgCheck.image = UIImage(named: "Checked")
        }else{
            cell.imgCheck.image =  UIImage(named: "UnChecked")
            cell.lblTypeName.textColor = .darkGray
           // cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           // var obj = arrSearch[indexPath.row]
        if arrSearch[indexPath.row].status{
            arrSearch[indexPath.row].status = false
            for item in arrSearchFinal {
                if item.type_of_trade == arrSearch[indexPath.row].type_of_trade {
                    arrSearchFinal = arrSearchFinal.filter{ $0.type_of_trade != item.type_of_trade}
                }
            }
        }else{
            arrSearch[indexPath.row].status = true
            arrSearchFinal.append(arrSearch[indexPath.row])
        }
        tblViewWork.reloadData()
        setTag()
    }
    func setTag(){
        strSkill.removeAll()
        tagListView.removeAllTags()
        for dict in arrSearch{
            if dict.status{
              strSkill.append(dict.type_of_trade)
            }
        }
        
        tagListView.addTags(strSkill)
    }
}

// MARK: - Textfield delegate
extension WorkTypeViewController: UITextFieldDelegate{
    
    @objc func SearchDidChange(_ textField: UITextField) {
        let textToSearch = textField.text ?? ""
        let filteredContacts = self.arrWorkType.filter({$0.type_of_trade.uppercased().contains(textToSearch.uppercased()) == true })
          arrSearch = filteredContacts
        if arrSearchFinal.count > 0 {
          arrSearch = arrSearchFinal
        }
        if  filteredContacts.count > 0 {
             arrSearch.removeAll()
              for dictLocal in filteredContacts {
                 var isFound = false
                for dict in arrSearchFinal {
                    if dict.type_of_trade == dictLocal.type_of_trade {
                       isFound = true
                        if dict.status {
                          arrSearch.append(dict)
                        }
                    }
                }
                if !isFound{
                    arrSearch.append(dictLocal)
                }
            }
        }else{
            arrSearchFinal = arrSearch
        }
        tblViewWork.reloadData()
    }
}







extension WorkTypeViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.ktype_of_trade {
          
            guard var workType = try? JSONDecoder().decode(DataFetcherWorkTypeModal.self, from:data) else { return  }
            
            if workType.code == ApiResponseStatus.kAPI_Result_Success {
               // arrWorkType = workType.type_of_trades!
                if controllerType{
                    let userInfo = AppSignleHelper.shard.login.userinfo?.type_of_trade
                    if userInfo?.count ?? 0 > 0{
                    for i in 0..<workType.type_of_trades!.count {
                        let dict = workType.type_of_trades![i]
                       for item in userInfo!{
                            if item.type_of_trade == dict.type_of_trade {
                                workType.type_of_trades![i].status = true
                            }
                        }
                        }
                    }
                    arrSearch = workType.type_of_trades!
                    tblViewWork.reloadData()
                    setTag()
                }else{
                    arrSearch = workType.type_of_trades!
                    tblViewWork.reloadData()
                }
                 
            } else {
                Util.showAlertWithCallback(AppName, message: workType.message, isWithCancel: false)
            }
        } else if check == ApiAction.TradersAndTradees.kwork_type {
          guard   let workType = try? JSONDecoder().decode(LoginModal.self, from: data) else { return  }
          //  guard let loginModal = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            
            if workType.code == ApiResponseStatus.kAPI_Result_Success {
                //set data in local
                 AppSignleHelper.shard.login = workType
                 UserDefaults.standard.setUserData(value: data)
                  if controllerType {
                         Util.showAlertWithCallback(AppName, message: workType.message, isWithCancel: false){
                             self.navigationController?.popViewController(animated: true)
                         }
                   } else {
                    
                    Util.showAlertWithCallback(AppName, message: workType.message, isWithCancel: false) {
                        let vc = ExperienceViewController.instance(.authentication) as! ExperienceViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    //  self.performSegue(withIdentifier: "pushToWorkTypeViewController", sender: self)
                    }
            }
            }
            
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    //******************************************************************
    // MARK:-  KEYBOARD NOTIFICATION METHODS
    //******************************************************************
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    //UIKeyboardFrameBeginUserInfoKey
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let height = keyboardFrame.height + 150
        scrollViewBg.contentInset.bottom = height
        scrollViewBg.scrollIndicatorInsets.bottom = height
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollViewBg.contentInset.bottom = 0
        scrollViewBg.scrollIndicatorInsets.bottom = 0
        scrollViewBg.scrollRectToVisible(CGRect.zero, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unregisterNotifications()
        
    }
    
}


extension Array where Element: Encodable {
    func asArrayDictionary() throws -> [[String: Any]] {
        var data: [[String: Any]] = []

        for element in self {
            data.append(try element.asDictionary())
        }
        return data
    }
}

extension Encodable {
        func asDictionary() throws -> [String: Any] {
            let data = try JSONEncoder().encode(self)
            guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
                throw NSError()
            }
            return dictionary
        }
}

public func json(from object:Any) -> String? {
       guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
           return nil
       }
       return String(data: data, encoding: String.Encoding.utf8)
   }
