//
//  LoginViewController.swift
//  Traydi
//
//  Created by mac on 10/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

class LoginViewController: TDBaseViewController {
    
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var txtFdEmail: UITextField!
    @IBOutlet weak var txtFdPassword: UITextField!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet weak var scrollViewBG: UIScrollView!
    @IBOutlet weak var btnLogin: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNotifications()
         self.setupToHideKeyboardOnTapOnView()
        
       // self.scrollViewBG.contentSize = CGSize(width: self.view.bounds.size.width, height: 818)
        
        self.navigationController?.isNavigationBarHidden = true
        
        txtFdPassword.isSecureTextEntry = true
        
        self.viewLogin.layer.shadowColor = UIColor.black.cgColor //UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 1).cgColor
        self.viewLogin.layer.shadowOpacity = 0.2
        self.viewLogin.layer.shadowOffset = .zero //CGSize(width: 3, height: 3)
        self.viewLogin.layer.shadowRadius = 20
        self.viewLogin.layer.cornerRadius = 15.0
        
        self.btnLogin.layer.cornerRadius = 25.0
      //  txtFdEmail.text = "sanitizer@gmail.com" // trader
      //  txtFdEmail.text = "traydihp@gmail.com"
        
    // txtFdEmail.text = "traydi@gmail.com"//"him16@gmail.com"  //old
      //   txtFdEmail.text = "him16@gmail.com"  //old
        txtFdEmail.text = "t222@gmail.com"
        txtFdPassword.text = "123456"
     // let device = UIDevice.current.model.split(separator: ",")
     // if (device[0]<"iPhone7")
       // let custombar = self.tabBarController as! CustomTabBarController
           //    custombar.isHiddenTabBar(hidden: true)
        let custombar = self.tabBarController as? CustomTabBarController
               custombar?.isHiddenTabBar(hidden: true)

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushForgotPassSegueIdentifier" {
            //segue.destination
        }else if segue.identifier == "pushRegisterSegueIdentifier" {
            //segue.destination
        }
    }
    
    @IBAction func eyeBtnAction(_ sender: UIButton) {
        txtFdPassword.isSecureTextEntry = !txtFdPassword.isSecureTextEntry
        sender.isSelected = !sender.isSelected
    }
    
      override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          self.view.endEditing(true)
      }
    
    func Validation() -> (message:String, status:Bool) {
        var msg:String = ""
        var status:Bool = true
        if txtFdEmail.text!.isEmpty {
            msg = UseCaseMessage.validation.empty.kemail
            status = false
        } else if txtFdPassword.text!.isEmpty {
            msg = UseCaseMessage.validation.empty.kpassword
          status = false
        } else if !self.isValidEmail(txtFdEmail.text!) {
            msg = UseCaseMessage.validation.empty.kemailValidation
            status = false
        }
        return (message:msg , status:status)
    }
    
    
    
    @IBAction func loginBtnAction(_ sender: Any) {
        let valid = Validation()
        if valid.status {
            APIManager.share.delegate = self
            var params = [String:Any]()
            params[ApiConstants.key.kEmail] = txtFdEmail.text
            params[ApiConstants.key.kPassword] = txtFdPassword.text
            let apiAction = ApiAction.TradersAndTradees.klogin
            APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
        
        } else {
            Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
        }
        
    }
    @IBAction func actionForgotPassword(_ sender: Any) {
      
        let vc = ForgotPassViewController.instance(.authentication) as! ForgotPassViewController
               //self.navigationController?.show(vc, sender: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func registerBtnAction(_ sender: Any) {
        
        let vc = RegisterViewController.instance(.authentication) as! RegisterViewController
       // self.navigationController?.show(vc, sender: nil)
         self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension LoginViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        if check == ApiAction.TradersAndTradees.klogin {
            guard let data = response as? Data else {
                return
            }
            
            guard let loginModal = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            if loginModal.code == ApiResponseStatus.kAPI_Result_Success {
              
                AppSignleHelper.shard.login = loginModal
                if AppSignleHelper.shard.login.userinfo?.user_type == "traders"{
                    UserDefaults.standard.set(false, forKey: "isSeeker")
                }else{
                    UserDefaults.standard.set(true, forKey: "isSeeker")
                }
                   
                UserDefaults.standard.setUserData(value: data)
                UserDefaults.standard.setLoggedIn(value: true)
                UserDefaults.standard.setTouchid(value: true)
                //set tab bar in bottom
            let vc = CustomTabBarController.instance(.tabBarViewController) as! CustomTabBarController
                vc.tabBar.isHidden = true
                UIApplication.shared.windows[0].rootViewController = vc
            } else {
                Util.showAlertWithCallback(AppName, message: loginModal.message, isWithCancel: false)
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
}

extension LoginViewController : UITextFieldDelegate , UIScrollViewDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    //******************************************************************
      // MARK:-  KEYBOARD NOTIFICATION METHODS
      //******************************************************************
      
      func registerNotifications() {
          NotificationCenter.default.addObserver(
              self,
              selector: #selector(self.keyboardWillShow),
              name: UIResponder.keyboardWillShowNotification,
              object: nil)
          
          NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
      }
      
      func unregisterNotifications() {
          NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
          NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
      }
      //UIKeyboardFrameBeginUserInfoKey
      @objc func keyboardWillShow(notification: NSNotification) {
          let userInfo = notification.userInfo ?? [:]
          let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
          let height = keyboardFrame.height + 150
          scrollViewBG.contentInset.bottom = height
          scrollViewBG.scrollIndicatorInsets.bottom = height
      }
      
      @objc func keyboardWillHide(notification: NSNotification) {
          scrollViewBG.contentInset.bottom = 0
          scrollViewBG.scrollIndicatorInsets.bottom = 0
          scrollViewBG.scrollRectToVisible(CGRect.zero, animated: true)
      }
      
      override func viewDidDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
          self.unregisterNotifications()
          
      }
}

