//
//  ExperienceViewController.swift
//  Traydi
//
//  Created by mac on 13/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import DropDown

class ExperienceViewController : UIViewController{
    
    @IBOutlet weak var scrollViewBg: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnWT1: UIButton!
    @IBOutlet weak var btnWT2: UIButton!
    @IBOutlet weak var btnWT3: UIButton!
    @IBOutlet weak var imgViewSkillLevel: UIImageView!
    @IBOutlet weak var btnSelectYears: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var constraintBackHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImageTypeHeight: NSLayoutConstraint!

    
    var controllerType: Bool = false
    let dropDown = DropDown()
    var selectedButtonSkill:UIButton?
    var selectedButtonOpprentice:UIButton?
    var indexYears = 0
    var arrYears = [YearsModel]()
    var isSelectYears : Bool  = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollViewBg.contentSize = CGSize(width: self.view.bounds.size.width, height: 818)
        
        self.btnContinue.layer.cornerRadius = 25.0
        self.imgViewSkillLevel.layer.cornerRadius = 25.0
        self.btnSelectYears.layer.cornerRadius = 20.0

        self.btnYes.layer.cornerRadius = 25.0
        self.btnNo.layer.cornerRadius = 25.0
        self.btnYes.setTitle("Yes", for: .normal)
        self.btnNo.setTitle("No", for: .normal)
        

        

        
        self.btnWT1.setTitle("licence", for: .normal)
        self.btnWT2.setTitle("experience", for: .normal)
        self.btnWT3.setTitle("labour", for: .normal)
       
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
        }else{
            //Traders
            self.lblTitle.text = "Traders"
        }
        if controllerType {
            lblTitle.text = "Edit"
            constraintBackHeight.constant = 0
            constraintImageTypeHeight.constant = 0
            btnContinue.setTitle("Save", for: .normal)
            let tabBarController = self.tabBarController as! CustomTabBarController
            tabBarController.isHiddenTabBar(hidden: true)
            
            //MARK: - Check for apprenrtice
        let  apprentice =     AppSignleHelper.shard.login.userinfo?.apprentice
            if apprentice == "Yes"{
                self.btnYESAction(self.btnYes!)
            }else  if apprentice == "No"{
                 self.btnNOAction(self.btnNo!)
            }
            
            //MARK: - Check for skill level
             let  skill_level =     AppSignleHelper.shard.login.userinfo?.skill_level
            if skill_level == "licence"{
                self.btnWorktype1Action(self.btnWT1!)
            }else if  skill_level == "experience"{
                 self.btnWorktype2Action(self.btnWT2!)
            }
            else if  skill_level == "labour"{
                 self.btnWorktype3Action(self.btnWT3!)
            }
            
        }else{
            self.btnWorktype1Action(self.btnWT1!)
            self.btnYESAction(self.btnYes!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupExperienceYearsApiCall()
    }
    
   func setupExperienceYearsApiCall() {
          APIManager.share.delegate = self
          let apiActions = ApiAction.TradersAndTradees.kyear_of_experience
          APIManager.share.performMaltipartPost(params: [:], apiAction: apiActions)
      }
    
   @IBAction func backBtnAction(_ sender: Any) {
             self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnWorktype1Action(_ sender: UIButton) {
        self.btnWT1.layer.cornerRadius = 25.0
        self.btnWT2.layer.cornerRadius = 25.0
        self.btnWT3.layer.cornerRadius = 25.0
        
        self.btnWT1.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        self.btnWT2.backgroundColor = UIColor.clear
        self.btnWT3.backgroundColor = UIColor.clear
        
        self.btnWT1.setTitleColor(UIColor.white, for: .normal)
        self.btnWT2.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT3.setTitleColor(UIColor.gray, for: .normal)
        
        self.btnWT1.setTitle("licence", for: .normal)
        self.btnWT2.setTitle("experience", for: .normal)
        self.btnWT3.setTitle("labour", for: .normal)
        selectedButtonSkill = sender
        
    }
    @IBAction func btnWorktype2Action(_ sender: UIButton) {
        self.btnWT1.layer.cornerRadius = 25.0
        self.btnWT2.layer.cornerRadius = 25.0
        self.btnWT3.layer.cornerRadius = 25.0
        
        self.btnWT1.backgroundColor = UIColor.clear
        self.btnWT2.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        self.btnWT3.backgroundColor = UIColor.clear
        
        self.btnWT1.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT2.setTitleColor(UIColor.white, for: .normal)
        self.btnWT3.setTitleColor(UIColor.gray, for: .normal)
        selectedButtonSkill = sender
    }
    
    @IBAction func btnWorktype3Action(_ sender: UIButton) {
        self.btnWT1.layer.cornerRadius = 25.0
        self.btnWT2.layer.cornerRadius = 25.0
        self.btnWT3.layer.cornerRadius = 25.0
        
        self.btnWT1.backgroundColor = UIColor.clear
        self.btnWT2.backgroundColor = UIColor.clear
        self.btnWT3.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        
        self.btnWT1.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT2.setTitleColor(UIColor.gray, for: .normal)
        self.btnWT3.setTitleColor(UIColor.white, for: .normal)
        selectedButtonSkill = sender
    }
    
    @IBAction func btnYESAction(_ sender: UIButton) {

        self.btnYes.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        self.btnNo.backgroundColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1)
        self.btnYes.setTitleColor(UIColor.white, for: .normal)
        self.btnNo.setTitleColor(UIColor.gray, for: .normal)
        selectedButtonOpprentice = sender

    }
    @IBAction func btnNOAction(_ sender: UIButton) {
        self.btnYes.backgroundColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1)
        self.btnNo.backgroundColor = UIColor(displayP3Red: 255.0/255.0, green: 106.0/255.0, blue: 44.0/255.0, alpha: 1)
        self.btnYes.setTitleColor(UIColor.gray, for: .normal)
        self.btnNo.setTitleColor(UIColor.white, for: .normal)
        selectedButtonOpprentice = sender
    }
    
    
    @IBAction func actionContinue(_ sender: Any) {
      let valid = Validation()
                 if valid.status {
            // api call for skill level
       let data = AppSignleHelper.shard.login.userinfo
            APIManager.share.delegate = self
            var params = [String:Any]()
        //    params[ApiConstants.key.ktrader_id] =  data?.id ?? "0"
                    if AppSharedData.shared.isSeeker == true{
                                      params[ApiConstants.key.ktradee_id] =  data?.id
                                   }else{
                                      params[ApiConstants.key.ktrader_id] =  data?.id
                                  }
                    
            params[ApiConstants.key.kskill_level] = selectedButtonSkill?.titleLabel?.text
            params[ApiConstants.key.kyear_of_experience] = arrYears[indexYears].years
            params[ApiConstants.key.kapprentice] = selectedButtonOpprentice?.titleLabel?.text
            let apiAction = ApiAction.TradersAndTradees.kskill_level
            APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
     }
    }
    func Validation() -> (message:String, status:Bool){
           var message:String = ""
           var status:Bool = true
        if selectedButtonSkill?.titleLabel?.text == "" {
               status = false
               message = UseCaseMessage.validation.empty.kSkillLevel
           }
        else if selectedButtonOpprentice?.titleLabel?.text == "" {
               status = false
               message = UseCaseMessage.validation.empty.kApprentice
           }
            else if  !isSelectYears{
           status = false
           message = UseCaseMessage.validation.empty.kExp_years
       }
           return (message:message,status:status)
       }

    @IBAction func btnSelectYears(_ sender: UIButton) {
        self.view.endEditing(true)
//        txtYears.text = ""
        
        let objGetData = self.arrYears.map { $0.years }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.direction = .bottom
        dropDown.dataSource = objGetData as! [String]
        dropDown.width = self.view.frame.size.width - 50
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.indexYears = index
            //self.txtYears.text = item
            self.btnSelectYears.setTitle("    \(item)", for: .normal)
            
        }
            dropDown.show()
       }
    
    func setuoDropDownAppearance()  {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 40
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
    }
    
}

extension ExperienceViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kskill_level {
            guard let dataFetcher = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                //set data in local
                  AppSignleHelper.shard.login = dataFetcher
                  UserDefaults.standard.setUserData(value: data)
              
                    Util.showAlertWithCallback(AppName, message: dataFetcher.message, isWithCancel: false) {
                        if self.controllerType {
                            self.navigationController?.popViewController(animated: true)
                                            } else {
                        let vc = BusinessDetailsViewController.instance(.authentication) as! BusinessDetailsViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
              

            }
        } else if check == ApiAction.TradersAndTradees.kyear_of_experience {
            guard let dataFetcher = try? JSONDecoder().decode(YearOfExperienceModel.self, from:data) else { return }
             if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
            if controllerType {
                   
             let  years =      AppSignleHelper.shard.login.userinfo?.year_of_experience
                for obj in dataFetcher.years!{
                    if obj.years == years{
                        if years != "" {
                            self.btnSelectYears.setTitle("    \(years ?? "")", for: .normal)
                             isSelectYears  = true
                        }
//                        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//                            print("Selected item: \(item) at index: \(index)")
//
//                            self.indexYears = index
//                            self.btnSelectYears.setTitle("    \(item)", for: .normal)
//                        }
                    }
                }
                arrYears = dataFetcher.years!
            }else{
                arrYears = dataFetcher.years!
                        if arrYears.count > 0 {
                            let years =  arrYears[0].years
                            if years != "" {
                                self.btnSelectYears.setTitle("    \(years ?? "")", for: .normal)
                                 isSelectYears  = true
                            }
                            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                                print("Selected item: \(item) at index: \(index)")
                                self.indexYears = 0
                                self.btnSelectYears.setTitle("    \(item)", for: .normal)
                            }
                        }
                }
            
            }
            

        }
    }
    
    func errorResponse(error: Any, check: String) {
        
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
}
