//
//  BusinessDetailsViewController.swift
//  Traydi
//
//  Created by mac on 16/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import DropDown

class BusinessDetailsViewController: UIViewController {
    
    @IBOutlet weak var scrollViewBg: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtFdBusinessName: UITextField!
    @IBOutlet weak var txtFdABNnumber: UITextField!
    @IBOutlet weak var txtFdWebsite: UITextField!
    @IBOutlet weak var btnSelectBusinessYears: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var constraintBackHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintImageStepHeight: NSLayoutConstraint!
    var controllerType: Bool = false
    let dropDown = DropDown()
    var arrYears = [YearsModel]()
    var indexYears = 0
    var isSelectYears : Bool  = false
    override func viewDidLoad() {
        super.viewDidLoad()
         registerNotifications()
         setupToHideKeyboardOnTapOnView()
        
        self.txtFdBusinessName.layer.cornerRadius = 20.0
        self.txtFdBusinessName.layer.borderWidth = 1.0
        self.txtFdBusinessName.layer.borderColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1).cgColor
        
        self.txtFdABNnumber.layer.cornerRadius = 20.0
        self.txtFdABNnumber.layer.borderWidth = 1.0
        self.txtFdABNnumber.layer.borderColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1).cgColor
        
        self.txtFdWebsite.layer.cornerRadius = 20.0
        self.txtFdWebsite.layer.borderWidth = 1.0
        self.txtFdWebsite.layer.borderColor = UIColor(displayP3Red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1).cgColor
        
        self.btnSelectBusinessYears.layer.cornerRadius = 20.0
        self.btnContinue.layer.cornerRadius = 25.0
        
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
        }else{
            //Traders
            self.lblTitle.text = "Traders"
        }
        if controllerType {
            lblTitle.text = "Edit"
            constraintBackHeight.constant = 0
            constraintImageStepHeight.constant = 0
            btnContinue.setTitle("Save", for: .normal)
            let tabBarController = self.tabBarController as! CustomTabBarController
            tabBarController.isHiddenTabBar(hidden: true)
            
            txtFdBusinessName.text =  AppSignleHelper.shard.login.userinfo?.business_name
             txtFdABNnumber.text =  AppSignleHelper.shard.login.userinfo?.aBN_CAN
            txtFdWebsite.text = AppSignleHelper.shard.login.userinfo?.website
            
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupExperienceYearsApiCall()
    }
    
    func setupExperienceYearsApiCall() {
        APIManager.share.delegate = self
        let apiActions = ApiAction.TradersAndTradees.kyear_of_experience
        APIManager.share.performMaltipartPost(params: [:], apiAction: apiActions)
    }
    
    func Validation() ->(message:String, status:Bool) {
        var message:String = ""
        var status:Bool = true
        if txtFdBusinessName.text!.isEmpty {
            status = false
            message = UseCaseMessage.validation.empty.kBusinessName
        } else if txtFdABNnumber.text!.isEmpty {
            status = false
            message = UseCaseMessage.validation.empty.kCANNumber
        } else if  txtFdWebsite.text!.isEmpty {
            status = false
            message = UseCaseMessage.validation.empty.kWebSide
        }
        else if  !isSelectYears{
            status = false
            message = UseCaseMessage.validation.empty.kyears
        }
        return (message:message, status:status)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
              self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func contactBtnAction(_ sender: Any) {
//        if controllerType {
//
//        } else {
            let valid = Validation()
            if valid.status {
            let data = AppSignleHelper.shard.login.userinfo
                APIManager.share.delegate = self
                var params = [String:Any]()
                
                params[ApiConstants.key.businessKey.kbusiness_name] = txtFdBusinessName.text
                params[ApiConstants.key.businessKey.kbusiness_experience] = arrYears[indexYears].years
                params[ApiConstants.key.businessKey.kwebsite] = txtFdWebsite.text
                params[ApiConstants.key.businessKey.kABN_CAN] = txtFdABNnumber.text
             //   params[ApiConstants.key.ktrader_id] =  data?.id
                if AppSharedData.shared.isSeeker == true{
                                  params[ApiConstants.key.ktradee_id] =  data?.id
                               }else{
                                  params[ApiConstants.key.ktrader_id] =  data?.id
                              }
                let apiaction = ApiAction.TradersAndTradees.kbusiness_details
                APIManager.share.performMaltipartPost(params: params, apiAction: apiaction)
            } else {
                Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
            }
          
       // }
    }

        @IBAction func btnSelectBusinessYears(_ sender: UIButton) {

            self.view.endEditing(true)
            let objGetData = self.arrYears.map { $0.years }
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
            dropDown.anchorView = sender // UIView or UIBarButtonItem
            dropDown.direction = .bottom
            dropDown.dataSource = objGetData as! [String]
            dropDown.width = self.view.frame.size.width - 50
            
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.indexYears = index
                self.btnSelectBusinessYears.setTitle("    \(item)", for: .normal)
            }
            dropDown.show()
    }
        
        func setuoDropDownAppearance()  {
            let appearance = DropDown.appearance()
            appearance.cellHeight = 40
            appearance.backgroundColor = UIColor(white: 1, alpha: 1)
            appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
            appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
            appearance.cornerRadius = 10
            appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
            appearance.shadowOpacity = 0.9
            appearance.shadowRadius = 25
            appearance.animationduration = 0.25
            appearance.textColor = .darkGray
        }
    
    @IBAction func actionSameMobileNumberUse(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
}
 //MARK:- Response apis
extension BusinessDetailsViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kbusiness_details { //DataFetcherModal
            guard let dataFetcher = try? JSONDecoder().decode(LoginModal.self, from:data) else { return }
            print(dataFetcher)
            if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                //set data in local
                AppSignleHelper.shard.login = dataFetcher
                 UserDefaults.standard.setUserData(value: data)
                
                Util.showAlertWithCallback(AppName, message: dataFetcher.message, isWithCancel: false) {
                    if self.controllerType {
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        let vc = SkillsViewController.instance(.authentication) as! SkillsViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    
                }
                
            }
        } else if check == ApiAction.TradersAndTradees.kyear_of_experience {
            guard let dataFetcher = try? JSONDecoder().decode(YearOfExperienceModel.self, from:data) else { return }
            
             if controllerType {
                   let  years =  AppSignleHelper.shard.login.userinfo?.business_experience
                
                for obj in dataFetcher.years!{
                    if obj.years == years{
                        if years != "" {
                            self.btnSelectBusinessYears.setTitle("    \(years ?? "")", for: .normal)
                            isSelectYears  = true
                            break
                        }
                      
                    }
                }
                  arrYears = dataFetcher.years!
             }else{
             arrYears = dataFetcher.years!
             if arrYears.count > 0{
                 let year = arrYears[0].years
                 if year != ""{
                     isSelectYears  = true
                     self.btnSelectBusinessYears.setTitle("    \(year ?? "")", for: .normal)
                 }
             }
           }
        }
    }
    func errorResponse(error: Any, check: String) {
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
}


extension BusinessDetailsViewController : UITextFieldDelegate {
     
     
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         
         if textField == txtFdBusinessName {
             txtFdABNnumber.becomeFirstResponder()
         }else if textField == txtFdABNnumber{
            txtFdWebsite.becomeFirstResponder()
         }else if textField == txtFdWebsite{
            txtFdWebsite.resignFirstResponder()
         }else{
             textField.resignFirstResponder()
         }
         return true
     }
     
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          let rawString = string
          let range1 = rawString.rangeOfCharacter(from: .whitespaces)
          if ((textField.text?.count)! == 0 && range1  != nil)
              || ((textField.text?.count)! > 0  && range1 != nil && textField != txtFdBusinessName && textField != txtFdABNnumber  && textField != txtFdWebsite)  {
              return false
          }
          let length = (textField.text?.count)! + string.count - range.length
          
             if txtFdBusinessName == textField  {
                    
                    let validCharacterSet = NSCharacterSet(charactersIn: APPConstants.AddressAcceptableCharacter).inverted
                    let filter = string.components(separatedBy: validCharacterSet)
                    if filter.count == 1 {
                        
                        return (length > 60) ? false : true
                    } else {
                        return false
                    }
                }
                else if textField == txtFdABNnumber {
                    let validCharacterSet = NSCharacterSet(charactersIn: APPConstants.phoneNoAcceptableCharacter).inverted
                    let filter = string.components(separatedBy: validCharacterSet)
                    if filter.count == 1 {
                        
                        return (length > 20) ? false : true
                    } else {
                        return false
                    }
                }
                    
                else if textField == txtFdWebsite {
                    let validCharacterSet = NSCharacterSet(charactersIn: APPConstants.emailAcceptableCharacter).inverted
                    let filter = string.components(separatedBy: validCharacterSet)
                    if filter.count == 1 {
                        
                        return (length > 60) ? false : true
                    } else {
                        return false
                    }
                }
                return true
          
          
      }
     
     
     override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         self.view.endEditing(true)
     }
    
    
    
    
    //******************************************************************
    // MARK:-  KEYBOARD NOTIFICATION METHODS
    //******************************************************************
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    //UIKeyboardFrameBeginUserInfoKey
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let height = keyboardFrame.height + 150
        scrollViewBg.contentInset.bottom = height
        scrollViewBg.scrollIndicatorInsets.bottom = height
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollViewBg.contentInset.bottom = 0
        scrollViewBg.scrollIndicatorInsets.bottom = 0
        scrollViewBg.scrollRectToVisible(CGRect.zero, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unregisterNotifications()
        
    }
    
}
