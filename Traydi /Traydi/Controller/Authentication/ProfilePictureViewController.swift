//
//  ProfilePictureViewController.swift
//  Traydi
//
//  Created by mac on 13/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//
import UIKit
import  SDWebImage

class ProfilePictureViewController: UIViewController{
    
    @IBOutlet weak var imgDocument: TYImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var scrollViewBg: UIScrollView!
    @IBOutlet weak var viewFields: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnTakeImage: UIButton!
    @IBOutlet weak var btnGallary: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var controllerType:Bool = false
    var imagePicker = UIImagePickerController()
    var arrDocument = [[String:Any]]()
    var imagepickerDocument:TDImagePicker!
    var profileData = [String:Any]()
    
    @IBOutlet weak var tableConstraint: NSLayoutConstraint!
    @IBOutlet weak var ConstraintImagestep: NSLayoutConstraint!
    @IBOutlet weak var backConstraintHeight: NSLayoutConstraint!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  btnContinue.isEnabled  = false
        
        self.scrollViewBg.contentSize = CGSize(width: self.view.bounds.width, height: 818)

        self.viewFields.layer.cornerRadius = 15.0
       
        imagepickerDocument = TDImagePicker(presentationController: self, delegate: self)
        
        self.btnTakeImage.layer.cornerRadius = 25.0
        self.btnGallary.layer.cornerRadius = 25.0
        
        self.btnTakeImage.setTitle("TAKE IMAGES", for: .normal)
        self.btnGallary.setTitle("GALLERY", for: .normal)
        self.btnTakeImage.setTitleColor(UIColor.white, for: .normal)
        self.btnGallary.setTitleColor(UIColor.gray, for: .normal)
        self.btnContinue.layer.cornerRadius = 25.0
        self.imgDocument.layer.cornerRadius = 6
        
       
        imagePicker.delegate = self
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
        }else{
            self.lblTitle.text = "Traders"
        }
        if controllerType {
           let userInfo = AppSignleHelper.shard.login.userinfo
              imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
             imgProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + userInfo!.image!), placeholderImage: UIImage(named: "camerawhite"))
             imgDocument.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            imgDocument.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + userInfo!.verification_id!), placeholderImage: UIImage(named: "images"))
            
          /*  let url = URL(string:ImagesUrl.baseProfileUri + userInfo!.image!)
            if let data = try? Data(contentsOf: url!)
            {
                let image: UIImage = UIImage(data: data)!
                self.profileData[ApiConstants.key.kimagee] = image
            }
            
            let url2 = URL(string:ImagesUrl.baseProfileUri + userInfo!.verification_id!)
                      if let data2 = try? Data(contentsOf: url2!)
                      {
                          let image2: UIImage = UIImage(data: data2)!
                          self.arrDocument.removeAll()
                          self.arrDocument.append([ApiConstants.key.kverification_id:image2 as Any])
                      }*/
            
          DispatchQueue.global(qos: .background).async {
                do
                {
                    if userInfo!.image != ""{
                    let data = try Data.init(contentsOf: URL.init(string:ImagesUrl.baseProfileUri + userInfo!.image!)!)
                    DispatchQueue.main.async {
                        let image: UIImage = UIImage(data: data)!
                        print(image)
                        self.profileData[ApiConstants.key.kimagee] = image
                         self.btnContinue.isEnabled  = true
                    }
            }
                }
                catch {
                }
                
                //2
                do
                {
                    if userInfo!.verification_id != ""{
                    let data = try Data.init(contentsOf: URL.init(string:ImagesUrl.baseProfileUri + userInfo!.verification_id!)!)
                    DispatchQueue.main.async {
                        let image: UIImage = UIImage(data: data)!
                        self.arrDocument.removeAll()
                        self.arrDocument.append([ApiConstants.key.kverification_id:image as Any])
                        self.btnContinue.isEnabled  = true
                    }
                    }
                }
                catch {
                }
            }
            
            
            lblTitle.text = "Edit"
            ConstraintImagestep.constant = 0
            backConstraintHeight.constant = 0
            btnEdit.isHidden = false
            let tabBarController = self.tabBarController as! CustomTabBarController
            tabBarController.isHiddenTabBar(hidden: true)
            btnContinue.setTitle("Save", for: .normal)
            
        }else{
             self.btnContinue.isEnabled  = true
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableConstraint.constant = tableView.contentSize.height
            view.layoutIfNeeded()
    }
  
    func Validation() -> (message:String, status:Bool){
        var message:String = ""
        var status:Bool = true
        if profileData.isEmpty {
            status = false
            message = UseCaseMessage.validation.empty.kProfileImage
        }
        if arrDocument.isEmpty {
            status = false
            message = UseCaseMessage.validation.empty.kdocImage
        }
        return (message:message,status:status)
    }
    
    @IBAction func acionEdit(_ sender: Any) {
        
    }
    @IBAction func actionContinue(_ sender: Any) {
  //      if controllerType {
           
     //   } else {
            let valid = Validation()
            if valid.status {
                Loader.showLoader("Loading....")
                APIManager.share.delegate = self
                arrDocument.append(profileData)
                var params = [String:Any]()
                print(arrDocument)
               // params[ApiConstants.key.ktrader_id] = AppSignleHelper.shard.login.userinfo?.id
                if AppSharedData.shared.isSeeker == true{
                                   params[ApiConstants.key.ktradee_id] =  AppSignleHelper.shard.login.userinfo?.id
                                }else{
                                   params[ApiConstants.key.ktrader_id] =  AppSignleHelper.shard.login.userinfo?.id
                               }
                print(params)
                let apiAction = ApiAction.TradersAndTradees.kprofile_picture
                APIManager.share.performImageUpload(params: params, apiAction: apiAction, imageParams: arrDocument)
            } else {
                Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
            }
            
     //   }
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTakeImageAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func btnGallaryAction(_ sender: Any) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func uploadVerificationIDBtnAction(_ sender: UIButton) {
        print("upload verfication id button clicked")
        imagepickerDocument.present(from: sender)
        
    }
}

extension ProfilePictureViewController : TDImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if image == nil{
            return
        }
        arrDocument.removeAll()
        arrDocument.append([ApiConstants.key.kverification_id:image as Any])
        imgDocument.image = image
        imgProfile.contentMode = .scaleAspectFill
      /*  let indexPath = IndexPath(row: arrDocument.count - 1, section: 0)
        self.tableView.insertRows(at: [indexPath], with: .bottom)
        tableConstraint.constant =   CGFloat(arrDocument.count  * 115)
               view.layoutIfNeeded()
       
        tableView.reloadData()*/
    }
}

extension ProfilePictureViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        imgProfile.image = image
        imgProfile.contentMode = .scaleAspectFill
        profileData[ApiConstants.key.kimagee] = image
        print(profileData)
        self.dismiss(animated: true, completion: nil)
    }
}

    //MARK:- Response apis 
extension ProfilePictureViewController: ApiManagerDelegate  {
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        if check == ApiAction.TradersAndTradees.kprofile_picture {
            guard let data = response as? Data else {
                return
            }
             guard let loginModal = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            if loginModal.code == ApiResponseStatus.kAPI_Result_Success {
                AppSignleHelper.shard.login = loginModal
                UserDefaults.standard.setUserData(value: data)
                if controllerType{
                    Util.showAlertWithCallback(AppName, message: loginModal.message, isWithCancel: false){
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    Util.showAlertWithCallback(AppName, message: loginModal.message, isWithCancel: false) {
                        let vc = WorkTypeViewController.instance(.authentication) as! WorkTypeViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
}

extension ProfilePictureViewController: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDocument.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TDProfilePictureTableViewCell", for: indexPath) as! TDProfilePictureTableViewCell
        cell.ImgDocument.image = arrDocument[indexPath.row][ApiConstants.key.kvalidation_id] as? UIImage
        let imageName = UUID().uuidString + ".png"
        cell.lbldocument.text = imageName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
