//
//  SkillsViewController.swift
//  Traydi
//
//  Created by mac on 16/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class SkillsViewController: UIViewController ,TagListViewDelegate {
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSelectedSkills: UILabel!
    @IBOutlet weak var searchBarSkills: UISearchBar!
    @IBOutlet weak var collectionViewTag: UICollectionView!
    @IBOutlet weak var tableViewSuggestedSkills: UITableView!
    @IBOutlet weak var constraintBackHeight: NSLayoutConstraint!
    @IBOutlet weak var ConstraintImageStepType: NSLayoutConstraint!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tagListView: TagListView!
    var controllerType:Bool = false
    var arrSuggestedModal = [SuggestedModal]()
    var strSkill = [String]()
    @IBOutlet weak var txtSearch: UITextField!
    var arrSearch = [SuggestedModal]()
    @IBOutlet weak var constHeightTable: NSLayoutConstraint!
    @IBOutlet weak var lblSkillRequiredMsg: UILabel!
    var arrSearchFinal = [SuggestedModal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollview.contentSize = CGSize(width: self.view.frame.width, height: 818)
        self.tableViewSuggestedSkills.allowsMultipleSelection = true
        self.btnContinue.layer.cornerRadius = 25.0
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
        }else{
            //Traders
            self.lblTitle.text = "Traders"
        }
        if controllerType {
            lblTitle.text = "Edit"
            constraintBackHeight.constant = 0
            ConstraintImageStepType.constant = 0
            btnContinue.setTitle("Save", for: .normal)
            let tabBarController = self.tabBarController as! CustomTabBarController
            tabBarController.isHiddenTabBar(hidden: true)
        }
        self.setupSuggestedApiCall()
        tagListView.delegate = self
        tagListView.layer.cornerRadius = 6
        tagListView.layer.borderColor =  UIColor.lightGray.cgColor
        tagListView.layer.borderWidth = 1
        
        
        txtSearch.delegate = self
        txtSearch.addTarget(self, action: #selector(SearchDidChange(_:)), for: .editingChanged)
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionContine(_ sender: Any) {
      
            if arrSearchFinal.count == 0 {
                Util.showAlertWithCallback(AppName, message: UseCaseMessage.validation.empty.kWorkerSkills, isWithCancel: false)
                return
            }
            if arrSearchFinal.count < 2 {
                Util.showAlertWithCallback(AppName, message: UseCaseMessage.validation.empty.kWorkerSkillsContinue, isWithCancel: false)
                
                return
            }
            // let data = AppSignleHelper.shard.login.userinfo
            APIManager.share.delegate = self
            let actionSKillNeeds = ApiAction.TradersAndTradees.kskill_needed
            var params = [String:Any]()
            let jsonEncoder = JSONEncoder()
            let jsonData = try! jsonEncoder.encode(arrSearchFinal)
            let json = String(data: jsonData, encoding:  String.Encoding.utf8)
            let data = AppSignleHelper.shard.login
            params[ApiConstants.key.ksuggested_skills] = json
          //  params[ApiConstants.key.ktrader_id] = data?.userinfo?.id ?? "0"
        if AppSharedData.shared.isSeeker == true{
                          params[ApiConstants.key.ktradee_id] =  data?.userinfo?.id ?? "0"
                       }else{
                          params[ApiConstants.key.ktrader_id] =  data?.userinfo?.id ?? "0"
                      }
            APIManager.share.performMaltipartPost(params: params, apiAction: actionSKillNeeds)
       
    }
    
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        arrSearchFinal = arrSearchFinal.filter{$0.skills != title}
        for i in 0..<arrSearch.count {
            let dict = arrSearch[i]
            if dict.skills == title {
                arrSearch[i].selectable = "false"
                self.tableViewSuggestedSkills.reloadData()
            }
            
        }
        if arrSearchFinal.count == 0 {
            lblSkillRequiredMsg.isHidden = false
        }else{
            lblSkillRequiredMsg.isHidden = true
        }
        lblSelectedSkills.text = "\(arrSearchFinal.count) Selected"
        tableViewSuggestedSkills.reloadData()
        
        sender.removeTagView(tagView)
    }
    
}

extension SkillsViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "SkillCellIdentifier")
        if cell == nil{
            cell = UITableViewCell(style: .default, reuseIdentifier: "SkillCellIdentifier")
        }
        // Configure the cell...
        let obj = arrSearch[indexPath.row]
        if obj.selectable == "true"{
            cell?.textLabel?.textColor = appColor
            cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
        }else{
            cell?.textLabel?.textColor = .darkGray
            cell?.accessoryType = UITableViewCell.AccessoryType.none
        }
        
        cell?.textLabel?.text = obj.skills
        return cell!
    }
}
extension SkillsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // var obj = arrSearch[indexPath.row]
        if arrSearch[indexPath.row].selectable == "true"{
            arrSearch[indexPath.row].selectable = "false"
            for item in arrSearchFinal {
                if item.skills == arrSearch[indexPath.row].skills {
                    arrSearchFinal = arrSearchFinal.filter{ $0.skills != item.skills}
                }
            }
            print(arrSearch)
            print(arrSearchFinal)
        }else{
            arrSearch[indexPath.row].selectable = "true"
            arrSearchFinal.append(arrSearch[indexPath.row])
        }
        if arrSearchFinal.count == 0 {
            lblSkillRequiredMsg.isHidden = false
        }else{
            lblSkillRequiredMsg.isHidden = true
        }
        lblSelectedSkills.text = "\(arrSearchFinal.count) Selected"
        tableViewSuggestedSkills.reloadData()
        setTag()
    }
    
    
    
    func setTag(){
        strSkill.removeAll()
        tagListView.removeAllTags()
        for dict in arrSearchFinal{
            if dict.selectable == "true"{
                strSkill.append(dict.skills!)
            }
        }
        
        tagListView.addTags(strSkill)
    }
}

// MARK: - Textfield delegate
extension SkillsViewController: UITextFieldDelegate{
    
    @objc func SearchDidChange(_ textField: UITextField) {
        let textToSearch = textField.text ?? ""
        if textToSearch != "" {
            let filteredContacts = self.arrSuggestedModal.filter({$0.skills?.uppercased().contains(textToSearch.uppercased()) == true })
            arrSearch = filteredContacts
            if arrSearchFinal.count > 0 {
                arrSearch = arrSearchFinal
            }
            if  filteredContacts.count > 0 {
                arrSearch.removeAll()
                for dictLocal in filteredContacts {
                    var isFound = false
                    for dict in arrSearchFinal {
                        if dict.skills == dictLocal.skills {
                            isFound = true
                            if dict.selectable == "true"  {
                                arrSearch.append(dict)
                            }
                        }
                    }
                    if !isFound{
                        arrSearch.append(dictLocal)
                    }
                }
            }else{
                arrSearchFinal = arrSearch
            }
            
        }
        else{
            arrSearch.removeAll()
            for dictLocal in self.arrSuggestedModal{
                var isFound = false
                for dict in arrSearchFinal {
                    if dict.skills == dictLocal.skills {
                        isFound = true
                        if dict.selectable == "true"  {
                            arrSearch.append(dict)
                        }
                    }
                }
                if !isFound{
                    arrSearch.append(dictLocal)
                }
            }
        }
        if arrSearchFinal.count == 0 {
            lblSkillRequiredMsg.isHidden = false
        }else{
            lblSkillRequiredMsg.isHidden = true
        }
        lblSelectedSkills.text = "\(arrSearchFinal.count) Selected"
        constHeightTable.constant = CGFloat(arrSearch.count * 50)
        tableViewSuggestedSkills.reloadData()
    }
}

//MARK:- Response
extension SkillsViewController : ApiManagerDelegate {
    func setupTagsApiCall() {
        APIManager.share.delegate = self
        let apiAction = ApiAction.TradersAndTradees.kTags
        APIManager.share.performActionGetApi(params: [:], apiAction: apiAction)
    }
    
    
    func setupSuggestedApiCall() {
        APIManager.share.delegate = self
        let apiAction = ApiAction.TradersAndTradees.kSuggested_skills
        let trade_needed_id =     AppSignleHelper.shard.login.userinfo?.type_of_trade
         let keys = trade_needed_id?.map { $0.id }
        let formattedArray = (keys!.map{String($0!)}).joined(separator: ",")
        
        APIManager.share.performMaltipartPost(params: ["trade":formattedArray as Any], apiAction: apiAction)
    }
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kSuggested_skills {
            guard var dataFetcher = try? JSONDecoder().decode(DataFetcherSuggestedSkills.self, from:data) else { return }
            if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                arrSuggestedModal = dataFetcher.suggested_skills!//
                arrSearch.removeAll()
                let userInfo = AppSignleHelper.shard.login.userinfo?.suggested_skills
                if userInfo?.count ?? 0 > 0{
                    for i in 0..<dataFetcher.suggested_skills!.count {
                        let dict = dataFetcher.suggested_skills![i]
                        for item in userInfo!{
                            if item.skills == dict.skills {
                                dataFetcher.suggested_skills![i].selectable = "true"
                            }
                        }
                    }
                }
                arrSearch =  dataFetcher.suggested_skills!
                arrSearchFinal = arrSearch.filter{$0.selectable == "true"}
                if arrSearchFinal.count == 0 {
                    lblSkillRequiredMsg.isHidden = false
                }else{
                    lblSkillRequiredMsg.isHidden = true
                }
                constHeightTable.constant = CGFloat(50 * arrSearch.count)
                tableViewSuggestedSkills.reloadData()
                setTag()
            }
        } else  if check == ApiAction.TradersAndTradees.kTags  {
            guard let dataFetcher = try? JSONDecoder().decode(DataFetcherTagesModal.self, from:data) else { return }
            //   arrTagsModal = dataFetcher.tagsList!
            collectionViewTag.reloadData()
            setupSuggestedApiCall()
        } else if  check == ApiAction.TradersAndTradees.kskill_needed {
            guard let dataFetcher = try? JSONDecoder().decode(LoginModal.self, from:data) else { return }
            if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                //set data in local
                AppSignleHelper.shard.login = dataFetcher
                 UserDefaults.standard.setUserData(value: data)
                Util.showAlertWithCallback(AppName, message: dataFetcher.message, isWithCancel: false) {
                    if self.controllerType {
                        self.navigationController?.popViewController(animated: true)
                          } else {
                        let vc = WorkRadiusViewController.instance(.authentication) as! WorkRadiusViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                 
                }
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
}

extension SkillsViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
