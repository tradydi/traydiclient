//
//  CredentialListViewController.swift
//  Traydi
//
//  Created by mac on 20/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class CredentialListViewController: UIViewController {
    
    @IBOutlet weak var btnAddCredential: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tblViewCredential: UITableView!
    @IBOutlet weak var constraintImageStepHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintBackHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var controllerType:Bool = false
    var arrCredentialModal = [CredentialModal]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnAddCredential.layer.cornerRadius = 25.0
        self.btnContinue.layer.cornerRadius = self.btnContinue.frame.size.height / 2
        if AppSharedData.shared.isSeeker == true{
            self.lblTitle.text  = "Tradees"
        }else{
            //Traders
            self.lblTitle.text = "Traders"
        }
        if controllerType {
            lblTitle.text = "Edit"
            constraintBackHeight.constant = 0
            constraintImageStepHeight.constant = 0
            btnContinue.setTitle("Save", for: .normal)
            let tabBarController = self.tabBarController as! CustomTabBarController
            tabBarController.isHiddenTabBar(hidden: true)
        }
        self.setupCredentialApiCall()
    }
    
    func setupCredentialApiCall() {
        var params = [String:Any]()
        params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id ?? "1"
        APIManager.share.delegate = self
        let apiAction = ApiAction.TradersAndTradees.kcredentials_list
        APIManager.share.performMaltipartPost(params: params, apiAction:apiAction)
        
    }
    @IBAction func addCredentialBtnAction(_ sender: Any) {
        if controllerType {
            self.performSegue(withIdentifier: "pushEditToAddCredentialViewController", sender: controllerType)
        } else {
            self.performSegue(withIdentifier: "presentToAddCredentialViewController", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushEditToAddCredentialViewController" {
            let vc = segue.destination as! AddCredentialViewController
            vc.controllerType = sender as! Bool
            vc.delegate = self
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueBtnAction(_ sender: Any) {
        
        let vc = CustomTabBarController.instance(.tabBarViewController) as! CustomTabBarController
        //vc.hidesBottomBarWhenPushed = true
        vc.tabBar.isHidden = true
        UIApplication.shared.windows[0].rootViewController = vc
        //self.navigationController?.setViewControllers([vc], animated: true)
    }
}
extension CredentialListViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCredentialModal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell = self.tblViewCredential.dequeueReusableCell(withIdentifier: "TDCredentialTableViewCell") as! TDCredentialTableViewCell
        let dict = arrCredentialModal[indexPath.row]
        cell.lblTitle.text = dict.title ?? ""
        cell.lblProvider.text = dict.provider ?? ""
        cell.lblNumber.text = dict.number ?? ""
        cell.lblDate.text = dict.expiry_date ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension CredentialListViewController: AddCredentialDelegate {
    func actionCreadentialList(items: [CredentialModal]) {
        arrCredentialModal = items
        tableView.reloadData()
    }
}

extension CredentialListViewController: ApiManagerDelegate {
    
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        if check == ApiAction.TradersAndTradees.kcredentials_list {
            guard let data = response as? Data else {
                return
            }
            guard let dataFetcher = try? JSONDecoder().decode(DataFetcherCredentialsModal.self, from:data) else { return }
            if dataFetcher.code == ApiResponseStatus.kAPI_Result_Success {
                arrCredentialModal = dataFetcher.credentials!
                tableView.reloadData()
            }
        }
    }
    
    
    func errorResponse(error: Any, check: String) {
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
}
