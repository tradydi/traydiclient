//
//  RegisterViewController.swift
//  Traydi
//
//  Created by mac on 11/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//
import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollViewBg: UIScrollView!
    @IBOutlet weak var viewInputFields: UIView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var txtFdPassword: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupToHideKeyboardOnTapOnView()
//        self.viewInputFields.layer.shadowColor = UIColor.black.cgColor
//        self.viewInputFields.layer.shadowOpacity = 0.2
//        self.viewInputFields.layer.shadowOffset = .zero //CGSize(width: 3, height: 3)
//        self.viewInputFields.layer.shadowRadius = 20
//        self.viewInputFields.layer.cornerRadius = 15.0
        self.viewInputFields.addShadowAllSide()
        
        self.btnRegister.layer.cornerRadius = 25.0
       // self.scrollViewBg.contentSize = CGSize(width: self.view.bounds.size.width, height: 818+300)
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
        let cCode =     getCountryCallingCode(countryRegionCode: countryCode)
            btnCountryCode.setTitle("+" + cCode, for: .normal)
            
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
        txtConfirmPassword.autocorrectionType = .no
         txtFdPassword.autocorrectionType = .no
        txtFdPassword.textContentType = .none
        txtConfirmPassword.textContentType = .none
        
        let custombar = self.tabBarController as? CustomTabBarController
        custombar?.isHiddenTabBar(hidden: true)

    }
    
    func countryCodee(from countryName: String) -> String {
        return NSLocale.isoCountryCodes.first { (code) -> Bool in
            let name = NSLocale.current.localizedString(forRegionCode: code)
            return name == countryName
            } ?? ""
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushVerifyNumberSegueIdentifier" {
            //segue.destination
//            let vc = VerifyNumberViewController.instance(.authentication) as! VerifyNumberViewController
//            vc.modelLoginModal = sender as? LoginModal
            
        }
    }
    @IBAction func loginBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }//
    @IBAction func eyeBtnAction(_ sender: UIButton) {
        txtFdPassword.isSecureTextEntry = !txtFdPassword.isSecureTextEntry
        txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
        sender.isSelected = !sender.isSelected
    }
    
    //MARK:- Validation TextFields  btnCountryCode
    func Validation() -> (message:String, status:Bool) {
        var message:String = ""
        var status:Bool = true
        if txtPhoneNumber.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kPhoneNumberTextField
            status = false
        } else if txtPhoneNumber.text!.count  < MinPhoneNumberLength{
            message = UseCaseMessage.validation.empty.kPhoneNumberValidation
            status = false
        }else if btnCountryCode.titleLabel?.text! == "" {
            message = UseCaseMessage.validation.empty.kCountryCodeTextField
            status = false
        }
        else if txtFullName.text!.isEmpty {
            message =  UseCaseMessage.validation.empty.kFullNameTextField
            status = false
        } else if txtEmail.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kemail
            status = false
        }else if !self.isValidEmail(txtEmail.text!) {
            message = UseCaseMessage.validation.empty.kemailValidation
            status = false
        } else if txtCity.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kcityTextField
            status = false
        } else if txtFdPassword.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kpassword
            status = false
        } else if txtConfirmPassword.text!.isEmpty {
            message = UseCaseMessage.validation.empty.kConfirmPasswordTextField
            status = false
        } else if txtFdPassword.text != txtConfirmPassword.text {
            message = UseCaseMessage.validation.empty.kPasswordNotMatch
            status = false
        }
        return (message:message, status: status)
    }
    
    //MARK: UITextFieldDelegate
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtPhoneNumber {
            txtFullName.becomeFirstResponder()
        }else if textField == txtFullName{
            txtEmail.becomeFirstResponder()
        }else if textField == txtEmail{
            txtCity.becomeFirstResponder()
        }
        else if textField == txtCity{
            txtFdPassword.becomeFirstResponder()
        }
        else if textField == txtFdPassword{
            txtConfirmPassword.becomeFirstResponder()
        }
        else if textField == txtConfirmPassword{
            textField.resignFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         let rawString = string
         let range1 = rawString.rangeOfCharacter(from: .whitespaces)
         if ((textField.text?.count)! == 0 && range1  != nil)
             || ((textField.text?.count)! > 0  && range1 != nil && textField != txtPhoneNumber && textField != txtFullName  && textField != txtEmail && textField != txtCity && textField != txtFdPassword  && textField != txtConfirmPassword)  {
             return false
         }
         let length = (textField.text?.count)! + string.count - range.length
         
      
               
               if txtPhoneNumber == textField {
                   
                   let validCharacterSet = NSCharacterSet(charactersIn: "0123456789").inverted
                   let filter = string.components(separatedBy: validCharacterSet)
                   if filter.count == 1 {
                       
                       return (length > PhoneNumberLength) ? false : true
                   } else {
                       return false
                   }
               }else if txtFullName == textField || txtCity == textField {
                   
                   let validCharacterSet = NSCharacterSet(charactersIn: APPConstants.NameAcceptableCharacter).inverted
                   let filter = string.components(separatedBy: validCharacterSet)
                   if filter.count == 1 {
                       
                       return (length > 30) ? false : true
                   } else {
                       return false
                   }
               }
               else if textField == txtEmail {
                   let validCharacterSet = NSCharacterSet(charactersIn: APPConstants.emailAcceptableCharacter).inverted
                   let filter = string.components(separatedBy: validCharacterSet)
                   if filter.count == 1 {
                       
                       return (length > 60) ? false : true
                   } else {
                       return false
                   }
               }
                   
               else if textField == txtFdPassword || textField == txtConfirmPassword {
                   return (length > 40) ? false : true
               }
               return true
         
         
     }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /*override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           let touch = touches.first
           if touch?.view == viewContainer {
               self.view.endEditing(true)
           }
       }*/
    
    
    @IBAction func actionCountryPicker(_ sender: Any) {
        let countryPicker = TDCountryPicker(style: .grouped)
        countryPicker.delegate = self
        
        // Display calling codes
        countryPicker.showCallingCodes = false
    
         let pickerNavigationController = UINavigationController(rootViewController: countryPicker)
        self.present(pickerNavigationController, animated: true, completion: nil)
    }
    
    @IBAction func actionRegister(_ sender: Any) {
        let valid = Validation()
        if valid.status {
            var params = [String:Any]()
            params[ApiConstants.key.registerConstants.kEmail] = txtEmail.text
            params[ApiConstants.key.registerConstants.kfull_name] = txtFullName.text
            params[ApiConstants.key.registerConstants.kmobile_number] = txtPhoneNumber.text
            params[ApiConstants.key.registerConstants.kdevice_type] = kDevicePlatform
            params[ApiConstants.key.registerConstants.kcountry_code] = btnCountryCode.titleLabel?.text!
            params[ApiConstants.key.registerConstants.kPassword] = txtConfirmPassword.text
            params[ApiConstants.key.registerConstants.kdevice_token] = appDelegate.shared.deviceToken
            APIManager.share.delegate = self
            let actionApi = ApiAction.TradersAndTradees.ksignup
            APIManager.share.performMaltipartPost(params: params, apiAction: actionApi)
        } else {
            Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
        }
    }
    
    
}

extension RegisterViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        Loader.hideLoader()
        if check == ApiAction.TradersAndTradees.ksignup {
            guard let data = response as? Data else {
                return
            }
            guard let loginModal = try? JSONDecoder().decode(LoginModal.self, from:data) else { return  }
            if loginModal.code == ApiResponseStatus.kAPI_Result_Success {
                AppSignleHelper.shard.login = loginModal
                let vc = VerifyNumberViewController.instance(.authentication) as! VerifyNumberViewController
                vc.modelLoginModal =  loginModal
                self.navigationController?.pushViewController(vc, animated: true)
               // self.performSegue(withIdentifier: "pushVerifyNumberSegueIdentifier", sender: loginModal)
            } else {
                Util.showAlertWithCallback(AppName, message: loginModal.message, isWithCancel: false)
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}

extension RegisterViewController: TDCountryPickerDelegate {
    
    func countryPicker(_ picker: TDCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        _ = picker.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
       // let flag =  picker.getFlag(countryCode: code)
        btnCountryCode.setTitle(dialCode, for: .normal)
     //   btnRegister.setImage(flag, for: .normal)
    }
    
    //******************************************************************
    // MARK:-  KEYBOARD NOTIFICATION METHODS
    //******************************************************************
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    //UIKeyboardFrameBeginUserInfoKey
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let height = keyboardFrame.height + 150
        scrollViewBg.contentInset.bottom = height
        scrollViewBg.scrollIndicatorInsets.bottom = height
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollViewBg.contentInset.bottom = 0
        scrollViewBg.scrollIndicatorInsets.bottom = 0
        scrollViewBg.scrollRectToVisible(CGRect.zero, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unregisterNotifications()
        
    }

    

    
    func getCountryCallingCode(countryRegionCode:String)->String{
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263" , "":""]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
    }
}

extension UIViewController
{
    func setupToHideKeyboardOnTapOnView()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

extension UITextField{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

extension UIView{
    func addShadowAllSide()  {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = .zero //CGSize(width: 3, height: 3)
        self.layer.shadowRadius = 20
        self.layer.cornerRadius = 15.0
    }
    }
