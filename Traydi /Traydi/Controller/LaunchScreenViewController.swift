
//
//  LaunchScreen2ViewController.swift
//  Traydi
//
//  Created by mac on 01/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class LaunchScreenViewController: TDBaseViewController {
    @IBOutlet weak var imglaunch: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imglaunch.image = UIImage(named: "Splash.png")

        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
            // your code here
           self.performSegue(withIdentifier: "goToGuide01ViewController", sender: false)
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
