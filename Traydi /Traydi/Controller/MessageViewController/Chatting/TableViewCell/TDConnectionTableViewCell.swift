//
//  TDConectionTableViewCell.swift
//  Traydi
//
//  Created by mac on 18/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation
import UIKit

protocol RequestConnectionDelegate: NSObject {
    func handlePendingRequest(cell:TDConnectionTableViewCell)
    func handleAcceptRequest(cell:TDConnectionTableViewCell)
    func handleCancelRequest(cell:TDConnectionTableViewCell)
}

class TDConnectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewPending: UIView!
    @IBOutlet weak var btnAccepts: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    weak var delegate:RequestConnectionDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionAccept(_ sender: Any) {
        delegate?.handleAcceptRequest(cell: self)
    }
    @IBAction func actionCancel(_ sender: Any) {
        delegate?.handleCancelRequest(cell: self)
    }
    @IBAction func actionPending(_ sender: Any) {
        delegate?.handleCancelRequest(cell: self)
    }
   
}
