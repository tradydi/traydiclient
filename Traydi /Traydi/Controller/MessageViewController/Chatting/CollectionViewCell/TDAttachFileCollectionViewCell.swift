//
//  TDAttachFileCollectionViewCell.swift
//  Traydi
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation
import UIKit

class TDAttachFileCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgAttach: UIImageView!
    
    @IBOutlet weak var lblAttachFileName: UILabel!
    
}
