//
//  TDProjectAndBookingViewController.swift
//  Traydi
//
//  Created by mac on 28/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDProjectAndBookingViewController: TDBaseViewController {
    
    @IBOutlet weak var btnSerachAndCreateProject: UIBarButtonItem!
    @IBOutlet weak var viewProject: UIView!
    @IBOutlet weak var viewBooking: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // FIXME: HIMANSHU PAL set add project or booking for tradee
        navigationController?.navigationBar.shadowImage = UIImage()
        if AppSharedData.shared.isSeeker{
            viewProject.isHidden = true
            viewBooking.isHidden = false
            self.title = "Bookings"
           // self.btnSerachAndCreateProject.image = #imageLiteral(resourceName: "searchmessage")
        }else{
            viewProject.isHidden = false
            viewBooking.isHidden = true
            self.title = "Project"
            self.btnSerachAndCreateProject.image = #imageLiteral(resourceName: "createmessage")
        }
      //  setupSideMenuImageView()
    }
    
    /*func setupSideMenuImageView() {
       //let logoImage = UIImage.init(named: "shelesh")
      let view = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
       let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
       imgV.image = UIImage(named: "shelesh")
       imgV.contentMode = .scaleAspectFill
     //  imgV.layer.cornerRadius = 22
      // imgV.clipsToBounds = true
       let buttonProfile = UIButton(frame: view.frame)
      // buttonProfile.addTarget(self, action: #selector(profilePicBtnAction), for: .touchUpInside)
       view.layer.cornerRadius = 22
       view.clipsToBounds = true
       view.addSubview(imgV)
       view.addSubview(buttonProfile)
       let barButtonItem = UIBarButtonItem(customView: view)
       self.navigationItem.leftBarButtonItem = barButtonItem
    }*/
    
    @IBAction func actionSerachAndCreateProject(_ sender: Any) {
        if AppSharedData.shared.isSeeker{
           // self.performSegue(withIdentifier: "", sender: true)
        }else{
            self.performSegue(withIdentifier: "identifierCreateProject", sender: false)
        }
    }
    
    @IBAction func acctionMenu(_ sender: Any) {
        self.performSegue(withIdentifier: "identifierMenu", sender: sender)
    }
    
}
