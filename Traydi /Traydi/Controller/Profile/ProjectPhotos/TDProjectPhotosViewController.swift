//
//  TDProjectPhotos.swift
//  Traydi
//
//  Created by mac on 25/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

class TDProjectPhotosViewController: UIViewController {
    @IBOutlet weak var
    cvProjectPhoto: UICollectionView!
        @IBOutlet weak var cvHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblDescribtion: UILabel!
    @IBOutlet weak var viewNavi: UIView!
    
    var arrImage = [UIImage]()
    
    
    //var imagePicker: TDImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNavi.clipsToBounds = true
        viewNavi.layer.cornerRadius = 20
        viewNavi.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
       // self.imagePicker = TDImagePicker(presentationController: self, delegate: self)
    }
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(animated)
              let custombar = self.tabBarController as! CustomTabBarController
              custombar.isHiddenTabBar(hidden: true)
              self.navigationController?.setNavigationBarHidden(false, animated: true)
        }

    @IBAction func actionAddMoreImage(_ sender: UIButton) {
       // self.imagePicker.present(from: sender)
        let vc = TDSettingsAddPhotoViewController.instance(.profile) as! TDSettingsAddPhotoViewController
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .flipHorizontal
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension TDProjectPhotosViewController : UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIScrollViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TDCVProjectPhotoCell", for: indexPath) as! TDCVProjectPhotoCell
        cell.imgVGallrey.image = arrImage[indexPath.row]
        return cell
    }
}

extension TDProjectPhotosViewController: TDProfileAddPhotoDelegate {
    func selectedPhoto(image: UIImage) {
        arrImage.append(image)
        let indexPath = IndexPath(item: arrImage.count - 1, section: 0)
        cvProjectPhoto.insertItems(at: [indexPath])
    }
}
