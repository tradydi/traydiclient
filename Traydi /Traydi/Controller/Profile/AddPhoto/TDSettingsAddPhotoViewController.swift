//
//  TDSettingsAddPhotoViewController.swift
//  Traydi
//
//  Created by mac on 27/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

protocol TDProfileAddPhotoDelegate:NSObject {
    func selectedPhoto(image:UIImage)
}

class TDSettingsAddPhotoViewController: UIViewController {
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var imageView: UIImageView!
    weak var delegate:TDProfileAddPhotoDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionUploadPhoto(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionTakePhoto(_ sender: Any) {
        imagePicker.sourceType = .camera
        self.present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func actionGallery(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension TDSettingsAddPhotoViewController:  UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        imageView.image = image
        imageView.contentMode  = .scaleAspectFill
        delegate?.selectedPhoto(image: image)
        picker.dismiss(animated: true, completion: nil)
         self.dismiss(animated: true, completion: nil)
    }
}
