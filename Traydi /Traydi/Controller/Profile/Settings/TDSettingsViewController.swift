//
//  TDSettingsViewController.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import AVFoundation
import MessageUI
class TDSettingsViewController: UIViewController {
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tvSettings: UITableView!
    
    var arrSettingsModal = [SettingsModal]()
    var arrDocumentType = [DataFetcherJobTitleModal]()
    
    var uploadIndexPath:IndexPath!
    var documentType:String = ""
    var paramImages = [[String:Any]]()
   // var isSelected : Bool = false
    
    var imgDocument: UIImage?
    var intEnableDisable = 0
     var picker = UIImagePickerController()
var arrEmail = [String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNavigation.clipsToBounds = true
        viewNavigation.layer.cornerRadius = 20
        viewNavigation.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        self.setupRegiserNib()
        self.isSetupDocumentTypeApiCall()
        imgDocument = nil
    }
    
    
    func setupRegiserNib(){
        tvSettings.register(UINib(nibName: "TDSettingsNotificationCell", bundle: Bundle.main), forCellReuseIdentifier: "TDSettingsNotificationCell")
        
        tvSettings.register(UINib(nibName: "TDSettingsDocumentDetailsTVCell", bundle: Bundle.main), forCellReuseIdentifier: "TDSettingsDocumentDetailsTVCell")
        
        tvSettings.register(UINib(nibName: "TDSettingsAccountTVCell", bundle: Bundle.main), forCellReuseIdentifier: "TDSettingsAccountTVCell")
        
        tvSettings.register(UINib(nibName: "TDSettingsContactUsTVCell", bundle: Bundle.main), forCellReuseIdentifier: "TDSettingsContactUsTVCell")
        
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func isSetupDocumentTypeApiCall() {
        APIManager.share.delegate = self
        let apiActions = ApiAction.TradersAndTradees.kdocument_type
        APIManager.share.performActionGetApi(params: [:], apiAction: apiActions)
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
}

extension TDSettingsViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSettingsModal.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dict = arrSettingsModal[section]
        if dict.isSelected {
            return dict.title.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDSettingsNotificationCell", for: indexPath) as! TDSettingsNotificationCell
            let dict = arrSettingsModal[indexPath.section].title[indexPath.row]
            cell.lblNotification.text = dict.title
            cell.delegate = self
            if dict.notify_on == "1" {
                cell.switchStatus.isOn = true
            } else {
                cell.switchStatus.isOn = false
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDSettingsDocumentDetailsTVCell", for: indexPath) as! TDSettingsDocumentDetailsTVCell
            cell.delegate = self
            //    print(arrSettingsModal[indexPath.section].title.count)
            //     print(indexPath.row)
            
            let accounts = arrSettingsModal[indexPath.section].title[indexPath.row]
            if accounts.isAddMore ?? false{
                cell.imgDocument.image = accounts.imgDoc
                //cell.lblDocumentName.text =  accounts.isImageName  // "Document Image"
                cell.tfDocumentName.text = accounts.isImageName
                cell.lblDocumentName.text = "Upload Document"
                cell.btnSelectDocumentType.setTitle(accounts.docType, for: .normal)
                cell.btnSelectDocumentType.setTitleColor(UIColor.black, for: .normal)
                if indexPath.row == 0 {
                    cell.btnAddMore.isHidden = false
                } else {
                    cell.btnAddMore.isHidden = true
                }
            }else{
                cell.imgDocument.image = UIImage(named: "document4")
                cell.lblDocumentName.text = "Upload Document"
               // cell.btnAddMore.isHidden = true
                cell.btnAddMore.isHidden = false
                cell.btnSelectDocumentType.setTitle("Select document type", for: .normal)
                cell.btnSelectDocumentType.setTitleColor(UIColor.lightGray, for: .normal)
            }
            
            
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDSettingsAccountTVCell", for: indexPath) as! TDSettingsAccountTVCell
            let accounts = arrSettingsModal[indexPath.section].title[indexPath.row].title
            cell.lblAccount.text = accounts
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDSettingsNotificationCell", for: indexPath) as! TDSettingsNotificationCell
            let dict = arrSettingsModal[indexPath.section].title[indexPath.row]
            cell.lblNotification.text = dict.title
            cell.delegate = self
           
            if dict.notify_on == "1" {
                cell.switchStatus.isOn = true
            } else {
                cell.switchStatus.isOn = false
            }
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDSettingsContactUsTVCell", for: indexPath) as! TDSettingsContactUsTVCell
            cell.delegate = self
             cell.tagListView.delegate = self
            cell.tagListView.removeAllTags()
            cell.tagListView.addTags(arrEmail)
            return cell
            
        default:
            break
        }
        return UITableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.tvSettings.frame.width, height: 60))
        let dict = arrSettingsModal[section]
        let imgV = UIImageView(frame: CGRect(x: 8, y: 8, width: 30, height: 30))
        imgV.image = UIImage(named:  dict.imageName!)
        imgV.contentMode = .scaleAspectFit
        view.addSubview(imgV)
        
        let lblHeader = UILabel(frame: CGRect(x: imgV.frame.width + 16 , y: 8, width: self.view.frame.width - (imgV.frame.width + 20), height: 40))
        lblHeader.textColor = .black
        lblHeader.text = dict.headerTitle
        view.addSubview(lblHeader)
        
        let arrRowData = dict.title
        
        let btnSelectedRow = UIButton(frame: CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.width, height: view.frame.height))
        if arrRowData.count > 0  {
            
            btnSelectedRow.setImage(UIImage(named: "expand"), for: .normal)
            
            btnSelectedRow.imageEdgeInsets = UIEdgeInsets(top: 0 , left: btnSelectedRow.frame.size.width - 30, bottom: 0, right: 5);
            
            btnSelectedRow.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40);
            
        }
        btnSelectedRow.tintColor = UIColor.darkGray
        btnSelectedRow.tag = section
        btnSelectedRow.addTarget(self, action: #selector(actionHeader), for: .touchUpInside)
        view.addSubview(btnSelectedRow)
        view.backgroundColor = .white
        return view
    }
    
    @objc func actionHeader(_ sender: UIButton) {
        
        // FIXME: HIMANSHU PAL:- pramod tag get indexpath
        let section = sender.tag
        print(section)
        let dict = arrSettingsModal[section]
        print(dict.headerTitle)
        var indexPath = [IndexPath]()
        if  dict.title.count > 0{
        for i in 0..<dict.title.count {
            let indexPa = IndexPath(item: i, section: section)
            indexPath.append(indexPa)
        }
        
        let isSelected = dict.isSelected
        arrSettingsModal[sender.tag].isSelected = !isSelected
        if isSelected {
            sender.setImage(UIImage(named: "expand"), for: .normal)
            tvSettings.deleteRows(at: indexPath, with: .automatic)
        } else {
            sender.setImage(UIImage(named: "condense"), for: .normal)
            tvSettings.insertRows(at: indexPath, with: .automatic)
        }
    }
        if section == 5{
            sendEmail()
        }else if section == 9{
         /*  let vc = LoginViewController.instance(.authentication) as! LoginViewController
            UIApplication.shared.windows.first?.rootViewController = vc
            UIApplication.shared.windows.first?.makeKeyAndVisible()*/
            self.navigationController!.setViewControllers([LoginViewController.instance()], animated: true)
             UserDefaults.standard.logoutData()
            UserDefaults.standard.logoutIsLogin()
           
             
        }
    }
}

extension TDSettingsViewController:TDSettigsContactUsDelegate , TDSettingsDocumentDelegate , TDSettingNotificatonDelegate {
    
    // TDPrivacy Police
     
    func actionNotifyOnAndOff(cell: TDSettingsNotificationCell) {
        guard let indexPath = tvSettings.indexPath(for: cell)  else {
            return
        }
        uploadIndexPath = indexPath
        print("indexPath.section\(indexPath.section)")
        print("indexPath.row\(indexPath.row)")
        let loginUser = AppSignleHelper.shard.login.userinfo
        var apiAction = ""
        var params = [String:Any]()
       
        if cell.switchStatus.isOn{
            intEnableDisable = 1
        }else{
            intEnableDisable = 0
        }
        if indexPath.section == 0 {
            apiAction =  ApiAction.TradersAndTradees.knotification_on_off
            if indexPath.row == 0{
                params[ApiConstants.key.NotificationOnOff.notify_on_connection] = intEnableDisable
            }else  if indexPath.row == 1{
                params[ApiConstants.key.NotificationOnOff.notify_on_message] = intEnableDisable
            }else  if indexPath.row == 2{
                params[ApiConstants.key.NotificationOnOff.notify_on_application] = intEnableDisable
            }
            else  if indexPath.row == 3{
                params[ApiConstants.key.NotificationOnOff.notify_on_review] = intEnableDisable
            }
            else  if indexPath.row == 4{
                params[ApiConstants.key.NotificationOnOff.notify_on_comment] = intEnableDisable
            }
            else  if indexPath.row == 5{
                params[ApiConstants.key.NotificationOnOff.notify_on_hour_adjust] = intEnableDisable
            }
            else  if indexPath.row == 6{
                params[ApiConstants.key.NotificationOnOff.notify_on_profile] = intEnableDisable
            }
            else  if indexPath.row == 7{
                params[ApiConstants.key.NotificationOnOff.notify_on_tradee_view] = intEnableDisable
            }
            else  if indexPath.row == 8{
                params[ApiConstants.key.NotificationOnOff.notify_on_likes] = intEnableDisable
            }
            else  if indexPath.row == 9{
                params[ApiConstants.key.NotificationOnOff.notify_on_connection_accepted] = intEnableDisable
            }
            else  if indexPath.row == 10{
                params[ApiConstants.key.NotificationOnOff.notifiy_on_clock] = intEnableDisable
            }
            else  if indexPath.row == 11{
                params[ApiConstants.key.NotificationOnOff.notifiy_on_job_complete] = intEnableDisable
            }
        }else{
            apiAction =  ApiAction.TradersAndTradees.kprivacy_on_off
            params[ApiConstants.key.NoticationKey.kprivacy_status] = intEnableDisable
        }
        params[ApiConstants.key.kuser_id] = loginUser?.id
        APIManager.share.delegate = self
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
    
    // TDSettingsDocumentDelegate methods
    func actionSelectDocumentType(sender: UIButton, cell: TDSettingsDocumentDetailsTVCell) {
        
        guard let indexPath = tvSettings.indexPath(for: cell) else {
            return
        }
        
        let alertController =  TDSettingsDocumentTypeViewController.instance(.profile) as! TDSettingsDocumentTypeViewController
        alertController.modalPresentationStyle = .popover
        alertController.arrDocumentType = arrDocumentType
        alertController.indexPaths = indexPath
        alertController.delegate = self
        let width = sender.frame.size.width
        alertController.preferredContentSize = CGSize(width: width, height: 300)
        let popover = alertController.popoverPresentationController
        popover?.permittedArrowDirections =  .any
        popover?.delegate = self
        popover?.sourceView = sender
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    func actionSelectUploadDocument(cell: TDSettingsDocumentDetailsTVCell) {
        //let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        
        guard let indexPath = self.tvSettings.indexPath(for: cell) else {
            return
        }
        uploadIndexPath = indexPath
        
        self.view.endEditing(true)
        self.openActionSheet()
        
     /*   let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import)
        
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)*/
    }
    
    func actionSelectAddDocuments(cell: TDSettingsDocumentDetailsTVCell) {
        guard let indexPath = self.tvSettings.indexPath(for: cell) else {
            return
        }
        guard let celli = self.tvSettings.cellForRow(at: indexPath) as? TDSettingsDocumentDetailsTVCell else {
            return
        }
        print(celli.tfDocumentName.text)
        let dict = arrSettingsModal[indexPath.section].title.last!
        print(dict.docType as Any)
        print(dict.isAddMore as Any)
        print(dict.imgDoc as Any)
        print(dict.title as Any)
        print(dict.isImageName as Any)
        print(dict.notify_on as Any)
       
        
        uploadIndexPath = indexPath
        
        print(dict.docType as Any)
        if dict.docType == ""{
            Util.showAlertWithCallback("", message: "Please select document type", isWithCancel: false)
            return
        }else  if celli.tfDocumentName.text == "" {
            Util.showAlertWithCallback(AppName, message: "Please enter name of document", isWithCancel: false)
            return
        }else  if imgDocument == nil {
            Util.showAlertWithCallback(AppName, message: "Please seelct document", isWithCancel: false)
            return
        }
        let apiActions = ApiAction.TradersAndTradees.kqualification_document
        APIManager.share.delegate = self
        var params = [String:Any]()
        params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id
        params[ApiConstants.key.kdocument_type] =   celli.btnSelectDocumentType.titleLabel?.text
        params[ApiConstants.key.document_name] = celli.lblDocumentName.text
        //  celli.btnSelectDocumentType.titleLabel?.text
        paramImages.removeAll()
        self.paramImages.append([ApiConstants.key.kdocument:imgDocument!])
        
        
        // FIXME: HIMANSHU PAL 25/8/20
        let doc = self.arrSettingsModal[self.uploadIndexPath.section].title[self.uploadIndexPath.row].docType
     
        self.arrSettingsModal[self.uploadIndexPath.section].title.remove(at: self.uploadIndexPath.row)
        self.arrSettingsModal[self.uploadIndexPath.section].title.insert(TitleModal(title: "nicc", notify_on: "document", isImageName: celli.tfDocumentName.text, imgDoc: imgDocument, isAddMore : true, docType : doc), at: self.uploadIndexPath.row)
        let indexset = IndexSet(arrayLiteral: 1)
        self.tvSettings.reloadSections(indexset, with: .none)
        print(params)
        print(self.paramImages)
        APIManager.share.performImageUpload(params: params, apiAction: apiActions, imageParams: self.paramImages)
        

    }
    
    
    // TDSettigsContactUsDelegate menthod
    func actionDelegateRemnders(cell: TDSettingsContactUsTVCell) {
        
    }
    
    func actionDelegateSend(cell: TDSettingsContactUsTVCell) {
        
        if cell.txtTo.text!.isEmpty && cell.txtMessageHere.text!.isEmpty && cell.txtSubject.text!.isEmpty {
            return
        }
        let apiAction = ApiAction.TradersAndTradees.kcontact_us
        var params = [String:Any]()
        params[ApiConstants.key.ContactUs.kto] =       cell.txtTo.text!  //arrEmail of emails to send multiple mails
        params[ApiConstants.key.ContactUs.kmessage] =  cell.txtMessageHere.text!
        params[ApiConstants.key.ContactUs.ksubject] =  cell.txtSubject.text!
        
        APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
            let dict = response.result.value as! [String:Any]
            
            let status = dict[ApiResponseStatus.kcode] as! String
            if status == "1" {
                Util.showAlertWithCallback(AppName, message: (dict["message"] as! String), isWithCancel: false)
            } else {
                Util.showAlertWithCallback(AppName, message: (dict["message"] as! String), isWithCancel: false)
            }
            
        }) { (error) in
            Util.showAlertWithCallback(AppName, message: error.localizedDescription, isWithCancel: false)
        }
    }
    
    func actionDelegateAdds(cell: TDSettingsContactUsTVCell) {
        guard let indexPath = tvSettings.indexPath(for: cell)  else {
            return
        }
       /* if !self.isValidEmail(cell.txtTo.text!) {
            Util.showAlertWithCallback("", message: "Please enter correct email", isWithCancel: false)
            return
        }*/
        arrEmail.append(cell.txtTo.text!)
     arrEmail =   arrEmail.removeDuplicates()
    //    cell.tagListView.addTag(cell.txtTo.text!)
        uploadIndexPath = indexPath
         print("arrEmail22-\(arrEmail)-")
        UIView.performWithoutAnimation({
               let loc = tvSettings.contentOffset
               tvSettings.reloadRows(at: [indexPath], with: .none)
               tvSettings.contentOffset = loc
           })
          cell.txtTo.text! = ""
        print("arrEmail-\(arrEmail)-")
    }
    
    
    
    func isValidEmail(_ email: String) -> Bool {
           let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

           let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
           return emailPred.evaluate(with: email)
       }
    
}


extension TDSettingsViewController: ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        } //1
        if check == ApiAction.TradersAndTradees.kdocument_type {
            do {
                
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                
                if resultObject[ApiResponseStatus.kcode] as! String == ApiResponseStatus.kAPI_Result_Success {
                    let jobTitle = resultObject["document_type"] as! [[String:Any]]
                    for item in jobTitle {
                        let jobModal = DataFetcherJobTitleModal(parameters: item)
                        self.arrDocumentType.append(jobModal)
                    }
                } else {
                    Util.showAlertWithCallback(AppName, message: (resultObject[ApiResponseStatus.kMessage] as! String), isWithCancel: false)
                }
                let apiActionNotification = ApiAction.TradersAndTradees.kget_notification
                
                let loginUser = AppSignleHelper.shard.login.userinfo
                
                var params = [String:Any]()
                params[ApiConstants.key.kuser_id] = loginUser?.id
                
                APIManager.share.performMaltipartPost(params: params
                    , apiAction: apiActionNotification)
                
            } catch {
                print("Unable to parse JSON response")
            }
            //2
        } else if check == ApiAction.TradersAndTradees.kget_notification {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                
                if resultObject[ApiResponseStatus.kcode] as! String == ApiResponseStatus.kAPI_Result_Success {
                    let notifications_notify = resultObject["notificationList"] as! [String:Any]
                    arrSettingsModal = [SettingsModal(header: "Notification", imageName: "notification", title: [
                        TitleModal(title: "Notify when a connection request is recieved.", notify_on: notifications_notify["notify_on_connection"] as! String),
                        TitleModal(title: "Notify when a message recieved.", notify_on: notifications_notify["notify_on_message"] as! String),
                        TitleModal(title: "Notify when a application is recieved", notify_on: notifications_notify["notify_on_application"] as! String),
                        TitleModal(title: "Notify when a review is recieved", notify_on: notifications_notify["notify_on_review"] as! String),
                        TitleModal(title: "Notify when a comments are recieved on posted", notify_on: notifications_notify["notify_on_comment"] as! String),
                        TitleModal(title: "Notify when hours are adjusted by the tradee.", notify_on: notifications_notify["notify_on_hour_adjust"] as! String),
                        TitleModal(title: "Notify when profile is viewed.", notify_on: notifications_notify["notify_on_profile"] as! String),
                        TitleModal(title: "Notify when tradee views your profile.", notify_on: notifications_notify["notify_on_tradee_view"] as! String),
                        TitleModal(title: "Notify when like received on postsd.", notify_on: notifications_notify["notify_on_likes"] as! String),
                        TitleModal(title: "Notify when connection Request Accepted", notify_on: notifications_notify["notify_on_connection_accepted"] as! String),
                        TitleModal(title: "Notify Tradee clocks-In/Out", notify_on: notifications_notify["notifiy_on_clock"] as! String),
                        TitleModal(title: "Notify when job Completed", notify_on: notifications_notify["notifiy_on_job_complete"] as! String),
                        
                    ]), SettingsModal(header: "Documents Details", imageName: "settingdoc", title: [TitleModal(title: "nic", notify_on: "document", isImageName: "", imgDoc:  #imageLiteral(resourceName: "document4.png") , isAddMore: false ,docType : "")]),
                        SettingsModal(header: "Account Settings", imageName: "settingcontact", title: [TitleModal(title: "John smith", notify_on: "0"), TitleModal(title: "Johnsmith@hotmail.com", notify_on: "1")]),
                        SettingsModal(header: "Privacy Settings", imageName: "settingprivacy", title: [TitleModal(title: "Only connections can conect me", notify_on: notifications_notify["privacy_status"] as! String)]),
                        SettingsModal(header: "About Us", imageName: "settingabout", title: []),
                        SettingsModal(header: "Contact Us", imageName: "settingcontact", title: []),
                        SettingsModal(header: "Traydi Legal Notice", imageName: "settinglegal", title: []),
                        SettingsModal(header: "Term and Condition", imageName: "settingterm", title: []),
                        SettingsModal(header: "Privacy Policy", imageName: "settingpolicy", title: []),SettingsModal(header: "Logout", imageName: "settingpolicy", title: []),
                    ]//[TitleModal(title: "Notifcation", notify_on: "0")]
                    self.tvSettings.reloadData()
                } else {
                    Util.showAlertWithCallback(AppName, message: (resultObject[ApiResponseStatus.kMessage] as! String), isWithCancel: false)
                }
                
            } catch {
                print("Unable to parse JSON response")
            }
        }
        else if check == ApiAction.TradersAndTradees.kprivacy_on_off || check == ApiAction.TradersAndTradees.knotification_on_off  {
            do {
                
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                
                if resultObject[ApiResponseStatus.kcode] as! String == ApiResponseStatus.kAPI_Result_Success {
                    //      let jobTitle = resultObject["tradersinfo"] as? [String:Any]
                    guard let indexPath = uploadIndexPath  else {
                        return
                    }
                    arrSettingsModal[indexPath.section].title[indexPath.row].notify_on = "\(intEnableDisable)"
                    let indexset = IndexSet(arrayLiteral: indexPath.section)
                    self.tvSettings.reloadSections(indexset, with: .none)
                    //
                    
                } else {
                    guard let indexPath = uploadIndexPath  else {
                        return
                    }
                    var setDefualt = 0
                    if intEnableDisable == 1{
                        setDefualt = 0
                    }else{
                        setDefualt = 1
                    }
                    arrSettingsModal[indexPath.section].title[indexPath.row].notify_on = "\(setDefualt)"
                    let indexset = IndexSet(arrayLiteral: indexPath.section)
                    self.tvSettings.reloadSections(indexset, with: .none)
                    Util.showAlertWithCallback(AppName, message: (resultObject[ApiResponseStatus.kMessage] as! String), isWithCancel: false)
                }
            } catch {
                print("Unable to parse JSON response")
            }
            
        }else if check == ApiAction.TradersAndTradees.kqualification_document{
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if resultObject[ApiResponseStatus.kcode] as! String == ApiResponseStatus.kAPI_Result_Success {
                    Util.showAlertWithCallback("", message: resultObject[ApiResponseStatus.kMessage] as? String, isWithCancel: false)
                }
            }catch {
                print("Unable to parse JSON response")
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        guard let indexPath = uploadIndexPath  else {
                               return
                           }
                           var setDefualt = 0
                           if intEnableDisable == 1{
                               setDefualt = 0
                           }else{
                               setDefualt = 1
                           }
                           arrSettingsModal[indexPath.section].title[indexPath.row].notify_on = "\(setDefualt)"
                           let indexset = IndexSet(arrayLiteral: indexPath.section)
                           self.tvSettings.reloadSections(indexset, with: .none)
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
}

extension TDSettingsViewController : UIPopoverPresentationControllerDelegate , TDDocumentTypeDelegate {
    func selectDocumentType(items: DataFetcherJobTitleModal, indexPath: IndexPath) {
        guard let celli = self.tvSettings.cellForRow(at: indexPath) as? TDSettingsDocumentDetailsTVCell else {
            return
        }
        
        // FIXME: HIMANSHU PAL 25/8/20
        //add ducument type in model;
        self.arrSettingsModal[indexPath.section].title.remove(at: indexPath.row)
        self.arrSettingsModal[indexPath.section].title.insert(TitleModal(title: "nic", notify_on: "document" , docType : items.name), at: indexPath.row)
        celli.btnSelectDocumentType.setTitle(items.name, for: .normal)
        celli.btnSelectDocumentType.setTitleColor(UIColor.black, for: .normal)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
/*
// upload documents Types
extension TDSettingsViewController: UIDocumentPickerDelegate{
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let cico = url as URL
        guard let celli = self.tvSettings.cellForRow(at: uploadIndexPath) as? TDSettingsDocumentDetailsTVCell else {
            return
        }
        //  celli.btnAddMore.isHidden = false
        isSelected = true
        
        celli.lblDocumentName.text = url.lastPathComponent
        let pathExtention = url.pathExtension
        print(pathExtention)
        let thumbnailImage = ROThumbnail.sharedInstance.getThumbnail(url)
        celli.imgDocument.image =    thumbnailImage
        var documentData:Data?
        
        do {
            documentData = try Data(contentsOf: cico as URL)
            DispatchQueue.main.async {
                let apiActions = ApiAction.TradersAndTradees.kqualification_document
                APIManager.share.delegate = self
                var params = [String:Any]()
                params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id
                params[ApiConstants.key.kdocument_type] =   celli.btnSelectDocumentType.titleLabel?.text
                params[ApiConstants.key.document_name] = celli.lblDocumentName.text
                //  celli.btnSelectDocumentType.titleLabel?.text
                
                // FIXME: HIMANSHU PAL 25/8/20
                let doc = self.arrSettingsModal[self.uploadIndexPath.section].title[self.uploadIndexPath.row].docType
                self.arrSettingsModal[self.uploadIndexPath.section].title.remove(at: self.uploadIndexPath.row)
                self.arrSettingsModal[self.uploadIndexPath.section].title.insert(TitleModal(title: "nicc", notify_on: "document", isImageName: url.lastPathComponent, imgDoc: thumbnailImage, isAddMore : true, docType : doc), at: self.uploadIndexPath.row)
                let indexset = IndexSet(arrayLiteral: 1)
                self.tvSettings.reloadSections(indexset, with: .none)
                
                
                self.paramImages.append([ApiConstants.key.kdocument:documentData!])
                 
                 APIManager.share.performImageUpload(params: params, apiAction: apiActions, imageParams: self.paramImages)
             /*   APIManager.share.performDocumentsUpload(params: params, apiAction: apiActions, documentsParams: self.paramImages, documentType: url.pathExtension, successCompletionHandler: { (response) in
                    
                    let result = response.result.value as! [String:Any]
                    Util.showAlertWithCallback(AppName, message: (result["message"] as! String), isWithCancel: false)
                    
                }) { (error) in
                    Util.showAlertWithCallback(AppName, message: error.localizedDescription, isWithCancel: false)
                }*/
            }
            
        } catch {
            print("Unable to load data: \(error)")
        }
        
        
        
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}*/



struct SettingsModal {
    var headerTitle:String?
    var imageName:String?
    
    var title:[TitleModal] = []
    var isSelected:Bool = false
    
    init(header:String, imageName:String, title:[TitleModal]) {
        self.headerTitle = header
        self.imageName = imageName
        self.title = title
    }
}

struct TitleModal {
    var title:String?
    var notify_on:String?
    var isImageName :String?
    var imgDoc : UIImage?
    var isAddMore: Bool?
    var docType :String?
    init(title:String, notify_on:String,isImageName : String? = ""  , imgDoc : UIImage? = UIImage(named: "") ,isAddMore : Bool? = false ,docType : String? = "") {
        self.title = title
        self.notify_on = notify_on
        self.isImageName = isImageName
        self.imgDoc = imgDoc
        self.isAddMore = isAddMore
        self.docType = docType
    }
}

extension TDSettingsViewController:  UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //MARK: - Camera delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            
            let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
            let url = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
            print(url)
            
            
            guard let celli = self.tvSettings.cellForRow(at: uploadIndexPath) as? TDSettingsDocumentDetailsTVCell else {
                return
            }
            //  celli.btnAddMore.isHidden = false
           
            
        //    celli.lblDocumentName.text = url.lastPathComponent
           // let pathExtention = url.pathExtension
          //  print(pathExtention)
         //   let thumbnailImage = ROThumbnail.sharedInstance.getThumbnail(url)
            celli.imgDocument.image =    image
            imgDocument = image
            let doc = self.arrSettingsModal[self.uploadIndexPath.section].title[self.uploadIndexPath.row].docType
            
            self.arrSettingsModal[self.uploadIndexPath.section].title.remove(at: self.uploadIndexPath.row)
            self.arrSettingsModal[self.uploadIndexPath.section].title.insert(TitleModal(title: "nicc", notify_on: "document", isImageName: celli.tfDocumentName.text, imgDoc: image, isAddMore : false, docType : doc), at: self.uploadIndexPath.row)
            
        /*  let apiActions = ApiAction.TradersAndTradees.kqualification_document
            APIManager.share.delegate = self
            var params = [String:Any]()
            params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id
            params[ApiConstants.key.kdocument_type] =   celli.btnSelectDocumentType.titleLabel?.text
            params[ApiConstants.key.document_name] = celli.lblDocumentName.text
            //  celli.btnSelectDocumentType.titleLabel?.text
            self.paramImages.append([ApiConstants.key.kdocument:image])
            
            
            // FIXME: HIMANSHU PAL 25/8/20
            
            let indexset = IndexSet(arrayLiteral: 1)
            self.tvSettings.reloadSections(indexset, with: .none)
            print(params)
            print(self.paramImages)
            APIManager.share.performImageUpload(params: params, apiAction: apiActions, imageParams: self.paramImages)*/
            
        }
        
        /* if let image = info[.editedImage]  as? UIImage {
         self.imgProfile.image = image
         } else {
         let image = info[.originalImage] as? UIImage
         self.imgProfile.image = image
         }*/
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    // action sheet for select image from galary or camera
    func openActionSheet(){
        let alert = UIAlertController(title: "Select Image", message: "" , preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ (UIAlertAction)in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera;
                self.picker.allowsEditing = true
                self.present(self.picker, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "Choose fromg allery", style: .default , handler:{ (UIAlertAction)in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: {
                })
            }
        }))
        alert.addAction(UIAlertAction(title: "txt.Cancel", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
        }))
        self.present(alert, animated: true, completion: {
            
        })
    }
}

extension TDSettingsViewController:TagListViewDelegate{
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
       // tagView.isSelected = !tagView.isSelected
         
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
      //  print("Tag Remove pressed: \(title), \(sender)")
      
       //   arrSearch = arrSearch.filter{ $0.type_of_trade != title}
        for i in 0..<arrEmail.count {
            let email = arrEmail[i]
            if email == title {
                arrEmail.remove(at: i)
               UIView.performWithoutAnimation({
                   let loc = tvSettings.contentOffset
                   tvSettings.reloadRows(at: [uploadIndexPath], with: .none)
                   tvSettings.contentOffset = loc
               })
                break
            }
        }
        sender.removeTagView(tagView)
    }
}



extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}


extension TDSettingsViewController :  MFMailComposeViewControllerDelegate  {
    func sendEmail() {
       let emailTitle = "Feedback"
        let messageBody = ""
        let toRecipents = ["test@gmail.com"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        self.present(mc, animated: true, completion: nil)
        
         /*      //TODO:  You should chack if we can send email or not
               if MFMailComposeViewController.canSendMail() {
                   let mail = MFMailComposeViewController()
                   mail.mailComposeDelegate = self
                   mail.setToRecipients(["you@yoursite.com"])
                   mail.setSubject("Email Subject Here")
                   mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
                   present(mail, animated: true)
               } else {
                   print("Application is not able to send an email")
               }
        */
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }



    }
