//
//  TDSettingsContactUsTVCell.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

protocol TDSettigsContactUsDelegate:NSObject {
    func actionDelegateSend(cell:TDSettingsContactUsTVCell)
    func actionDelegateRemnders(cell:TDSettingsContactUsTVCell)
     func actionDelegateAdds(cell:TDSettingsContactUsTVCell)
}


class TDSettingsContactUsTVCell: UITableViewCell {
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var txtMessageHere: UITextView!
    @IBOutlet weak var btnAdd: UIButton!
    weak var delegate:TDSettigsContactUsDelegate?
    @IBOutlet weak var tagListView: TagListView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.txtMessageHere.delegate = self
        txtMessageHere.text = "messsage here"
        txtMessageHere.textColor = UIColor.darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func actionAddEmail(_ sender: Any) {
        delegate?.actionDelegateAdds(cell: self)
    }

    @IBAction func actionSend(_ sender: Any) {
        delegate?.actionDelegateSend(cell: self)
    }
    
    @IBAction func actionRemender(_ sender: Any) {
        delegate?.actionDelegateRemnders(cell: self)
    }
}

extension TDSettingsContactUsTVCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtMessageHere.textColor == UIColor.darkGray {
            txtMessageHere.text = ""
            txtMessageHere.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtMessageHere.text == "" {
            txtMessageHere.text = "messsage here"
            txtMessageHere.textColor = UIColor.darkGray
        }
    }
}
