//
//  TDSettingsNotificationCell.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

protocol TDSettingNotificatonDelegate:NSObject {
    func actionNotifyOnAndOff(cell:TDSettingsNotificationCell)
}

class TDSettingsNotificationCell: UITableViewCell {
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var switchStatus: UISwitch!
    weak var delegate:TDSettingNotificatonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        switchStatus.layer.cornerRadius = switchStatus.frame.height/2
               switchStatus.transform = CGAffineTransform(scaleX: 0.8, y: 0.8);
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionValueChange(_ sender: Any) {
        self.delegate?.actionNotifyOnAndOff(cell: self)
    }
    
    
}
