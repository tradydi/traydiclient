//
//  TDSettingsDocumentDetailsTVCell.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

protocol TDSettingsDocumentDelegate:NSObject {
    func actionSelectDocumentType(sender:UIButton, cell:TDSettingsDocumentDetailsTVCell)
    func actionSelectUploadDocument(cell:TDSettingsDocumentDetailsTVCell)
    func actionSelectAddDocuments(cell:TDSettingsDocumentDetailsTVCell)
}

class TDSettingsDocumentDetailsTVCell: UITableViewCell {
    
    @IBOutlet weak var imgDocument: UIImageView!
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var btnSelectDocumentType: UIButton!
    @IBOutlet weak var btnAddMore: UIButton!
    @IBOutlet weak var tfDocumentName: UITextField!
    
    weak var delegate:TDSettingsDocumentDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actopmDocuments(_ sender: UIButton) {
        delegate?.actionSelectDocumentType(sender: sender, cell: self)
        
    }
    
    @IBAction func actionUploadDocument(_ sender: Any) {
        delegate?.actionSelectUploadDocument(cell: self)
    }
    
    @IBAction func actionAddDocuments(_ sender: Any) {
        delegate?.actionSelectAddDocuments(cell: self)
    }
    
}
