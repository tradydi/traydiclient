//
//  TDProfileQualificationTableViewCell.swift
//  Traydi
//
//  Created by mac on 25/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit

class TDProfileQualificationTableViewCell: UITableViewCell {
    @IBOutlet weak var imgDocument: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblissueName: UILabel!
    @IBOutlet weak var lblDocumentType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
