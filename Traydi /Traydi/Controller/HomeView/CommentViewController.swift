//
//  CommentViewController.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import  SDWebImage
class CommentViewController: UIViewController {
   // @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var txtVComment: UITextView!
    @IBOutlet weak var CommentConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var txtVConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var tableViewComments: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    var postComment: Postinfo?
   // var arrComments = [Comment]()
    var isImages:Bool = true
    var myIndex : IndexPath?
    @IBOutlet weak var lblReplyTo: UILabel!
    var isReplyToComment  : Bool = false
    var replyToIndex : Int = 0
    @IBOutlet weak var viewComment: UIView!
    
    @IBOutlet weak var stkReply: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBarController = self.tabBarController as! CustomTabBarController
                      tabBarController.isHiddenTabBar(hidden: true)
        txtVComment.textColor = .gray
        txtVComment.delegate = self
        btnSend.isEnabled = false
        tableViewComments.sectionHeaderHeight = UITableView.automaticDimension
 
         //    constHeightCommentView.isActive = false
       //  viewComment.isHidden = false
            // FIXME: HIMANSHU PAL text only
            
            self.lblComment.text = "\(postComment?.comments!.count ?? 0)" + " Comments"
         //   self.txtVComment.becomeFirstResponder()
            scrollToBottom()
       
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.tableViewComments.remembersLastFocusedIndexPath = true
        
        stkReply.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       tableViewComments.reloadData()
       //
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            if (self.postComment?.comments!.count)! > 0{
                let indexPath = IndexPath(row: NSNotFound, section: (self.postComment?.comments!.count)! - 1)
                self.tableViewComments.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
            
        }
    }
    
    @IBAction func backgroundBtnAction(_ sender: Any) {
        //self.txtFdComment.resignFirstResponder()
        self.dismiss(animated: true) {
            //completion block
        }
    }
    
    @IBAction func emojiBtnAction(_ sender: Any) {
        
    }
    
    func validation() -> Bool {
        var status:Bool = true
        if txtVComment.text.isEmpty || txtVComment.text == "Add a Comment...."  {
            status = false
        }
        return status
    }
    @IBAction func actionCloseToReply(_ sender: Any) {
         isReplyToComment = false
          stkReply.isHidden = true
    }
    
    
    @IBAction func sendMessageBtnAction(_ sender: Any) {
        
        if validation() {
            if isReplyToComment{
               replyToComment()
            }else{
                 self.addcomment()
            }
           
        }
    }
    func  addcomment(){
        var params = [String:Any]()
        //let params = value
        let loginData  = AppSignleHelper.shard.login.userinfo
        params[ApiConstants.key.kuser_id] = loginData?.id
        params[ApiConstants.key.kpost_id] = postComment?.id
        params[ApiConstants.key.kcomment] = txtVComment.text!
        params[ApiConstants.key.kfull_name] = loginData?.full_name
        let apiAction = ApiAction.TradersAndTradees.kcomment_post
        print(apiAction)
        print(params)
        APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
            
            let data = response.result.value as? [String:Any]
            print("data-\(data!)-")
            let dict = [ApiConstants.key.kuser_id:loginData?.id,
                        ApiConstants.key.kpost_id:self.postComment?.id,
                        ApiConstants.key.kcomment:self.txtVComment.text!,
                        "id":(data!["comment"] as? String),
                        ApiConstants.key.kfull_name:loginData?.full_name,
                        "image": loginData!.image!]
            
          
            
            let comment = Comment(parameters: dict as [String : Any])
            self.postComment?.comments?.append(comment)
                self.tableViewComments.reloadData()
                 let indexPath = IndexPath(row: NSNotFound, section: (self.postComment?.comments!.count)! - 1)
                self.tableViewComments.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
          
            self.txtVComment.text = ""
        }) { (error) in
            
        }
    }
    
    func replyToComment(){
        let dict = postComment?.comments![replyToIndex]
           var params = [String:Any]()
           let loginData  = AppSignleHelper.shard.login.userinfo
           params[ApiConstants.key.kuser_id] = loginData?.id
           params[ApiConstants.key.kpost_id] = dict?.post_id
           params[ApiConstants.key.kreply] = txtVComment.text!
           params[ApiConstants.key.kcomment_id] = dict?.id
           let apiAction = ApiAction.TradersAndTradees.kcomment_reply
        print(apiAction)
        print(params)
           APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
               let data = response.result.value as? [String:Any]
               print("data-\(data!)-")
               let reply = data?["reply"] as? [[String : Any]]
               let getReply  = reply?.last
               let dict = [ApiConstants.key.kuser_id: getReply?["user_id"] as? String,
                           ApiConstants.key.kpost_id: getReply?["post_id"] as? String,
                           ApiConstants.key.kreply:getReply?["reply"] as? String,
                           ApiConstants.key.kid: getReply!["id"] as? String,
                           ApiConstants.key.kfull_name:getReply!["full_name"] as? String,
                           "image": getReply!["image"] as? String]
               let replay = Replies(parameters: dict as [String : Any])
            self.postComment?.comments![self.replyToIndex].replies?.append(replay)
            self.stkReply.isHidden = true
                   UIView.performWithoutAnimation {
                       self.tableViewComments.reloadData()
                   }
              
               self.txtVComment.text = "Add a Comment...."
           }) { (error) in
               
           }
    }
    
  
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        UIView.animate(withDuration: 0.1) {
            //self.CommentConstraintBottom.constant += (keyboardFrame.size.height)
           // self.tableViewComments.frame.size.height = self.tableViewComments.frame.height - keyboardFrame.size.height
            if !(self.postComment?.comments!.isEmpty)! {
              
                
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification){
         print("keyboardWillHide")
        UIView.animate(withDuration: 0.1) {
            self.CommentConstraintBottom.constant = 0
            self.tableViewComments.frame.size.height = 0
        }
    }
}

extension CommentViewController: UITableViewDataSource, UITableViewDelegate, CommentDelegate {
  
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewHeaderCell")  as! TDCommentTableViewHeaderCell
            let dict = postComment?.comments![section]
            cell.lblUserName.text = postComment!.full_name
            cell.lblComment.text = dict!.comment
            cell.delegate = self
            cell.myIndex = section
            cell.lblComment.text = dict!.comment
                //like button
             if  dict?.is_like  == "1"{
                  cell.imgLike.image = UIImage(named: "likepost")
              }else{
                   cell.imgLike.image = UIImage(named: "likecomments")
              }
             if dict?.total_like == "0"{
                 cell.lblLike.text = dict?.total_like ?? "0" + " like"
              }
             else if dict?.total_like == "1"{
                 cell.lblLike.text = dict?.total_like ?? "1" + " like"
              }else{
                 cell.lblLike.text = dict?.total_like ?? "2" + " likes"
              }
            
             cell.lblUserName.text = dict!.full_name
            cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
             cell.imgVCommentUserProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (dict?.image)!), placeholderImage: UIImage(named: "user"))
            
            return cell
       
    }
    

    

    func numberOfSections(in tableView: UITableView) -> Int {
            return postComment?.comments?.count ?? 0
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return (postComment?.comments![section].replies?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewCell")  as! TDCommentTableViewCell
            let dict = postComment?.comments![indexPath.section].replies?[indexPath.row]
                cell.lblComment.text = dict?.reply ?? "N/A"
                cell.lblUserName.text = dict?.full_name  ?? "N/A"
           cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgVCommentUserProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (dict?.image)! ), placeholderImage: UIImage(named: "user"))
            return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDCommentTableViewHeaderCell")  as! TDCommentTableViewHeaderCell
            let dict = postComment?.comments![section]
            cell.lblComment.text = dict!.comment
               //like button
            if  dict?.is_like  == "1"{
                 cell.imgLike.image = UIImage(named: "likepost")
             }else{
                  cell.imgLike.image = UIImage(named: "likecomments")
             }
            if dict?.total_like == "0"{
                cell.lblLike.text = dict?.total_like ?? "0" + " like"
             }
            else if dict?.total_like == "1"{
                cell.lblLike.text = dict?.total_like ?? "1" + " like"
             }else{
                cell.lblLike.text = dict?.total_like ?? "2" + " likes"
             }
           
            cell.lblUserName.text = dict!.full_name
            cell.imgVCommentUserProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgVCommentUserProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (dict?.image)!), placeholderImage: UIImage(named: "user"))
            return cell.frame.size.height
        
    }
    private func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    

    
    func addComment(cell: TDCommentTableViewHeaderCell, index: Int) {
        // FIXME: HIMANSHU PAL reply to particular comment
        stkReply.isHidden = false
        let dict = postComment?.comments![index]
        lblReplyTo.text =  "Reply to: " + (dict?.full_name)!
        isReplyToComment = true
        replyToIndex = index
    }
    
    func feedViewCell(cell:  TDCommentTableViewHeaderCell, index: Int) {
        
        guard let celli = cell as? TDCommentTableViewHeaderCell else {
            return
        }
        let user_id = AppSignleHelper.shard.login.userinfo?.id
        let likeData =  postComment?.comments?[index]
      
        var params = [String:Any]()
        params["user_id"] = user_id ?? "0"
        params["post_id"] = likeData?.post_id ?? "0"
        params["comment_id"] = likeData?.id ?? "0"
        
        
        let apiAction = ApiAction.TradersAndTradees.klike_comment
        APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
            let obj = response.result.value as? [String:Any]
            let liked = obj?["liked"] as? Int
             let likes = obj?["likes"] as? String ?? "0"
            self.postComment?.comments?[index].is_like = "\(liked ?? 0)"
              self.postComment?.comments?[index].total_like = likes
            
         if  liked  == 1{
                //likepost
                self.postComment?.comments?[index].is_like = "1"
                celli.imgLike.image = UIImage(named: "likepost")
            }else{
                 self.postComment?.comments?[index].is_like = "0"
                 celli.imgLike.image = UIImage(named: "likecomments")
            }
            if likes == "0"{
                cell.lblLike.text = likes  + " like"
             }
            else if likes == "1"{
                cell.lblLike.text = likes + " like"
             }else{
                cell.lblLike.text = likes + " likes"
             }
            
            self.tableViewComments.reloadData()

        }) { (error) in
         Util.showAlertWithCallback(AppName, message: error.localizedDescription , isWithCancel: false)
        }
    }
    
    
}





extension CommentViewController:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtVComment.textColor ==  .gray {
            txtVComment.textColor = .black
            txtVComment.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtVComment.text.isEmpty {
            txtVComment.textColor = .gray
            txtVComment.text = "Add a comment..."
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let textHeight = textView.contentSize.height
        print(textHeight)
        if textHeight < 100 {
            txtVConstraintHeight.constant = textHeight
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        if text != "Add a comment..." {
            btnSend.isEnabled = true
        }
               
        return true
    }

}
//MARK:- Api Response
extension CommentViewController:ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        
    }
    
    func errorResponse(error: Any, check: String) {
        
    }
}

extension UITableView {
  func hasRowAtIndexPath(indexPath: IndexPath) -> Bool {
    return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section)
  }

  func scrollToTop(animated: Bool) {
    let indexPath = IndexPath(row: 0, section: 0)
    if self.hasRowAtIndexPath(indexPath: indexPath) {
      self.scrollToRow(at: indexPath, at: .top, animated: animated)
    }
  }
}

extension UITableView {
      func lastIndexpath() -> IndexPath {
          let section = max(numberOfSections - 1, 0)
          let row = max(numberOfRows(inSection: section) - 1, 0)

          return IndexPath(row: row, section: section)
      }
  }
