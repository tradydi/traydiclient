//
//  ProfileImageViewCell.swift
//  Traydi
//
//  Created by mac on 25/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
class ProfileImageViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewbg: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
    }
    
}
