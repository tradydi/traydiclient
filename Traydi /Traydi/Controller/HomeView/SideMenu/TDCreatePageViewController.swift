//
//  TDCreatePageViewController.swift
//  Traydi
//
//  Created by mac on 27/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class TDCreatePageViewController: TDBaseViewController {
    
    @IBOutlet weak var tableMember: UITableView!
    @IBOutlet weak var segSelectPermission: UISegmentedControl!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var imgVPage: UIImageView!
    @IBOutlet weak var constratintMemberHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTop: UIView!
    var imagePicker: TDImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let custombar = self.tabBarController as! CustomTabBarController
            custombar.isHiddenTabBar(hidden: true)
        self.rightandLeftBottmCornerRadius(view: viewTop)
        imagePicker = TDImagePicker(presentationController: self, delegate: self)
       // updateViewConstraints()
    }
    override func updateViewConstraints() {
        constratintMemberHeight.constant = tableMember.contentSize.height
        super.updateViewConstraints()
    }
    
    @IBAction func actionUpload(_ sender: UIButton) {
        imagePicker.present(from: sender)
    }
    
    @IBAction func actionInviteMember(_ sender: Any) {
      let vc = InviteMembersViewController.instance(.home) as! InviteMembersViewController
     
      self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //MARK:- Validation
    func validation() -> (status:Bool, message:String) {
        var msg:String = ""
        var status:Bool = true
        if txtTitle.text!.isEmpty {
            msg  = UseCaseMessage.validation.empty.kTitle
            status =  false
        } else if imgVPage.image == UIImage(named: "") {
            msg = UseCaseMessage.validation.empty.kPageImage
            status = false
        }
        
        return (status:status, message:msg)
    }
    
    @IBAction func actionCreatePage(_ sender: Any) {
        let valid = validation()
        if valid.status {
            var params = [String:Any]()
            params[ApiConstants.key.kPage_tittle] = txtTitle.text
           // AppSignleHelper.shard.login.userinfo?.id ??
            params[ApiConstants.key.kuser_id] =  "5"
            var imageParams = [[String:Any]]()
            imageParams.append([ApiConstants.key.kimage:imgVPage.image as Any])
            let apiAction = ApiAction.TradersAndTradees.kadd_page
            APIManager.share.delegate = self
            APIManager.share.performImageUpload(params: params, apiAction: apiAction, imageParams: imageParams)
            
        } else {
            Util.showAlertWithCallback(AppName, message: valid.message, isWithCancel: false)
        }
    }
}

extension TDCreatePageViewController:TDImagePickerDelegate {
    func didSelect(image: UIImage?) {
        imgVPage.image = image
        imgVPage.layer.cornerRadius = imgVPage.frame.size.height / 2
        imgVPage.clipsToBounds = true
        imgVPage.contentMode = .scaleAspectFill
    }
}

extension TDCreatePageViewController:UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "identifierMember", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension TDCreatePageViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        
        if check == ApiAction.TradersAndTradees.kadd_page {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let code  = resultObject["code"] as? String
                let message = resultObject["message"] as? String
                if code == ApiResponseStatus.kAPI_Result_Success {
                   Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                } else {
                    Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                }
            } catch {
                print("Unable to parse JSON response")
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
}
