//
//  InviteMembersViewController.swift
//  Traydi
//
//  Created by mac on 05/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit

class InviteMembersViewController: UIViewController {
    
    @IBOutlet weak var tblViewMembers: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblViewMembers.delegate = self
        self.tblViewMembers.dataSource = self
    }
    
    
    
}


extension InviteMembersViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "InviteMemberViewCell", for: indexPath) as! InviteMemberViewCell
        
        return cell
        
    }

}
