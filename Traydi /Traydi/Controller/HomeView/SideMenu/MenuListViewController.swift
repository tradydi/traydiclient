//
//  MenuListViewController.swift
//  Traydi
//
//  Created by mac on 21/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import SDWebImage
protocol SidemenuDelegate:NSObject {
    func didSelectRowAt(indexPath:IndexPath, pagesData:DataFetcherPagesModal)
    func actionCreatePage(viewController:MenuListViewController)
}

class MenuListViewController: TDBaseViewController {
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var progressBarStatus: UIProgressView!
    @IBOutlet weak var tblViewOptionList: UITableView!
    var arrSideMenuList = [DataFetcherSideMenuPages]()
    @IBOutlet weak var lblUserName: UILabel!
    
    
    weak var delegate:SidemenuDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let arrMenu = [["id": "", "user_id": "", "page_tittle": "Photo", "image": "", "created_at": ""],
            ["id": "", "user_id": "", "page_tittle": "Work Type", "image": "", "created_at": ""],
            ["id": "", "user_id": "", "page_tittle": "Experience", "image": "", "created_at": ""],
            ["id": "", "user_id": "", "page_tittle": "Business Details", "image": "", "created_at": ""],
            ["id": "", "user_id": "", "page_tittle": "Skill Required", "image": "","created_at": ""],
            ["id": "", "user_id": "", "page_tittle": "Work Radius", "image": "", "created_at": ""],
            ["id": "", "user_id": "", "page_tittle": "Credentials", "image": "", "created_at": ""],
            ] as [[String:Any]]
        var sidemenu = [DataFetcherPagesModal]()
        
        for item in arrMenu {
            sidemenu.append(DataFetcherPagesModal(parameters: item))
        }
        let userInfo = AppSignleHelper.shard.login.userinfo
        lblUserName.text = userInfo?.full_name
      //  imgViewProfile.cacheImage(urlString: ImagesUrl.baseProfileUri + userInfo!.image!)
        imgViewProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
         imgViewProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + userInfo!.image!), placeholderImage: UIImage(named: "camerawhite"))
        arrSideMenuList.append(DataFetcherSideMenuPages(sideMenuTitle: "", pages: sidemenu))
        
        tblViewOptionList.register(UINib(nibName: "TDSideMenuHeaderTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDSideMenuHeaderTableViewCell")
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
       // self.isSetupPageListApiCall()
        
        // FIXME: HIMANSHU PAL 10/8/20
       /* let pages = AppSignleHelper.shard.login.userinfo?.pages
        var sideMenu = [DataFetcherPagesModal]()
        for page in pages! {
            let dict = ["id":page.id,"user_id":page.user_id,"page_tittle":page.page_tittle,"image":page.image,"created_at":page.created_at]
            sideMenu.append(DataFetcherPagesModal(parameters: dict as [String : Any]))
        }
        self.arrSideMenuList.append( DataFetcherSideMenuPages(sideMenuTitle: "Pablic page", pages: sideMenu))*/
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userInfo = AppSignleHelper.shard.login.userinfo
        lblUserName.text = userInfo?.full_name
      //  imgViewProfile.cacheImage(urlString: ImagesUrl.baseProfileUri + userInfo!.image!)
        imgViewProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
         imgViewProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + userInfo!.image!), placeholderImage: UIImage(named: "camerawhite"))
       

    }
    
    func isSetupPageListApiCall() {
        var params = [String:Any]()
        //AppSignleHelper.shard.login.userinfo?.id
        params[ApiConstants.key.kuser_id] = "1"
        let apiAction = ApiAction.TradersAndTradees.kPages_list
        APIManager.share.delegate = self
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
    
   
    
    @IBAction func crossBtnAction(_ sender: Any) {
        self.setupSideMenuAnimation(isAdd: false, toView: self.view)
    }
    
    @objc func createPageButtonAction(_ sender: Any){
        print("Create PAge Button Action")
    }
}

//MARK: - APIManager response
extension MenuListViewController:ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        
        if check == ApiAction.TradersAndTradees.kPages_list {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let code  = resultObject["code"] as? String
                let message = resultObject["message"] as? String
                
                if code == ApiResponseStatus.kAPI_Result_Success {
                    let pages = resultObject["pages"] as? [String:Any]
                    let pagesList = pages!["pages"] as?  [[String:Any]]
                    var sideMenu = [DataFetcherPagesModal]()
                    
                    for page in pagesList! {
                        sideMenu.append(DataFetcherPagesModal(parameters: page))
                    }
                    self.arrSideMenuList.append( DataFetcherSideMenuPages(sideMenuTitle: "Pablic page", pages: sideMenu))
                    self.tblViewOptionList.reloadData()
                } else {
                    Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                }
                
            } catch {
                print("Unable to parse JSON response")
            }
        }
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension MenuListViewController: UITableViewDataSource, UITableViewDelegate, TDSideMEnuHeaderTableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSideMenuList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let pagesList = arrSideMenuList[section].pages
        
        return pagesList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 45
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TDSideMenuHeaderTableViewCell") as! TDSideMenuHeaderTableViewCell
            cell.backgroundColor = .white
            cell.delegate = self
            cell.btnCreatePage.tag = section
            return cell
        }
        return UIView()
    }
    func actionCreatePage(cell: TDSideMenuHeaderTableViewCell) {
        self.setupSideMenuAnimation(isAdd: false, toView: view)
        delegate?.actionCreatePage(viewController: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dict = arrSideMenuList[indexPath.section].pages![indexPath.row]
        
        if !dict.image!.isEmpty{
            let cell = tableView.dequeueReusableCell(withIdentifier: "identifierSideMenu", for: indexPath)
            // cell = UITableViewCell(style: .default, reuseIdentifier: "identifierSideMenu")
             cell.tag = indexPath.row
            
             cell.textLabel?.text = dict.page_tittle ?? ""
            print(ImagesUrl.basePostUri + dict.image!)
           // cell.imageView?.cacheImage(urlString: ImagesUrl.basePostUri + dict.image!)
            cell.imageView?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imageView?.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + dict.image!), placeholderImage: UIImage(named: "camerawhite"))
            
            cell.imageView?.layer.cornerRadius = (cell.imageView?.frame.size.height ?? 0) / 2
            cell.imageView?.clipsToBounds = true
             return cell
        } else {
             let cell = tableView.dequeueReusableCell(withIdentifier: "TDFirstSideMenuTableViewCell", for: indexPath) as! TDFirstSideMenuTableViewCell
            cell.lblTitle.text! = dict.page_tittle ?? ""
            cell.viewBg.clipsToBounds = true
            cell.tag = indexPath.row
            cell.viewBg.layer.cornerRadius =  cell.viewBg.frame.size.height / 2
            cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
            cell.viewBg.layer.borderWidth = 1
            cell.btnComplet.isSelected = false
            
            
            return cell
        }
        
    
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? TDFirstSideMenuTableViewCell  else {
            return
        }
        cell.btnComplet.isSelected = false
        if indexPath.row == 0{
            if  AppSignleHelper.shard.login.userinfo?.profile_image_status == "1"{
                cell.btnComplet.isSelected = true
            }
    }else  if indexPath.row == 1{
              if  AppSignleHelper.shard.login.userinfo?.work_type_status == "1"{
                  cell.btnComplet.isSelected = true
              }
      }
        else  if indexPath.row == 2{
                if  AppSignleHelper.shard.login.userinfo?.skill_level_status == "1"{
                    cell.btnComplet.isSelected = true
                }
        }
        else  if indexPath.row == 3{
                if  AppSignleHelper.shard.login.userinfo?.business_details_status == "1"{
                    cell.btnComplet.isSelected = true
                }
        }
        
        else  if indexPath.row == 4{
                if  AppSignleHelper.shard.login.userinfo?.skill_needed_status == "1"{
                    cell.btnComplet.isSelected = true
                }
        }
        else  if indexPath.row == 5{
                       if  AppSignleHelper.shard.login.userinfo?.work_radius_status == "1"{
                           cell.btnComplet.isSelected = true
                       }
               }
        
        else  if indexPath.row == 6{
                       if  AppSignleHelper.shard.login.userinfo?.credentials_status == "1"{
                           cell.btnComplet.isSelected = true
                       }
               }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         let dict = arrSideMenuList[indexPath.section].pages![indexPath.row]
        if !dict.image!.isEmpty {
          return 60
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let dict = arrSideMenuList[indexPath.section].pages![indexPath.row]
        if !dict.image!.isEmpty {
           
        } else {
            let cell = tableView.cellForRow(at: indexPath) as!TDFirstSideMenuTableViewCell
            UIView.animate(withDuration: 0.3, animations: {
                cell.viewBg.backgroundColor =   UIColor(named:"colorBaseApp")?.withAlphaComponent(0.5)
            }) { (status) in
                cell.viewBg.backgroundColor = .white
            }
        }
        
        //UIView.animate(withDuration: 0.5) {
            self.setupSideMenuAnimation(isAdd: false, toView: self.view)
        self.delegate?.didSelectRowAt(indexPath: indexPath ,pagesData: dict)
       // }
    }
    
    func imageWithImage(image:UIImage,scaledToSize newSize:CGSize)->UIImage{
      UIGraphicsBeginImageContext( newSize )
        image.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.automatic)
    }
    
}
