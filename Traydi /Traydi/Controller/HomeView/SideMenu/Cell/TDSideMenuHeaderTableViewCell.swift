//
//  TDSideMenuHeaderTableViewCell.swift
//  Traydi
//
//  Created by mac on 23/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import UIKit
protocol TDSideMEnuHeaderTableViewDelegate:NSObject {
    func actionCreatePage(cell:TDSideMenuHeaderTableViewCell)
}

class TDSideMenuHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnCreatePage: UIButton!
    weak var delegate:TDSideMEnuHeaderTableViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionCreatePage(_ sender: Any) {
        delegate?.actionCreatePage(cell: self)
    }
}
