//
//  ShareOptionViewController.swift
//  Traydi
//
//  Created by mac on 26/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
protocol ShareOptionViewDelegate: NSObject  {
    func shareOptionViewBackgroundClicked()
}



class ShareOptionViewController: UIViewController {
    
    weak var delegate:ShareOptionViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func backgroundBtnAction(_ sender: Any) {
        if delegate != nil {
            delegate?.shareOptionViewBackgroundClicked()
        }
    }
    
}
