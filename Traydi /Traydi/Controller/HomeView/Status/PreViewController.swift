//
//  PreViewController.swift
//  ARStories
//
//  Created by Antony Raphel on 05/10/17.
//

import UIKit
import AVFoundation
import AVKit
import CoreMedia
import SDWebImage

class PreViewController: UIViewController, SegmentedProgressBarDelegate , UITextFieldDelegate {

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    var pageIndex : Int = 0
    var rowIndex : Int = 0
    var items: [UserDetails] = []
    var item: [Content] = []
    var SPB: SegmentedProgressBar!
    var player: AVPlayer!
 //   let loader = ImageLoader()
    @IBOutlet weak var viewConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var txtVConstraintHeight: NSLayoutConstraint!
    @IBOutlet var viewReactionBG: UIView!
    @IBOutlet weak var btnReaction: UIButton!
    @IBOutlet weak var txtVMsg: UITextView!
    @IBOutlet var btnGif1 : UIButton!
    @IBOutlet var btnGif2 : UIButton!
    @IBOutlet var btnGif3 : UIButton!
    @IBOutlet var btnGif4 : UIButton!
    @IBOutlet var btnGif5 : UIButton!
    @IBOutlet var btnGif6 : UIButton!
    var arrDocument = [[String:Any]]()
    var profileData = [String:Any]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(items[pageIndex].story?.count)
        if items[pageIndex].story?.count ?? 0 == 0{
            self.dismiss(animated: true, completion: nil)
            return
            }
        let imgUrl = items[pageIndex].image!
        userProfileImage.layer.cornerRadius = self.userProfileImage.frame.size.height / 2;
     //   userProfileImage.imageFromServerURL(ImagesUrl.baseProfileUri +  imgUrl)
         userProfileImage.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
         userProfileImage.sd_setImage(with: URL(string:ImagesUrl.baseProfileUri +  imgUrl), placeholderImage: UIImage(named: "images"))
        lblUserName.text = items[pageIndex].full_name
        item = items[pageIndex].story ?? []
        
        SPB = SegmentedProgressBar(numberOfSegments: item.count, duration: 5)
        if #available(iOS 11.0, *) {
            SPB.frame = CGRect(x: 18, y: UIApplication.shared.statusBarFrame.height + 5, width: view.frame.width - 35, height: 3)
        } else {
            // Fallback on earlier versions
            SPB.frame = CGRect(x: 18, y: 15, width: view.frame.width - 35, height: 3)
        }
        
        SPB.delegate = self
        SPB.topColor = UIColor.white
        SPB.bottomColor = UIColor.white.withAlphaComponent(0.25)
        SPB.padding = 2
        SPB.isPaused = true
        SPB.currentAnimationIndex = 0
        SPB.duration = getDuration(at: 0)
        view.addSubview(SPB)
        view.bringSubviewToFront(SPB)
        
        let tapGestureImage = UITapGestureRecognizer(target: self, action: #selector(tapOn(_:)))
        tapGestureImage.numberOfTapsRequired = 1
        tapGestureImage.numberOfTouchesRequired = 1
        imagePreview.addGestureRecognizer(tapGestureImage)
        
        let tapGestureVideo = UITapGestureRecognizer(target: self, action: #selector(tapOn(_:)))
        tapGestureVideo.numberOfTapsRequired = 1
        tapGestureVideo.numberOfTouchesRequired = 1
        videoView.addGestureRecognizer(tapGestureVideo)
        //
        let custombar = self.tabBarController as? CustomTabBarController
               custombar?.isHiddenTabBar(hidden: true)
               self.txtVMsg.delegate = self
               self.txtVMsg.textColor = .lightGray
       /// NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardDidShowNotification, object: nil)
            //   NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let longGestureVote = UILongPressGestureRecognizer(target: self, action: #selector(self.longTapVote(sender:)))
               btnReaction.addGestureRecognizer(longGestureVote)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        UIView.animate(withDuration: 0.8) {
            self.view.transform = .identity
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            print(self.items[self.pageIndex].story?.count)
            if self.items[self.pageIndex].story?.count ?? 0 > 0{
            self.SPB.startAnimation()
            self.playVideoOrLoadImage(index: 0)
            }else{
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       DispatchQueue.main.async {
        if self.SPB != nil {
            self.SPB.currentAnimationIndex = 0
            self.SPB.cancel()
            self.SPB.isPaused = true
          //  self.resetPlayer()
        }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - SegmentedProgressBarDelegate
    //1
    func segmentedProgressBarChangedIndex(index: Int) {
        playVideoOrLoadImage(index: index)
    }
    
    //2
    func segmentedProgressBarFinished() {
        print(pageIndex)
        print(self.items.count - 1)
        if pageIndex == (self.items.count - 1) {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            _ = ContentViewControllerVC.goNextPage(fowardTo: pageIndex + 1)
        }
    }
    func segmentedProgressBarPrevious() {
          if pageIndex > 0 {
            
              _ = ContentViewControllerVC.goNextPage(fowardTo: pageIndex - 1)
          }
      }
    
    @objc func tapOn(_ sender: UITapGestureRecognizer) {
        SPB.skip()
    }
    
    //MARK: - Play or show image
    func playVideoOrLoadImage(index: NSInteger) {
        
        rowIndex = index
        self.SPB.duration = 5
        self.imagePreview.isHidden = false
        self.videoView.isHidden = true
         self.SPB.isPaused  = true
        self.imagePreview.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        
        self.imagePreview.sd_setImage(with: URL(string: ImagesUrl.storyImage  +  item[index].image!)) { (image, error, imageCacheType, imageUrl) in
            if image != nil{
            print("image found")
                self.SPB.isPaused  = false
            }else{
            print("image not found")
            self.SPB.skip()
            }
        }
        
        
    }
    
    // MARK: Private func
    private func getDuration(at index: Int) -> TimeInterval {
        var retVal: TimeInterval = 5.0
            retVal = 5.0
       
        return retVal
    }
    
    private func resetPlayer() {
        if player != nil {
            player.pause()
            player.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    //MARK: - Button actions
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
     //   resetPlayer()
    }
    
    //---------------------------------------------------------
    @objc func longTapVote(sender : UIGestureRecognizer) {
        //        viewForFrame.frame = UIScreen.main.bounds
        
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            self.SPB.isPaused  = true
            addViewReaction()
            
        }
    }
    @IBAction func actionLike(_ sender: Any) {
         viewReactionBG.isHidden = true
    }
    
    @IBAction func actionSendComment(_ sender: Any) {
        
        // FIXME: HIMANSHU PAL add comment to stroy section
        if  txtVMsg.text == "Add a Comment..." ||  txtVMsg.text.removingWhitespaces() == ""{
            return
            }
        postCommentonStroyApi()
        
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
               print("keyboardWillShow")
              let info = notification.userInfo!
              let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
              UIView.animate(withDuration: 0.1) {
                //  self.viewConstraintBottom.constant += (keyboardFrame.size.height)
              }
          }

          @objc func keyboardWillHide(notification: NSNotification){
               print("keyboardWillHide")
              UIView.animate(withDuration: 0.1) {
                  self.viewConstraintBottom.constant = 0
              }
          }
       func textFieldDidBeginEditing(_ textField: UITextField) {
           btnReaction.isHidden = true
       }
}





extension PreViewController:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
         self.SPB.isPaused  = true
        if txtVMsg.textColor ==  .lightGray {
            txtVMsg.textColor = .white
            txtVMsg.text = ""
           
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
         self.SPB.isPaused  = false
        if txtVMsg.text.isEmpty {
            txtVMsg.textColor = .lightGray
            txtVMsg.text = "Add a Comment..."
          
        }
    }

   func textViewDidChange(_ textView: UITextView) {
           let textHeight = textView.contentSize.height
           if textHeight < 100 {
               txtVConstraintHeight.constant = textHeight
           }
       }
       func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
           if(text == "\n") {
               textView.resignFirstResponder()
               return false
           }
           return true
       }

}





extension PreViewController{
    func addViewReaction(){
        viewReactionBG.isHidden = false
           let jeremyGif = UIImage.gifImageWithName("AnimatedGIFsource")
           let imageView = UIImageView(image: jeremyGif)
           imageView.frame = CGRect(x: 0, y: 0, width: viewReactionBG.frame.size.width, height: viewReactionBG.frame.size.height)
           imageView.layer.cornerRadius =  imageView.frame.size.height/2
           imageView.clipsToBounds = true
           self.viewReactionBG.addSubview(imageView)
           viewReactionBG.sendSubviewToBack(imageView)
           viewReactionBG.backgroundColor = UIColor.clear
           let width = viewReactionBG.frame.size.width/6
           
           btnGif1.frame = CGRect(x: 0, y: 0, width: width, height: width)
           btnGif2.frame = CGRect(x: width, y: 0, width: width, height: width)
           btnGif3.frame = CGRect(x: width*2, y: 0, width: width, height: width)
           btnGif4.frame = CGRect(x: width*3, y: 0, width: width, height: width)
           btnGif5.frame = CGRect(x: width*4, y: 0, width: width, height: width)
           btnGif6.frame = CGRect(x: width*5, y: 0, width: width, height: width)
           
       }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
             self.view.endEditing(true)
        viewReactionBG.isHidden = true
         }
    func unregisterNotifications() {
             NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
             NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
         }
    
    @IBAction func actionGif1(_ sender: Any) {
        print("actionGif1"  )
        
        addRectionApi(imgName: "emoji1")

    }
    @IBAction func actionGif2(_ sender: Any) {
        print("actionGif2"  )
       addRectionApi(imgName: "emoji2")
    }
    @IBAction func actionGif3(_ sender: Any) {
        print("actionGif3"  )
     addRectionApi(imgName: "emoji3")
    }
    @IBAction func actionGif4(_ sender: Any) {
        print("actionGif4"  )
       addRectionApi(imgName: "emoji4")
    }
    @IBAction func actionGif5(_ sender: Any) {
        print("actionGif5"  )
       addRectionApi(imgName: "emoji5")
    }
    
    @IBAction func actionGif6(_ sender: Any) {
          print("actionGif6"  )
         addRectionApi(imgName: "emoji6")
      }
    
    
    func addRectionApi(imgName : String){
         self.SPB.isPaused  = true
        APIManager.share.delegate = self
        viewReactionBG.isHidden = true
        DispatchQueue.main.async {
            let image: UIImage = UIImage(named: "emoji1")!
            self.arrDocument.removeAll()
            self.profileData[ApiConstants.key.kimagee] = image
            self.arrDocument.append(self.profileData)
            var params = [String:Any]()
            print(self.arrDocument)
            params[ApiConstants.key.kuser_type] = self.item[self.rowIndex].user_type //item.user_type
            params[ApiConstants.key.kstory_id] = self.item[self.rowIndex].user_id // self.arrListData?.id
            params[ApiConstants.key.kcomment] = self.txtVMsg.text
            params[ApiConstants.key.kid] = self.item[self.rowIndex].id
            print(params)
            let apiAction = ApiAction.TradersAndTradees.kadd_reaction
            print(apiAction)
            APIManager.share.performImageUpload(params: params, apiAction: apiAction, imageParams: self.arrDocument)
        }
    }
    
}


extension PreViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
         self.SPB.isPaused  = false
        if check == ApiAction.TradersAndTradees.comment_story   {
            do {
                 let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let code  = resultObject["code"] as? String
                let message = resultObject["message"] as? String
                
                if code == ApiResponseStatus.kAPI_Result_Success {
                    Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                }
                  
                txtVMsg.text = ""
             } catch {
                     print("Unable to parse JSON response")
             }
        }else if check == ApiAction.TradersAndTradees.kadd_reaction   {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
               let code  = resultObject["code"] as? String
               let message = resultObject["message"] as? String
               
               if code == ApiResponseStatus.kAPI_Result_Success {
                   Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
               }
                 
               txtVMsg.text = ""
            } catch {
                    print("Unable to parse JSON response")
            }
        }
        
        
    }
    
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
        Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    
    func postCommentonStroyApi() {
        APIManager.share.delegate = self
        var params = [String:Any]()
      /*  params[ApiConstants.key.kuser_type] = arrListData?.user_type
        params[ApiConstants.key.kstory_id] = arrListData?.id
        params[ApiConstants.key.kcomment] = txtVMsg.text
        params[ApiConstants.key.kid] = arrListData?.user_id*/
        
        
                   params[ApiConstants.key.kuser_type] = item[rowIndex].user_type
                   params[ApiConstants.key.kstory_id] = item[rowIndex].user_id // self.arrListData?.id
                   params[ApiConstants.key.kcomment] = self.txtVMsg.text
                   params[ApiConstants.key.kid] = item[rowIndex].id
        
         let apiAction = ApiAction.TradersAndTradees.comment_story
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
}












extension UIImageView {
    func imageFromServerURL(_ URLString: String) {
        self.image = nil
        if let url = URL(string: URLString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    print("ERROR LOADING IMAGES FROM URL: \(String(describing: error))")
                    return
                }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            self.image = downloadedImage
                        }
                    }
                }
            }).resume()
        }
    }
}

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}
