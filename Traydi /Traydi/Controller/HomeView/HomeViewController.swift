//
//  HomeViewController.swift
//  Traydi
//
//  Created by mac on 21/12/19.
//  Copyright © 2019 Creative thought infotech. All rights reserved.
//

import UIKit
import GoogleMaps
//import GooglePlaces
import SocketIO
import AVKit
import SDWebImage


var arrProject = [[String : Any]]()

class HomeViewController: TDBaseViewController {
    
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnFeeds: UIButton!
    @IBOutlet weak var btnMyPosts: UIButton!
    var menuListViewController: MenuListViewController?
    @IBOutlet weak var profileImagesCollection: UICollectionView!
    @IBOutlet weak var tableViewFeed: UITableView!
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    var shareOptionViewController:ShareOptionViewController?
    var commentViewController: CommentViewController?
    var type = ""
    var arrPostlist = [Postinfo]()
    var statusDataFetcher: DataFetcherStatusModal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
   //  SocketIOManager.sharedInstance.establishConnection()
        self.viewHeader.layer.cornerRadius = 15.0
        self.viewHeader.clipsToBounds = true
        self.viewHeader.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
       
         profileImagesCollection.backgroundColor = UIColor.clear
         self.tableViewFeed.register(UINib(nibName: "FeedViewCell", bundle: nil), forCellReuseIdentifier: "FeedViewCell")
        self.tableViewFeed.register(UINib(nibName: "FeedViewMultipleCell", bundle: nil), forCellReuseIdentifier: "FeedViewMultipleCell")
       
        let sb = UIStoryboard(name: "Home", bundle: nil)
        menuListViewController = (sb.instantiateViewController(withIdentifier: "MenuListViewController") as! MenuListViewController)
        menuListViewController!.delegate = self
        print(btnFeeds.isSelected)
        print(btnMyPosts.isSelected)
        
//        let tabBarController = self.tabBarController as! CustomTabBarController
//                    tabBarController.isHiddenTabBar(hidden: true)
//                tabBarController.lblNotificationCount.text = "1"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.setNavigationBarHidden(false, animated: true)
        let custombar = self.tabBarController as! CustomTabBarController
        custombar.isHiddenTabBar(hidden: false)
        self.setupSideMenuImageView()
        tableViewFeed.register(UINib(nibName: "TDFeedSharePostTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TDFeedSharePostTableViewCell")
      //  self.postListApiCalling()
         self.feedsBtnAction(self.btnFeeds as Any)
        
      
       
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

    }
    
     func setupSideMenuImageView() {
       // imgViewProfile.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + userInfo!.image!), placeholderImage: UIImage(named: "camerawhite"))
        //let logoImage = UIImage.init(named: "shelesh")
         let userInfo = AppSignleHelper.shard.login.userinfo
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let imgV = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
      //  imgV.image = UIImage(named: "shelesh")
        imgV.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        print(userInfo!.image!)
        imgV.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + userInfo!.image!), placeholderImage: UIImage(named: "user"))
        imgV.contentMode = .scaleAspectFill
      //  imgV.layer.cornerRadius = 22
       // imgV.clipsToBounds = true
        let buttonProfile = UIButton(frame: view.frame)
        buttonProfile.addTarget(self, action: #selector(profilePicBtnAction), for: .touchUpInside)
        view.layer.cornerRadius = 22
        view.clipsToBounds = true
        view.addSubview(imgV)
        view.addSubview(buttonProfile)
        let barButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = barButtonItem
     }
 
    @IBAction func profilePicBtnAction(_ sender: Any) {
      self.navigationController?.view.addSubview(self.menuListViewController!.view)
        self.setupSideMenuAnimations(isAdd: true, toView:  self.menuListViewController!.view)
    }
    func setupSideMenuAnimations(isAdd:Bool, toView:UIView) {
        if isAdd {
            toView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: (view.frame.size.height + (navigationController?.navigationBar.frame.height)!))
            
            toView.frame.origin.y =  (view.frame.origin.y - (navigationController?.navigationBar.frame.height)!)
            toView.frame.origin.x = -self.view.frame.width
            toView.alpha = 0
            UIView.animate(withDuration: 0.5) {
                toView.frame.origin.x = 0
                toView.alpha = 1
               self.navigationController?.view.addSubview(toView)
                self.tabBarController?.view.addSubview(toView)
            }
        }
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "HomeSearchViewController")
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func feedsBtnAction(_ sender: Any) {
        self.btnFeeds.isSelected = true
        self.btnMyPosts.isSelected = false
        self.type = "Feeds"
          self.postListApiCalling()
        self.tableViewFeed.reloadData()
    }
    @IBAction func myPostsBtnAction(_ sender: Any) {
        self.btnFeeds.isSelected = false
        self.btnMyPosts.isSelected = true
        self.type = "My Post"
        self.postListApiCalling()
      //  let userData = AppSignleHelper.shard.login.userinfo
        
       // arrPostlist.filter { (postinfo) -> Bool in
          //  return postinfo.user_id == userData?.id
       // }
        self.tableViewFeed.reloadData()
    }
    @IBAction func actionMessage(_ sender: Any) {
   
    }
    
  /*  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IdentifierPost" {
            let vcPost = segue.destination as! CommentViewController
            vcPost.title = "Post"
            vcPost.postComment = (sender as! Postinfo)
            let custombar = self.tabBarController as! CustomTabBarController
                custombar.isHiddenTabBar(hidden: true)
        }
    }*/
}



extension HomeViewController: FeedViewCellDelegate ,HomeListingDeleteDelegate{
    func feedViewShareHeader(myIndex: Int) {
        
    }
    
    func feedViewLikeHeader(myIndex: Int) {
        
    }
    
    func deleteIndex(indexx: IndexPath, buttontag:Int) {
        self.dismiss(animated: true, completion: nil)
        print("buttontag-\(buttontag)-")
         let objPost = arrPostlist[indexx.row]
       if buttontag == 555 {
            
            // FIXME: HIMANSHU PAL:- delete post
            self.arrPostlist.remove(at:indexx.row)
            self.tableViewFeed.deleteRows(at: [indexx], with: .left)
        }else if buttontag == 111{
            // FIXME: HIMANSHU PAL:- report post
            let alertController = UIAlertController(title: "Report", message: "", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Enter Reason"
            }
            let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
                let strportText = alertController.textFields![0] as UITextField
                print("Reason \(strportText)")
                self.ReportApiCalling(strportText.text ?? "")
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in })
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }else if buttontag == 666{
            self.repostApiCalling(postID: objPost.id ?? "0", userType: objPost.user_type ?? "", createPostType: objPost.create_post_type ?? "1")
            //
        }
        else if buttontag == 333{
           // FIXME: HIMANSHU PAL:-  follow
            self.followApiCalling(objPost.user_id ?? "0")
            
        }
        
        
    }
    
   
    
   
    
   
    
    func feedViewPlayVideo(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
  
        let objPost = arrPostlist[indexPath.row]
        if  (objPost.image != nil) {
            if objPost.image?.count == 1 {
                 let videoURL = URL(string: ImagesUrl.basePostUri + objPost.image![0].image!)
                // Create an AVPlayer, passing it the HTTP Live Streaming URL.
                let player = AVPlayer(url: videoURL!)
                      let controller = AVPlayerViewController()
                      controller.player = player
                      present(controller, animated: true) {
                          player.play()
                      }
            }
        }
    }
    
  
    func feedViewControllerMenu(Cell: UITableViewCell , sender: UIButton) {
       /* guard let indexPath = tableViewFeed.indexPath(for: Cell) else {
            return
        }*/
        
        
        guard  let indexPath = tableViewFeed.indexPath(for: Cell) else {
                   return
               }
               
        var comentsPostID = arrPostlist[indexPath.row].user_id
        
        let alertController = storyboard?.instantiateViewController(withIdentifier: "TDPopMenuViewController") as! TDPopMenuViewController
        alertController.modalPresentationStyle = .popover
        alertController.preferredContentSize = CGSize(width: 200, height: 200)
        let popover = alertController.popoverPresentationController
        popover?.permittedArrowDirections =  .up
        popover?.delegate = self
        popover?.sourceView = sender
        alertController.myIndex = indexPath
        alertController.delegate = self
        if arrPostlist[indexPath.row].repost_by != ""{
           comentsPostID = arrPostlist[indexPath.row].repost_by
        }
        
        if self.btnFeeds.isSelected == true{
            
            if comentsPostID == AppSignleHelper.shard.login.userinfo?.id {
                alertController.typeButton  = "My Post"
                alertController.dictPostlist = arrPostlist[indexPath.row]
                alertController.preferredContentSize = CGSize(width: 200, height: 100)
            }else{
                alertController.typeButton = self.type
                alertController.preferredContentSize = CGSize(width: 200, height: 150)
            }
        }else{
            alertController.typeButton = self.type
            alertController.preferredContentSize = CGSize(width: 200, height: 100)
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func feedViewCell(cell:UITableViewCell) {
        
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        let objPost = arrPostlist[indexPath.row]
        let createPostType = arrPostlist[indexPath.row].create_post_type
        var someText  = ""
        var sharedObjects:[AnyObject]  = [AnyObject]()
        if createPostType == "1" || createPostType == ""{
            someText = objPost.tittle ?? ""
            sharedObjects    = [someText as AnyObject]
        }else{
            someText = objPost.descriptions ?? ""
            for dict in objPost.image!{
                let objectsToShare:URL = URL(string: ImagesUrl.baseProfileUri + dict.image! )!
                sharedObjects.append(objectsToShare as AnyObject)
            }
            
        }
        
        
        
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail]
        
        self.present(activityViewController, animated: true, completion: nil)
        
        
        
        /*  let sb = UIStoryboard(name: "Home", bundle: nil)
         let vc = sb.instantiateViewController(withIdentifier: "ShareOptionViewController") as! ShareOptionViewController
         vc.delegate = self
         self.shareOptionViewController = vc
         vc.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - 50)
         self.view.addSubview(vc.view)
         self.view.bringSubviewToFront(vc.view)*/
        
    }
    
    // MARK:-  comments
    func feedViewCellWillComment(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        
        let comentsPost = arrPostlist[indexPath.row]
        let createPostType = arrPostlist[indexPath.row].create_post_type
        let vc =  CommentViewController.instance(.home) as! CommentViewController
        vc.postComment = comentsPost
       // if  createPostType == "1"{
             vc.title = "Post"
      //  }
       // else{
        //     vc.title = "Posts"
      //  }
       
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true) {
            //completion block
      //  }
    }
    //MARK:- Likes
    func feedViewCellLikePost(cell: UITableViewCell) {
        guard  let indexPath = tableViewFeed.indexPath(for: cell) else {
            return
        }
        guard let celli = cell as? FeedViewMultipleCell else {
            return
        }
        
        let user_id = AppSignleHelper.shard.login.userinfo?.id
        let likeData = arrPostlist[indexPath.row]
        
        var params = [String:Any]()
        params["user_id"] = user_id ?? "0"
        params["post_id"] = likeData.id ?? "0"
        
        let apiAction = ApiAction.TradersAndTradees.kLikePost
        APIManager.share.performPostApiHandler(params: params, apiAction: apiAction, successCompletionHandler: { (response) in
            let likedata = response.result.value as? [String:Any]
            let likeResponse = LikeModel(parameters:likedata!)
            self.arrPostlist[indexPath.row].total_like = likeResponse.likes
            if  likeResponse.liked  == 1{
                //likepost
                self.arrPostlist[indexPath.row].is_like = "1"
                celli.imgLike.image = UIImage(named: "likepost")
            }else{
                 self.arrPostlist[indexPath.row].is_like = "0"
                 celli.imgLike.image = UIImage(named: "likecomments")
            }
            
            self.tableView(self.tableViewFeed, willDisplay: cell, forRowAt: indexPath)

        }) { (error) in
         Util.showAlertWithCallback(AppName, message: error.localizedDescription , isWithCancel: false)
        }
    }
}
extension HomeViewController: ShareOptionViewDelegate , UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
         return .none
     }

        
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
         return true
    }
    func shareOptionViewBackgroundClicked() {
        
        self.shareOptionViewController?.view.removeFromSuperview()
    }
}
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int  {
        print(statusDataFetcher?.story?.count)
        if statusDataFetcher?.story?.count ?? 0 > 0{
            return (statusDataFetcher?.story!.count)!
        }
        return  0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.profileImagesCollection.dequeueReusableCell(withReuseIdentifier: "ProfileImageViewCell", for: indexPath) as! ProfileImageViewCell
        
        let dict = statusDataFetcher?.story?[indexPath.row]
        if dict?.image?.count ?? 0 >  0 {
            cell.imgView.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgView.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + (dict?.image)!), placeholderImage: UIImage(named: "images"))
        }else{
            cell.imgView.image = UIImage(named: "images")
        }
        
      
     //   cell.imgView.image = UIImage(named: "post_img")
        cell.imgView.layer.cornerRadius = cell.imgView.frame.height / 2
        cell.viewbg.layer.cornerRadius = cell.viewbg.frame.height / 2
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           let dict = statusDataFetcher?.story?[indexPath.row]
        if dict?.story?.count ?? 0 > 0{
          /*  let vc = TDStatusPageViewController.instance(.home) as! TDStatusPageViewController
           // vc.strName = dict?.full_name ?? ""
           // vc.imgUser = ImagesUrl.baseProfileUri + (dict?.image)!
            vc.pages = statusDataFetcher?.story! as! [UserDetails]
            vc.currentIndex = indexPath.row
                   vc.title = "Status"
                   self.navigationController?.pushViewController(vc, animated: true)*/
            
            DispatchQueue.main.async {
                       let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContentView") as! ContentViewController
                       vc.modalPresentationStyle = .overFullScreen
                vc.pages = self.statusDataFetcher?.story! as! [UserDetails]
                     vc.title = "Status"
                       vc.currentIndex = indexPath.row
                       self.present(vc, animated: true, completion: nil)
                   }
            
            
            
        }else{
            print("No story available")
            }
       
        //  self.performSegue(withIdentifier: "identifierStatus", sender: self)
    }
    
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPostlist.count
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    let cell = tableView.dequeueReusableCell(withIdentifier: "FeedViewMultipleCell")  as! FeedViewMultipleCell
        cell.delegate = self
        let objPost = arrPostlist[indexPath.row]
    if objPost.repost_full_name != ""{
        cell.stkRepost.isHidden = false
              cell.lblTitle.text = objPost.repost_full_name
               cell.imgProfile?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
               cell.imgProfile?.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + objPost.repost_image!),placeholderImage: UIImage(named: "user"))
               //---
               cell.lblRepostToName.text = objPost.full_name
               cell.imgeRePostTo?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
               cell.imgeRePostTo?.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + objPost.profile_image!),placeholderImage: UIImage(named: "user"))
        
    }else{
       cell.stkRepost.isHidden = true
       cell.lblTitle.text = objPost.full_name
         cell.imgProfile?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
       cell.imgProfile?.sd_setImage(with: URL(string: ImagesUrl.baseProfileUri + objPost.profile_image!),placeholderImage: UIImage(named: "user"))
        
    }
    
    //like button
    if  objPost.is_like  == "1"{
        cell.imgLike.image = UIImage(named: "likepost")
    }else{
         cell.imgLike.image = UIImage(named: "likecomments")
    }
    if objPost.total_like == "0"{
        cell.lblLike.text = objPost.total_like ?? "0" + " like"
    }
   else if objPost.total_like == "1"{
        cell.lblLike.text = objPost.total_like ?? "1" + " like"
    }else{
        cell.lblLike.text = objPost.total_like ?? "2" + " likes"
    }
    //comment
    if objPost.comments?.count == 0{
        cell.lblcomment.text =   "\(objPost.comments?.count ?? 0) comment"
       }else if objPost.comments?.count == 1{
           cell.lblcomment.text = "\(objPost.comments?.count ?? 1) comment"
       }
    else{
           cell.lblcomment.text = "\(objPost.comments?.count ?? 1) comments"
       }

     
    
   let aa =  stringToDateWithFormat(strDate: objPost.created_at! ,dateFormat: "dd-MM-yyyy HH:mm:ss")
    let utcDate = aa!.toGlobalTime()
    cell.lblTime.text = utcDate.timeAgoSinceDate()
    if objPost.tittle != "" {
        cell.lblFeedText.text = objPost.tittle
    }else{
        cell.lblFeedText.text = objPost.descriptions
    }
        if self.btnFeeds.isSelected == true {
                  
              }else{

              }
       
        cell.imgViewFeed.isHidden = true
        cell.viewAllImg.isHidden = true
        cell.stackTwoImage.isHidden = true
        cell.constraintImageHeight.constant = 0
         cell.btnPlay.isHidden = true
        
        if  (objPost.image != nil) {
             cell.constraintImageHeight.constant = 170
            if objPost.image?.count == 1 {
                  
                if  objPost.create_post_type == "2"{
                     let videoURL = URL(string: ImagesUrl.basePostUri + objPost.image![0].image!)
                    cell.imgViewFeed.image = nil
                    let img = self.getImageFromCache(urlString: (videoURL)!.absoluteString)
                    if img == nil {
                        getThumbnailImageFromVideoUrl(url: videoURL!) { (img,url)  in
                            cell.imgViewFeed?.image =    img
                            if img != nil && url != nil{
                                self.cacheImage(urlString: (url)?.absoluteString ?? "", img: img!)
                            }
                            
                        }
                    }else{
                        cell.imgViewFeed?.image =  img
                    }
                    
                    cell.btnPlay.isHidden = false
                }else{
                    cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                    cell.imgViewFeed?.sd_setImage(with: URL(string: ImagesUrl.basePostUri + objPost.image![0].image!), placeholderImage: UIImage(named: "user"))
                    cell.btnPlay.isHidden = true
                }
                cell.imgViewFeed.isHidden = false
                 cell.viewAllImg.isHidden = false
                cell.stackTwoImage.isHidden = true
            } else if objPost.image?.count == 2 {
                cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                 cell.imgViewFeed?.sd_setImage(with: URL(string: ImagesUrl.basePostUri + objPost.image![0].image!), placeholderImage: UIImage(named: "user"))
                cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                 cell.imgViewFeed1?.sd_setImage(with: URL(string: ImagesUrl.basePostUri + objPost.image![1].image!), placeholderImage: UIImage(named: "user"))
                cell.imgViewFeed1.isHidden = false
                cell.imgViewFeed.isHidden = false
                 cell.viewAllImg.isHidden = false
                cell.stackTwoImage.isHidden = false
                cell.viewMoreImages.isHidden = true
            }else if objPost.image?.count == 3 {
                 cell.imgViewFeed.isHidden = false
                cell.imgViewFeed?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgViewFeed?.sd_setImage(with: URL(string: ImagesUrl.basePostUri + objPost.image![0].image!), placeholderImage: UIImage(named: "user"))
                cell.imgViewFeed1?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                 cell.imgViewFeed1?.sd_setImage(with: URL(string: ImagesUrl.basePostUri + objPost.image![1].image!), placeholderImage: UIImage(named: "user"))
                cell.imgViewFeed2?.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
                cell.imgViewFeed2?.sd_setImage(with: URL(string: ImagesUrl.basePostUri + objPost.image![2].image!), placeholderImage: UIImage(named: "user"))
                cell.imgViewFeed.isHidden = false
                 cell.viewAllImg.isHidden = false
                cell.imgViewFeed1.isHidden = false
                cell.stackTwoImage.isHidden = false
                cell.viewMoreImages.isHidden = false
            }else{
            cell.imgViewFeed.image  = UIImage(named: "user")
            }
        }
       /*  if indexPath.row == 9 {
            let cellshare = tableView.dequeueReusableCell(withIdentifier: "TDFeedSharePostTableViewCell")  as! TDFeedSharePostTableViewCell
            return cellshare
        }*/
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let objPost = arrPostlist[indexPath.row]
        if objPost.post_type  == "" {
            
        } else {
            guard let cell = cell as? FeedViewMultipleCell else { return }
            cell.lblLike.text = objPost.total_like! + " Like"
        }
    }
      
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !arrPostlist[indexPath.row].image!.isEmpty && arrPostlist[indexPath.row].image?.count ?? 0 > 1{
            let dict = arrPostlist[indexPath.row]
                       let vc = MultipleImageVC.instance(.home) as! MultipleImageVC
                               vc.postComment = dict
                       self.navigationController?.pushViewController(vc, animated: true)
        } else {
          //  self.performSegue(withIdentifier: "IdentifierPost", sender: arrPostlist[indexPath.row])
           
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage? , _ url : URL?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage, url) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil, nil) //11
                }
            }
        }
    }
}

// sideMenu delegate
extension HomeViewController:SidemenuDelegate {
    func didSelectRowAt(indexPath: IndexPath, pagesData:DataFetcherPagesModal) {
        
        if indexPath.section == 0 {
            
            switch indexPath.row {
            case 0:
                let vc = ProfilePictureViewController.instance(.authentication) as! ProfilePictureViewController
                vc.controllerType = true
                vc.title = "Edit"
                self.navigationController?.pushViewController(vc, animated: true)
            case 1:
                let vc = WorkTypeViewController.instance(.authentication) as! WorkTypeViewController
                vc.controllerType = true
                vc.title = "Edit"
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                let vc = ExperienceViewController.instance(.authentication) as! ExperienceViewController
                vc.controllerType = true
                vc.title = "Edit"
                self.navigationController?.pushViewController(vc, animated: true)
            case 3:
                let vc = BusinessDetailsViewController.instance(.authentication) as! BusinessDetailsViewController
                vc.controllerType = true
                vc.title = "Edit"
                self.navigationController?.pushViewController(vc, animated: true)
            case 4:
                let vc = SkillsViewController.instance(.authentication) as! SkillsViewController
                vc.controllerType = true
                vc.title = "Edit"
                self.navigationController?.pushViewController(vc, animated: true)
            case 5:
                if !CLLocationManager.locationServicesEnabled() {
                    Util.showAlertWithCallback(AppName, message: "Please go to setting and turn on the location", isWithCancel: true) {
                        UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                    }
                }else{
                    let vc = WorkRadiusViewController.instance(.authentication) as! WorkRadiusViewController
                    vc.controllerType = true
                    vc.title = "Edit"
                    self.navigationController?.pushViewController(vc, animated: true)
                 }
                
            case 6:
                let vc = AddCredentialViewController.instance(.authentication) as! AddCredentialViewController
                vc.controllerType = true
                vc.title = "Edit"
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
        } else if indexPath.section == 1 {
            let vc = TDPublicPageViewController.instance(.home) as! TDPublicPageViewController
            vc.pageData = pagesData
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func actionCreatePage(viewController: MenuListViewController) {
        let vc = TDCreatePageViewController.instance(.home) as! TDCreatePageViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension HomeViewController {
    func postListApiCalling() {
        APIManager.share.delegate = self
        var params = [String:Any]()
        params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id
         var apiAction = ApiAction.TradersAndTradees.kPostList
        if btnFeeds.isSelected{
            apiAction = ApiAction.TradersAndTradees.kPostList
        }else{
            apiAction = ApiAction.TradersAndTradees.kpost_list_by_user
            
        }
       
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
    func ReportApiCalling(_ reason : String) {
        APIManager.share.delegate = self
        var params = [String:Any]()
        params[ApiConstants.key.kreport_by] = AppSignleHelper.shard.login.userinfo?.id
        params[ApiConstants.key.kreason] = reason
        
         let apiAction = ApiAction.TradersAndTradees.kreport
       
        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
    func followApiCalling(_ receiverId : String) {
           APIManager.share.delegate = self
           var params = [String:Any]()
        params[ApiConstants.key.ConnectionRequestkeys.kreceiver_id] = receiverId
        params[ApiConstants.key.ConnectionRequestkeys.ksender_id] = AppSignleHelper.shard.login.userinfo?.id
           
        let apiAction = ApiAction.TradersAndTradees.kfollow
          print(apiAction)
        print(params)

           APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
       }
    
    
    func repostApiCalling(postID :String , userType: String , createPostType: String) {
        APIManager.share.delegate = self
        var params = [String:Any]()
        
        params[ApiConstants.key.kpost_id] = postID
        params[ApiConstants.key.krepost_by] = AppSignleHelper.shard.login.userinfo?.id
        params[ApiConstants.key.krepost_user_type] = userType
        params[ApiConstants.key.kcreate_post_type] = createPostType
        
        let apiAction = ApiAction.TradersAndTradees.krepost
         print(apiAction)
         print(params)

        APIManager.share.performMaltipartPost(params: params, apiAction: apiAction)
    }
    
    
    
    
    
}

extension HomeViewController : ApiManagerDelegate {
    func successResponse(response: Any, check: String) {
        guard let data = response as? Data else {
            return
        }
        if check == ApiAction.TradersAndTradees.kPostList || check == ApiAction.TradersAndTradees.kpost_list_by_user  {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let postModal = Postlist(parameters:resultObject)
                if postModal.code == ApiResponseStatus.kAPI_Result_Success {
                    self.arrPostlist.removeAll()
                    self.arrPostlist = postModal.postinfo ?? []
                    self.tableViewFeed.reloadData()
                    let apiActionStatus = ApiAction.TradersAndTradees.kmy_story
                    var params = [String:Any]()
                    params[ApiConstants.key.kuser_id] = AppSignleHelper.shard.login.userinfo?.id
                    APIManager.share.performMaltipartPost(params: params, apiAction: apiActionStatus)
                } else {
                    self.arrPostlist.removeAll()
                    self.tableViewFeed.reloadData()
                    Util.showAlertWithCallback(AppName, message: postModal.message, isWithCancel: false)
                }
                 
            } catch {
                    print("Unable to parse JSON response")
            }
            
        } else if check == ApiAction.TradersAndTradees.kmy_story {
            do {
                let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let statusModal = DataFetcherStatusModal(parameters:resultObject)
                if statusModal.code == ApiResponseStatus.kAPI_Result_Success {
                    self.statusDataFetcher = statusModal
                    self.collectionViewStatus.reloadData()
                } else {
                   // Util.showAlertWithCallback(AppName, message: statusModal.message, isWithCancel: false)
                }
            } catch {
                print("Unable to parse JSON response")
            }
        }
        else if check == ApiAction.TradersAndTradees.kreport {
            do {
                       let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                       let code  = resultObject["code"] as? String
                       let message = resultObject["message"] as? String
                       
                       if code == ApiResponseStatus.kAPI_Result_Success {
                        Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                       } else {
                           Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                       }
                       
                   } catch {
                       print("Unable to parse JSON response")
                   }
        }
        else if check == ApiAction.TradersAndTradees.kfollow {
            do {
                       let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                       let code  = resultObject["code"] as? String
                       let message = resultObject["message"] as? String
                       
                       if code == ApiResponseStatus.kAPI_Result_Success {
                        Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                       } else {
                           Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                       }
                       
                   } catch {
                       print("Unable to parse JSON response")
                   }
        }
        else if check == ApiAction.TradersAndTradees.krepost {
            do {
                       let resultObject = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                       let code  = resultObject["code"] as? String
                       let message = resultObject["message"] as? String
                       
                       if code == ApiResponseStatus.kAPI_Result_Success {
                        self.postListApiCalling()
                        Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                       } else {
                           Util.showAlertWithCallback(AppName, message: message, isWithCancel: false)
                       }
                       
                   } catch {
                       print("Unable to parse JSON response")
                   }
        }
        
        
    }
 
    func errorResponse(error: Any, check: String) {
        Loader.hideLoader()
        guard let errormsg = error as? Error else {
            return
        }
       Util.showAlertWithCallback(AppName, message: errormsg.localizedDescription , isWithCancel: false)
    }
    

    
     func stringToDateWithFormat( strDate:String,  dateFormat:String) -> Date?{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"//dateFormat
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        if let date1 = formatter.date(from: strDate) {
      //  print("strDate---->\(strDate), date------->\(date1)")
            return date1//"\(formatter.string(from: Date()))"
        }else {
            return nil
        }
    }
}


extension AVAsset {

    func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: self)
            let time = CMTime(seconds: 0.0, preferredTimescale: 600)
            let times = [NSValue(time: time)]
            imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
                if let image = image {
                    completion(UIImage(cgImage: image))
                } else {
                    completion(nil)
                }
            })
        }
    }
}
