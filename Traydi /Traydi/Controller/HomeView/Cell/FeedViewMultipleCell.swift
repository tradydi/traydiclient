//
//  FeedMultipleViewCell.swift
//  Traydi
//
//  Created by mac on 06/02/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//


import UIKit

class FeedViewMultipleCell: UITableViewCell {
 
    @IBOutlet weak var constraintImgFeedHeight: NSLayoutConstraint!
    @IBOutlet weak var imgViewFeed: UIImageView!
    @IBOutlet weak var imgViewFeed1: UIImageView!
    @IBOutlet weak var imgViewFeed2: UIImageView!
    @IBOutlet weak var btnMoreOption: UIButton!
    
    @IBOutlet weak var stkRepost: UIStackView!
    @IBOutlet weak var imgProfile: TYImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblFeedText: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblcomment: UILabel!
    @IBOutlet weak var viewMoreImages: UIView!
    @IBOutlet weak var stackTwoImage: UIStackView!
    @IBOutlet weak var constraintImageHeight: NSLayoutConstraint!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var viewAllImg: UIView!
    @IBOutlet weak var imgeRePostTo: TYImageView!
    @IBOutlet weak var lblRepostToName: UILabel!
    @IBOutlet weak var lblRepostToTime: UILabel!
    
    
     weak var delegate:FeedViewCellDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    
        
    }
    @IBAction func moreOptionBtnAction(_ sender: UIButton) {
        print("MoreOptionBtnAction")
        delegate?.feedViewControllerMenu(Cell: self, sender: sender)
    }
    @IBAction func deleteBtnAction(_ sender: Any) {
         print("DeleteBtnAction")
    }
    
    @IBAction func editBtnAction(_ sender: Any) {
         print("EditBtnAction")
    }
    @IBAction func likeBtnAction(_ sender: Any) {
         print("LikeBtnAction")
        if delegate != nil{
            delegate?.feedViewCellLikePost(cell: self)
        }
    }
    @IBAction func commentsBtnAction(_ sender: Any) {
        if delegate != nil{
            delegate?.feedViewCellWillComment(cell: self)
        }
    }
    @IBAction func shareBtnAction(_ sender: Any) {
        if delegate != nil{
            delegate?.feedViewCell(cell: self)
        }
    }
    @IBAction func actionPlayVideo(_ sender: Any) {
        if delegate != nil{
         delegate?.feedViewPlayVideo(cell: self)
        }
    }
    
    
    static func heightForCellOf(type:FeedViewCellType, text:String?, width:CGFloat)-> Int{
        if type == .Text{
            let hText = text?.heightWithConstrainedWidth(width: width - 40, font: UIFont.systemFont(ofSize: 17))
            return 125 + Int(hText!)
        }
        return 80
    }
}
