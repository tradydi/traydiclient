//
//  TDCpmmentTableViewCell.swift
//  Traydi
//
//  Created by mac on 21/01/20.
//  Copyright © 2020 Creative thought infotech. All rights reserved.
//

import Foundation
import UIKit

class TDCommentTableViewCell: UITableViewCell {
    @IBOutlet weak var viewCommentBg: UIView!
    
    @IBOutlet weak var imgVCommentUserProfile: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    
    @IBOutlet weak var lblReply: UILabel!
    @IBOutlet weak var lblCommentTime: UILabel!
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
        viewCommentBg.clipsToBounds = true
        viewCommentBg.layer.cornerRadius = 10
        viewCommentBg.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
      
       }

       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }
    
    @IBAction func actionCommentLike(_ sender: Any) {
    }
    
    @IBAction func actionCommentReply(_ sender: Any) {
    }
    
}

protocol CommentDelegate: NSObject {
    func addComment(cell:TDCommentTableViewHeaderCell , index : Int)
    func feedViewCell(cell:TDCommentTableViewHeaderCell, index : Int)
}

class TDCommentTableViewHeaderCell: UITableViewCell {
    
    weak var delegate:CommentDelegate?
      var myIndex : Int?
    @IBOutlet weak var viewCommentBg: UIView!
    @IBOutlet weak var imgVCommentUserProfile: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
        @IBOutlet weak var lblReply: UILabel!
    @IBOutlet weak var lblCommentTime: UILabel!
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
        viewCommentBg.clipsToBounds = true
        viewCommentBg.layer.cornerRadius = 10
        viewCommentBg.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner , .layerMinXMaxYCorner]
      
       }

       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }
    
    @IBAction func actionCommentLike(_ sender: Any) {
        delegate?.feedViewCell(cell: self, index: myIndex!)
    }
    
    @IBAction func actionCommentReply(_ sender: Any) {
        delegate?.addComment(cell: self, index: myIndex!)
       
        
    }
    
}
